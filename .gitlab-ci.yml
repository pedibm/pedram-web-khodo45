# Official docker image.
image: docker:latest

services:
  - docker:dind

stages:
  - build
  - test
  - commands
  - deploy

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip

before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  - apk add --update rsync openssh-client gettext
  # Install ssh-agent if not already installed, it is required by Docker.
  # (change apt-get to yum if you use a CentOS-based image)
  # - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'

.ssh_template: &ssh_definition  # Hidden key that defines an anchor named 'job_definition'
    before_script:
        - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
        - apk add --update rsync openssh-client gettext

        # Run ssh-agent (inside the build environment)
        - eval $(ssh-agent -s)
        # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
        - echo "$SSH_PRIVATE_KEY" > id_rsa
        - chmod 600 id_rsa
        - ssh-add id_rsa
        - rm id_rsa

        # For Docker builds disable host key checking. Be aware that by adding that
        # you are suspectible to man-in-the-middle attacks.
        # WARNING: Use this only with the Docker executor, if you use it with shell
        # you will overwrite your user's SSH config.
        - mkdir -p ~/.ssh
        - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
        # In order to properly check the server's host key, assuming you created the
        # SSH_SERVER_HOSTKEYS variable previously, uncomment the following two lines
        # instead.
        # - mkdir -p ~/.ssh
        # - '[[ -f /.dockerenv ]] && echo "$SSH_SERVER_HOSTKEYS" > ~/.ssh/known_hosts'
        # - rsync -avz -e ssh --progress sajad.gitlab root@staging.booltan.net:/tmp/
#        - ssh $SSH "docker pull $CI_REGISTRY_IMAGE:$DOCKER_TAG;"
        - docker pull $CI_REGISTRY_IMAGE:$DOCKER_TAG
        - docker save $CI_REGISTRY_IMAGE:$DOCKER_TAG > $DOCKER_TAG
        - tar czf $DOCKER_TAG.tgz $DOCKER_TAG
        - scp $DOCKER_TAG.tgz $SSH:$DIR
        - ssh $SSH "cd $DIR;tar xzf $DOCKER_TAG.tgz"
        - ssh $SSH "cd $DIR;docker load < $DOCKER_TAG"
        - ssh $SSH "cd $DIR;rm $DOCKER_TAG $DOCKER_TAG.tgz"
        - rm $DOCKER_TAG $DOCKER_TAG.tgz
#        - ssh $SSH "source <(curl -s https://gist.githubusercontent.com/narsic/7f4c81e1bfd8c1d28afd409a744b3f42/raw/c1799bfc6ce047d88c7db133e6dc974a83919b01/docker-free.sh)"


.ssh_template_staging: &ssh_definition_staging  # Hidden key that defines an anchor named 'job_definition'
    before_script:
        - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
        - apk add --update rsync openssh-client gettext

        # Run ssh-agent (inside the build environment)
        - eval $(ssh-agent -s)
        # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
        - echo "$SSH_PRIVATE_KEY" > id_rsa
        - chmod 600 id_rsa
        - ssh-add id_rsa
        - rm id_rsa

        # For Docker builds disable host key checking. Be aware that by adding that
        # you are suspectible to man-in-the-middle attacks.
        # WARNING: Use this only with the Docker executor, if you use it with shell
        # you will overwrite your user's SSH config.
        - mkdir -p ~/.ssh
        - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'


build:
  stage: build
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
  except:
    - master


build_prod:
  stage: build
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - master

test:
  stage: test
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - pip install -r requirements/test.txt
  script:
    - make test
#    - make lint
  except:
    - master

test_prod:
  stage: test
  image: $CI_REGISTRY_IMAGE
  before_script:
    - pip install -r requirements/test.txt
  script:
    - make test
  only:
    - master

deploy_staging:
  <<: *ssh_definition_staging
  stage: deploy
  script:
    - envsubst '$PRODUCTION_STAGING $DIR $NGINX_SITE' <  $NGINX_CONF_TEMPLATE > $NGINX_CONF
    - envsubst < $DOCKER_COMPOSE_TEMPLATE > $DOCKER_COMPOSE
    - envsubst <  $VARIABLES_FILE_TEMPLATE > $VARIABLES_FILE
    - rsync -avz -e ssh --progress $NGINX_CONF $SSH:$DIR/$NGINX_CONF
    - rsync -avz -e ssh --progress $DOCKER_COMPOSE $VARIABLES_FILE $SSH:$DIR
    - rsync -avz -e ssh --progress $VARNISH_CONF_TEMPLATE $SSH:$DIR/$VARNISH_CONF
    - rsync -avz -e ssh --progress $VARNISH_WORDPRESS_CONF_TEMPLATE $SSH:$DIR/$VARNISH_WORDPRESS_CONF
    - ssh $SSH "cd $STAG_PROJECT_DIR; docker pull $CI_REGISTRY_IMAGE:$DOCKER_TAG; docker-compose up -d web celeryd celeryb; source <(curl -s https://gist.githubusercontent.com/narsic/7f4c81e1bfd8c1d28afd409a744b3f42/raw/c1799bfc6ce047d88c7db133e6dc974a83919b01/docker-free.sh) | true"
  variables:
    DIR: '$STAG_PROJECT_DIR'
    NGINX_SITE: '$STAG_NGINX_SITE'
    PRODUCTION_STAGING: 'staging'
    SSH: '$STAG_SSH'
    DOCKER_TAG: 'staging'
    DATABASE_URL: '$STAG_DATABASE_URL'
    BASE_URL: '$STAG_BASE_URL'
    RIZZO_TOKEN: '$STAG_RIZZO_TOKEN'
    DEBUG: 'true'
    TEMPLATE_DEBUG: 'true'
    NUM_WORKERS: '$STAG_NUM_WORKERS'
    SCALE: 1
    WORDPRESS_IP: '$WORDPRESS_IP'
  environment:
    name: staging
    url: http://staging.khodro45.com
  only:
    - staging

flush_staging:
  stage: commands
  script:
    - ssh $SSH "cd $DIR; docker-compose exec -T redis redis-cli flushall"
  variables:
    DIR: '$STAG_PROJECT_DIR'
    PRODUCTION_STAGING: 'staging'
    SSH: '$STAG_SSH'
    DOCKER_TAG: 'staging'
  environment:
    name: staging
    url: http://staging.khodro45.com
  when: manual
  only:
    - staging

migrate_staging:
  <<: *ssh_definition_staging
  stage: commands
  script:
    - ssh $SSH "cd $STAG_PROJECT_DIR; docker pull $CI_REGISTRY_IMAGE:$DOCKER_TAG;"
    - ssh $SSH "cd $DIR; docker-compose run --rm web python manage.py migrate"
  variables:
    DIR: '$STAG_PROJECT_DIR'
    PRODUCTION_STAGING: 'staging'
    SSH: '$STAG_SSH'
    DOCKER_TAG: 'staging'
  environment:
    name: staging
    url: http://staging.khodro45.com
  when: manual
  only:
    - staging

collect_static_staging:
  <<: *ssh_definition_staging
  stage: commands
  script:
    - ssh $SSH "cd $STAG_PROJECT_DIR; docker pull $CI_REGISTRY_IMAGE:$DOCKER_TAG;"
    - ssh $SSH "cd $DIR; docker-compose run --rm web python manage.py collectstatic --noinput"
  variables:
    DIR: '$STAG_PROJECT_DIR'
    PRODUCTION_STAGING: 'staging'
    SSH: '$STAG_SSH'
    DOCKER_TAG: 'staging'
  environment:
    name: staging
    url: http://staging.khodro45.com
  when: manual
  only:
    - staging

flush_prod:
  stage: commands
  script:
    - ssh $SSH "cd $DIR; docker-compose exec -T redis redis-cli flushall"
  variables:
    DIR: '$PROD_PROJECT_DIR'
    NGINX_SITE: '$PROD_NGINX_SITE'
    PRODUCTION_STAGING: 'production'
    SSH: '$PROD_SSH'
    DOCKER_TAG: 'latest'
    DATABASE_URL: '$PROD_DATABASE_URL'
  environment:
    name: production
    url: http://carpars.co
  when: manual
  only:
    - master

migrate_prod:
  <<: *ssh_definition
  stage: commands
  script:
    - ssh $SSH "cd $DIR; docker-compose run --rm web python manage.py migrate"
  variables:
    DIR: '$PROD_PROJECT_DIR'
    NGINX_SITE: '$PROD_NGINX_SITE'
    PRODUCTION_STAGING: 'production'
    SSH: '$PROD_SSH'
    DOCKER_TAG: 'latest'
    DATABASE_URL: '$PROD_DATABASE_URL'
  environment:
    name: production
    url: http://carpars.co
  when: manual
  only:
    - master

collect_static_prod:
  <<: *ssh_definition
  stage: commands
  script:
    - ssh $SSH "cd $DIR; docker-compose run --rm web python manage.py collectstatic --noinput"
  variables:
    DIR: '$PROD_PROJECT_DIR'
    SSH: '$PROD_SSH'
    DOCKER_TAG: 'latest'
  environment:
    name: production
    url: http://carpars.co
  when: manual
  only:
    - master

deploy_prod:
  <<: *ssh_definition
  stage: deploy
  script:
    - envsubst '$PRODUCTION_STAGING $DIR $NGINX_SITE' < $NGINX_CONF_TEMPLATE > $NGINX_CONF
    - envsubst < $DOCKER_COMPOSE_TEMPLATE > $DOCKER_COMPOSE
    - envsubst < $VARIABLES_FILE_TEMPLATE > $VARIABLES_FILE
    - rsync -avz -e ssh --progress $NGINX_CONF $SSH:$DIR/$NGINX_CONF
    - rsync -avz -e ssh --progress $DOCKER_COMPOSE $VARIABLES_FILE $SSH:$DIR
    - rsync -avz -e ssh --progress $VARNISH_CONF_TEMPLATE $SSH:$DIR/$VARNISH_CONF
    - rsync -avz -e ssh --progress $VARNISH_WORDPRESS_CONF_TEMPLATE $SSH:$DIR/$VARNISH_WORDPRESS_CONF
    - ssh $SSH "cd $DIR; docker-compose up -d web celeryd celeryb;"
  variables:
    DIR: '$PROD_PROJECT_DIR'
    NGINX_SITE: '$PROD_NGINX_SITE'
    PRODUCTION_STAGING: 'production'
    SSH: '$PROD_SSH'
    DOCKER_TAG: 'latest'
    DATABASE_URL: '$PROD_DATABASE_URL'
    BASE_URL: '$PROD_BASE_URL'
    RIZZO_TOKEN: '$PROD_RIZZO_TOKEN'
    DEBUG: 'false'
    TEMPLATE_DEBUG: 'false'
    NUM_WORKERS: '$PROD_NUM_WORKERS'
    SCALE: '$PROD_SCALE'
    WORDPRESS_IP: '$WORDPRESS_IP'
  environment:
    name: production
    url: http://carpars.co
  when: manual
  only:
    - master
  retry: 2
