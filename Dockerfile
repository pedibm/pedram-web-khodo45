FROM python:3.5
CMD apt update
CMD apt install libjpeg-dev
WORKDIR /project
ADD . .
RUN make prod-setup
#RUN apt-get remove --purge -y $BUILD_PACKAGES $(apt-mark showmanual) && rm -rf /var/lib/apt/lists/*
#RUN rm -rf ~/.cache
