# Khodro45 Project

Khodro45 Back-End System
The repository contains an all abckend related project for khodro45,
 development, production and (automated) test settings and environments.
 
### `Before Start Developing please read listed project below`

* [Django REST Swagger](https://django-rest-swagger.readthedocs.io/en/latest/)
* [Django Celery](https://docs.celeryproject.org/en/latest/django/first-steps-with-django.html)
* [Celery](https://docs.celeryproject.org/en/latest/index.html)
* [Django Rest Framework](https://www.django-rest-framework.org/)
* [Django-filter](https://django-filter.readthedocs.io/en/master/)
* [Python client for FCM](https://pypi.org/project/pyfcm/)
* [ImageKit](https://github.com/matthewwithanm/django-imagekit)

### Quickstart

    # Install Docker and Docker-compose
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh

    # Clone Your Local Project
    git clone https://gitlab.com/carpars/web.git khodro45
    cd khodro45

    # DockerCompose Start
    docker-compose up

    ### Everything Setup And Ready To Use ###

    # Default user pass for admin
    user: admin
    pass: admin

    # Run Celery
    ## to run celery and worker in development environment
    - python manage.py celeryd
    - python manage.py celerybeat
    
### Batteries included

The development environment includes:

* [South](http://south.readthedocs.org/en/latest/about.html)
  for database migrations (both development and production use it)
* [Django Debug Toolbar](https://github.com/django-debug-toolbar/django-debug-toolbar)
  for displaying extra information about view execution
* SQLite database (`dev.db` in the project root directory)
* Integrated view debugger making it easy to debug crashes directly from the
  browser (Werkzeug and django-extension's
  [runserver_plus](http://pythonhosted.org/django-extensions/runserver_plus.html))
* Full SQL statement logging
* Beefed-up Django shell with model auto-loading and
  [IPython](http://ipython.org/) REPL
* [Flake8](https://pypi.python.org/pypi/flake8) source code checker
  (style, passive code analysis)
* Console E-mail backend set by default in dev for simple E-mail send testing
* Automated testing all set-up with
  [nose](https://nose.readthedocs.org/en/latest/), optionally creating test
  coverage reports, and using the in-memory SQLite database (and disabled
  South) to speed up test execution
* Disabled cache for easier debugging

The production environment includes:

* South for database migrations (both development and production use it)
* [Docker Compose](http://github.com/docker/docker-compose) Single Point To Deploy, Docker Compose
* [Gunicorn](http://gunicorn.org/) integration
* [Django Compressor](http://django_compressor.readthedocs.org/en/master/)
  for CSS/JS asset minification and compilation
* Database auto-discovery via environment settings, compatible with Heroku
* [Sentry](http://sentry.readthedocs.org/en/latest/) client (raven\_compat)
  for exception logging (used only if `SENTRY_DSN` variable is set in
  settings or environment)
* Local-memory cache (although memcached is strongly recommended if available)

### The extended tour

After setting up your project (exec in Docker), try these:

    # make sure all tests pass (you'll need to write them first, though :)
    make test

    # get a test coverage report (outputs to stdout, saves HTML format in
    # cover/index.html and produces Cobertura report compatible with Jenkins)
    make coverage

    # clean up test artifacts, *.pyc files and cached compressed assets
    make clean

    # check if the code follows PEP8 and is free of obvious errors
    # this also includes cyclomatic complexity check and will complain if your
    # code is too complex (configurable by editing the Makefile)
    make lint

    # update the environment (eg. after pulling in new code)
    make dev-update

    # open up the new and improved Django shell
    python manage.py shell_plus

Yearn for more? Django-extension comes with tons of useful management commands,
run `python manage.py help` to get an overview.

### The production setup

The Production Setup as same as development setup, everything Committed and 
tested in CI/CD please referer to [here](./.gitlab-ci.yml) 
The production environment can be set up automatically (at it may require
setting up docker, docker-compose and per-server settings manually).

To set up the production environment for a Khodro45 project, loosely
follow this procedure:

    # Install Docker and Docker-compose
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh

    # Clone Your Local Project
    git clone https://gitlab.com/carpars/web.git khodro45
    cd khodro45

    # DockerCompose Start
    docker-compose up

    ### You need to restore databse file to db container ###
    


### The settings files

The settings files `base` (base settings used in all environments),
`prod` (production settings), `dev` (local development settings) and
`test` (settings used when running automated tests) should contain only the
settings used by all developers/servers.

Per-server (or per-developer) settings should go into `local` module
(ie. `project/settings/local.py`). The usual pattern for this module is to
first import everything from the settings variant that best matches your
environment (`prod` for servers, `dev` for local development), and then
override/add settings as needed.

Example production settings just specifying the production database:

    # file: project/settings/local.py
    from .prod import *

    DATABASES = {
        'default': { ... }
    }

You shouldn't need to add `local.py` to the repository (in fact, git is
already set up to ignore it). If some setting needs to be shared by everyone,
it should probably be added to `base`, `dev` or `prod`.

The local settings file isn't required. If it doesn't exist, the production
setup will be used by default. This is useful if you don't have per-server
settings or they're deployed via Unix environment (as they are on eg. Heroku
and similar cloud hosting providers).

### Environment settings

In either production or develoment mode, settings can also be set via
the environment variables. The following variables are supported:

* `DATABASE_URL` - Heroku-compatible database URL
* `DEBUG` - String `true` enables DEBUG, any other disables
* `TEMPLATE_DEBUG` - String `true` enables TEMPLATE_DEBUG, any other disables
* `COMPRESS_ENABLED` - String `true` enables django-compressor, any other
  disables
* `SQL_DEBUG` - String `true` enables SQL statement logging, any other
   disables (disabled by default, available only if using `dev.py`)
* `CACHE_BACKEND` - String value to put into `CACHES['default']['BACKEND']`
* `EMAIL_BACKEND` - String value for EMAIL_BACKEND (only if using `dev.py`)
* `KEY_TIMEOUT` - Integer value for Redis time based key
* `SMS_NIK` - Dictionary value for Nik SMS service details
* `SMS_PAYAM` - Dictionary value for ADP service details
* `SMS_OPERATOR` - String value for switch between sms panel, can be `nik`, `payam`
* `FCM_TOKEN` - String Must keep Firebase token
* `STAGING_NOTIF_IDENTIFIER` - String identifier for staging notification
* `ONLINE_THRESHOLD` - Integer timeout key for online user to become offline

Note that values from `local.py` override environment settings! You probably
want to use either the local settings file or the environment settings, not
mix them.

All the environment variable set up to be replaced and change in Build OR Deploy 
Please Change Variables [here](https://gitlab.com/carpars/web/-/settings/ci_cd) 
to take effect on staging or production, Do not Hard Code Environment Variable

### Gitlab CI/CD support

The staging and production has setup for build, test and deploy on both environment
also the must used python task like, flush cache, migrate and collect static
has task in gitlab tasks See [Here](https://gitlab.com/carpars/web/-/jobs) for 
more information

### Django Debug Toolbar

Django Debug Toolbar is set up so it's always visible in the dev
environment, and always hidden in the production environment.

### Sentry / Raven

To use the Sentry client, you'll need a server to point it to. Installing
Sentry server is easy as:

    # mkvirtualenv --no-site-packages sentry-env
    # pip install sentry
    # sentry init
    # sentry start

You'll want to install Sentry into its own environment as it requires
Django 1.2 or 1.3 at the moment.

If you don't want to install Sentry yourself, you can use a hosted
version at http://getsentry.com/.

When you connect to your (or hosted) Sentry server and create a new project
there, you'll be given Sentry DSN which you need to put into production
settings to activate Sentry exception logging.

### Compressor

Django Compressor can minify and compile your CSS and JS assets. DJ Skeletor
comes with Compressor support, but to make use of it, you need to use
`{% compress %}` tags in your templates.

By default Compressor runs in online mode, and files are compressed
and cached (if needed) when the template that uses them is first served.
Optionally, it can also use offline mode (COMPRESSOR_OFFLINE) in which
the static files are pre-compressed in deployment phase. To activate this,
you'll need to activate the `COMPRESSOR_OFFLINE` setting (it's commented
out in `settings/prod.py` by default) and update `Makefile` to run the
compressor in the deployment phase.

Note that if you enable offline mode, you will need to run compress after
every template or static file change, so it's recommended to only use it
for deployed/production environments.

### Test code coverage

DJ Skeletor comes with support for nose test runner and code coverage
reporting through coverage.py.

To run a normal test without code coverage report, run `make test`.

To run a test with a coverage report, run `make coverage`. The report
is generated in HTML format in the `cover/` subdirectory, and in the
Cobertura format in `coverage.xml` file (useful for integrating with
Continuous Integration systems, such as Jenkins). The test run also produces
`nosetests.xml` file in the standard JUnit format, also useful for integration
with Jenkins or other CI systems.
