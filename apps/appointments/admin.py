import jdatetime
from django.conf.urls import url
from django.contrib import admin
from django.db import models
from django.forms import TextInput
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.utils.html import format_html
from django.views.decorators.debug import sensitive_post_parameters
from import_export import resources
from import_export.admin import ImportExportActionModelAdmin
from import_export.fields import Field
from jalali_date import datetime2jalali

from .models import Appointment, AppointmentLog, Message, MessageLog


class AppointmentResource(resources.ModelResource):
    brand = Field()
    model = Field()
    option = Field()
    status = Field()
    state = Field()
    sex = Field()

    class Meta:
        model = Appointment
        exclude = ('reminder', 'reminder_user', 'reminder_seen', 'user', 'branch')
        export_order = (
            'id', 'phone', 'status', 'state', 'brand', 'model', 'option', 'year', 'klm', 'reserve', 'name', 'email',
            'sex', 'age', 'job', 'address',)

    def dehydrate_brand(self, obj):
        return '%s' % (obj.option.model.brand.title,)

    def dehydrate_model(self, obj):
        return '%s' % (obj.option.model.title,)

    def dehydrate_option(self, obj):
        return '%s' % (obj.option.title,)

    def dehydrate_status(self, obj):
        if obj.status:
            return '%s' % (obj.STATUS_TYPE[int(obj.status)][1],)
        return None

    def dehydrate_state(self, obj):
        if obj.state:
            return '%s' % (obj.STATE_TYPE[int(obj.state)][1],)
        return None

    def dehydrate_sex(self, obj):
        if obj.sex:
            return '%s' % (obj.SEX[int(obj.sex)][1],)
        return None


class AppointmentAdmin(ImportExportActionModelAdmin):
    list_display = (
        'id', 'option', 'year', 'name', 'phone', 'state', 'reserve_jalali', 'created_jalali', 'status',
        'actions_html')
    search_fields = (
        'name', 'email', 'phone', 'option__title', 'option__model__title', 'option__model__brand__title', 'year', 'klm',
        'branch__title',)
    list_filter = ('status', 'branch', 'state',)
    date_hierarchy = 'reserve'
    list_editable = ['state', 'name', ]
    list_per_page = 25
    resource_class = AppointmentResource

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '18'}), },
    }

    readonly_fields = ["created_time", "modified_time", 'user', 'reminder_user']

    def actions_html(self, obj):
        if obj.inspection_set.last():
            return format_html(
                '<button class="button btn" type="button" onclick="window.open(\'/view/{pk}/\')" > * Publish </button>',
                pk=obj.inspection_set.last().id)
        elif obj.status is Appointment.STATUS_RESERVE:
            return format_html(
                '<button class="button btn" type="button" onclick="window.location=\'{pk}/confirm/\'"> Confirm '
                '</button>',
                pk=obj.id)

    actions_html.allow_tags = True
    actions_html.short_description = "Actions"

    def reserve_jalali(self, obj):
        if obj.reserve:
            jdatetime.set_locale('fa_IR')
            jalali = datetime2jalali(obj.reserve).strftime('%Y %b %d %H:%M')
            return jalali

    reserve_jalali.short_description = "Reserve Date"
    reserve_jalali.admin_order_field = 'reserve'

    def created_jalali(self, obj):
        jdatetime.set_locale('fa_IR')
        jalali = datetime2jalali(obj.created_time).strftime('%Y %b %d %H:%M')
        return jalali

    created_jalali.short_description = "Created Date"
    created_jalali.admin_order_field = 'created_time'

    def get_urls(self):
        return [
                   url(r'^(\d+)/confirm/$',
                       self.admin_site.admin_view(self.confirm))
               ] + super(AppointmentAdmin, self).get_urls()

    @method_decorator(sensitive_post_parameters())
    def confirm(self, request, id, form_url=''):
        # if not self.has_change_permission(request):
        #     raise PermissionDenied
        # obj = get_object_or_404(self.get_queryset(request), pk=id)
        # if obj.inspection_set.first():
        #     msg = ugettext('This item already confirmed')
        #     messages.error(request, msg)
        # else:
        #     inspect = Inspection.create_from_appointment(appointment=obj)
        #     obj.status = Appointment.STATUS_CONFIRM
        #     obj.save()
        #
        #     msg = ugettext('Confirmed, Use app for inspection.')
        #     messages.success(request, msg)
        return HttpResponseRedirect('../../')

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        if 'reminder' in form.changed_data:
            obj.reminder_user = request.user
        obj.save()


class AppointmentLogAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'appointment', 'user', 'state', 'status',)
    search_fields = (
        'appointment', 'user', 'state', 'status',)
    list_filter = ('status', 'user', 'state',)
    date_hierarchy = 'created_time'
    list_per_page = 25
    readonly_fields = ("created_time",)
    raw_id_fields = ('appointment', 'user')


class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'text', 'state',)
    search_fields = ('status', 'text',)
    date_hierarchy = 'created_time'
    list_per_page = 25
    readonly_fields = ("created_time",)


class MessageLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'appointment_id', 'message')
    date_hierarchy = 'created_time'
    list_per_page = 25
    readonly_fields = ("created_time",)


admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(AppointmentLog, AppointmentLogAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(MessageLog, MessageLogAdmin)
