import django_filters.rest_framework
from datetime import timedelta, datetime
from django.utils import timezone
from django_filters import Filter
from rest_framework import viewsets, routers, mixins, filters
from rest_framework.decorators import list_route, detail_route
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.inspections.models import Inspection
from apps.segments.serializers import DateSerializer, HoursSerializer
from carpars.utilities import Redis
from .models import Appointment, Message, MessageLog
from .serializers import AppointmentSerializer, AppointmentCreateSerializer, CheckCreatedTimeSerializer, \
    MessageListSerializer, AppointmentPublicCreateSerializer, AppointmentPublicSerializer
from .tasks import send_notify_complete, copy_inspection_data, send_custom_sms, send_notify_other_cities


class AppointmentViewSet(mixins.UpdateModelMixin,
                         mixins.RetrieveModelMixin,
                         mixins.CreateModelMixin,
                         viewsets.GenericViewSet):
    model = Appointment
    queryset = Appointment.objects.all()
    read_serializer_class = AppointmentPublicSerializer
    write_serializer_class = AppointmentPublicCreateSerializer

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.write_serializer_class
        return self.read_serializer_class

    def perform_create(self, serializer):
        # Create Log Entry
        instance = serializer.save()
        send_notify_other_cities.apply_async(
                              kwargs={"appointment": instance},
                              countdown=60*5)

        instance.create_log()

    def retrieve(self, request, *args, **kwargs):
        """Retrive an Appointment for website step 3"""
        # FIXME: make own serializer, and create key by id and time merge as key
        return super(AppointmentViewSet, self).retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        super(AppointmentViewSet, self).perform_update(serializer)
        obj = self.get_object()
        if self.request.user.is_anonymous:
            send_notify_complete.delay(obj)

    @list_route(methods=['GET'])
    def days(self, req):
        """list of date to appointment and its value"""
        days = []
        start_day = 0
        now = timezone.now().astimezone()
        start_hour = now.hour - 9 + 2  # 9 start working hour, accept reserve from 2 hour later
        if start_hour >= 8:  # if there is no enough time to get reservation, don't show a day
            start_day = 1
        for i in range(start_day, 10):
            d = timezone.now() + timedelta(days=i)
            if d.weekday() == 4:  # Exclude Friday
                continue
            days.append(d)
        data = DateSerializer(days, many=True).data
        return Response(data)

    @list_route(methods=['GET'])
    def hours(self, req):
        """
           day -- insert the "value" of last api response in this value box
        """
        day = req.GET['day']
        day = day + ' 09:00'
        day = datetime.strptime(day, '%Y%m%d %H:%M')
        hours = []
        now = timezone.now().astimezone()
        start_hour = 0
        end_hour = 9
        if now.day == day.day:
            start_hour = now.hour - 9 + 2  # 9 start working hour, accept reserve from 2 hour later
        if day.weekday() == 3:  # Thursday working hour must be reduce.
            start_hour = start_hour + 1
            end_hour = 6
        for i in range(start_hour, end_hour):
            d = day + timedelta(hours=i)
            hours.append(d)
            d = d + timedelta(minutes=30)
            hours.append(d)
        data = HoursSerializer(hours, branch=req.GET['branch'], many=True).data
        return Response(data)


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'


class ListFilter(Filter):
    def filter(self, qs, value):
        if not value:
            return qs

        # For django-filter versions < 0.13, use lookup_type instead of lookup_expr
        self.lookup_expr = 'in'
        values = value.split(',')
        return super(ListFilter, self).filter(qs, values)


class OrderFilter(django_filters.FilterSet):
    state = ListFilter(name='state')

    class Meta:
        model = Appointment
        fields = {
            'id': ['icontains', 'exact'],
            'state': ['icontains', 'exact'],
            'name': ['icontains', 'exact'],
            'email': ['icontains', 'exact'],
            'phone': ['icontains', 'exact'],
            'status': ['exact'],
            'option': ['exact'],
            'branch': ['exact'],
            'klm': ['exact'],
            'year': ['exact'],
        }


class AppointmentBackOfficeViewSet(mixins.ListModelMixin,
                                   mixins.RetrieveModelMixin,
                                   mixins.UpdateModelMixin,
                                   mixins.CreateModelMixin,
                                   viewsets.GenericViewSet):
    model = Appointment
    read_serializer_class = AppointmentSerializer
    write_serializer_class = AppointmentCreateSerializer
    pagination_class = LargeResultsSetPagination
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       filters.OrderingFilter,
                       )
    filter_class = OrderFilter
    ordering_fields = '__all__'
    permission_classes = (IsAuthenticated,)
    return_null = False

    def get_queryset(self):
        """
        Optionally restricts the returned appointments based on user role
        """
        if self.queryset:
            return self.queryset
        user = self.request.user
        queryset = Appointment.objects.all()
        if self.return_null:
            return queryset.filter(id=0)
        if user.role is user.ROLE_QC:
            queryset = queryset.filter(status__in=[
                Appointment.STATUS_AUCTION,
                Appointment.STATUS_BUY,
                Appointment.STATUS_FINISH_INSPECTOR])
        elif user.role is user.ROLE_SELLS:
            queryset = queryset.filter(id=0)  # Disable view for Sells person
        elif user.role is user.ROLE_CALL_CENTER:
            queryset = queryset.filter(created_time__gte=user.date_joined, city_id=1)  # Just Show Tehran Items
        return queryset

    def list(self, request, *args, **kwargs):
        created_from = request.GET.get('from', None)
        created_to = request.GET.get('to', None)
        reserve_from = request.GET.get('reserve_from', None)
        reserve_to = request.GET.get('reserve_to', None)
        reminder = request.GET.get('reminder', None)

        q = self.get_queryset()

        if created_from and created_to:
            data = {'from_date': created_from, 'to_date': created_to}
            ser = CheckCreatedTimeSerializer(data=data)
            if ser.is_valid(raise_exception=True):
                q = q.filter(created_time__gte=ser.validated_data['from_date'],
                             created_time__lte=ser.validated_data['to_date'])

        if reserve_from and reserve_to:
            data = {'from_date': reserve_from, 'to_date': reserve_to}
            ser = CheckCreatedTimeSerializer(data=data)
            if ser.is_valid(raise_exception=True):
                q = q.filter(reserve__gte=ser.validated_data['from_date'],
                             reserve__lte=ser.validated_data['to_date'])

        if reminder == 'true':
            today = timezone.now().date()
            q = q.filter(reminder__date__gte=today, reminder_user=request.user).order_by(
                '-reminder')

        self.queryset = q
        if not q:
            self.return_null = True

        return super(AppointmentBackOfficeViewSet, self).list(request, *args, **kwargs)

    def perform_update(self, serializer):
        reminder = serializer.validated_data.get('reminder')
        instance = self.get_object()
        if reminder and reminder != instance.reminder:  # Reminder is different
            instance = serializer.save(user=self.request.user,
                                       reminder_user=self.request.user,
                                       reminder_seen=False)
        else:
            instance = serializer.save(user=self.request.user)
        if serializer.validated_data.get('state', None) or serializer.validated_data.get('status', None):
            instance.create_log(user=self.request.user)

    # def update(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     instance.user = request.user
    #     instance.save()
    #     return super(AppointmentBackOfficeViewSet, self).update(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.write_serializer_class
        return self.read_serializer_class

    @detail_route(methods=['POST'])
    def confirm(self, request, pk, *args, **kwargs):
        instance = self.get_object()
        if instance.status == Appointment.STATUS_RESERVE:
            Inspection.create_from_appointment(appointment=instance)
            instance.change_status(Appointment.STATUS_CONFIRM, request.user)

        else:
            error_msg = {'error': 'this appointment cannot confirm'}
            return Response(error_msg, status=400)
        serializer = self.read_serializer_class(instance)
        return Response(serializer.data)

    @detail_route(methods=['GET'])
    def publish(self, request, pk, *args, **kwargs):
        instance = self.get_object()
        serializer = AppointmentSerializer(instance)
        return Response(serializer.data)

    @detail_route(methods=['POST'])
    def status_auction_to_buy(self, request, pk, *args, **kwargs):
        instance = self.get_object()
        if (instance.status == Appointment.STATUS_AUCTION) and (not instance.in_auction()):
            instance.change_status(Appointment.STATUS_BUY, request.user)
        else:
            error_msg = {'error': 'this appointment not buy'}
            return Response(error_msg, status=400)
        serializer = AppointmentSerializer(instance)
        return Response(serializer.data)

    @detail_route(methods=['POST'])
    def to_reserve(self, request, pk, *args, **kwargs):
        instance = self.get_object()
        today = timezone.now()
        error_msg = {'error': 'this appointment not reserved'}
        if instance.reserve:
            if instance.name and instance.status == Appointment.STATUS_CHECK and \
                    instance.reserve >= today and instance.branch:
                instance.change_state_status(Appointment.STATE_BACHELOR_INSPECTOR,
                                             Appointment.STATUS_RESERVE, request.user)
            else:
                return Response(error_msg, status=400)
        else:
            return Response(error_msg, status=400)
        serializer = self.read_serializer_class(instance)
        return Response(serializer.data)

    @detail_route(methods=['POST'])
    def again_inspection(self, request, pk, *args, **kwargs):
        if request.user.role not in [request.user.ROLE_SELLS, request.user.ROLE_QC]:
            return Response({'error': 'Access Denied'}, status=401)

        instance = self.get_object()
        try:
            instance.inspection_set.get(status=Inspection.STATUS_NEW)
            return Response({'error': 'Already has Inspection in progress'}, status=400)
        except Inspection.DoesNotExist:
            pass

        # Create new inspection depend on task manager, so we set a redis temporary key to prevent duplicate
        r = Redis.get_instance().conn
        key = 'inspection_again_{}'.format(instance.id)
        r_time = r.get(key)  # Check if already created inspection
        if r_time:
            return Response({'error': 'Already has Inspection in progress, Redis'}, status=400)

        if (instance.status == Appointment.STATUS_AUCTION and (not instance.in_auction())) or \
                instance.status == Appointment.STATUS_FINISH_INSPECTOR:
            copy_inspection_data(instance.id, user=request.user)
        else:
            return Response({'error': 'this appointment not inspection again'}, status=400)
        serializer = self.read_serializer_class(instance)

        r.set(key, True, 90)  # Set redis key, it's going to create new inspection

        return Response(serializer.data)

    @list_route(methods=['GET'])
    def message_list(self, request):
        message = Message.objects.all()
        ser = MessageListSerializer(message, many=True).data
        return Response(ser)

    def message(self, request, pk, message_id, *args, **kwargs):
        obj = self.get_object()
        message = Message.objects.get(id=message_id)
        if obj.status != message.status:
            error = {"error": "message not match"}
            return Response(error, status=400)
        else:
            log = MessageLog()
            log.appointment = obj
            string = message.text
            if obj.reserve:
                from django.template import Context
                from django import template
                variables = {'appointment': obj}
                template = template.Template(message.text)
                context = Context(variables)
                string = template.render(context)

            log.message = message
            obj.state = message.state
            obj.save()
            log.save()
            send_custom_sms.delay(obj.phone, string)
            # send_custom_sms(obj.phone, string)
            return Response({obj.phone: string})

    @list_route(methods=['GET'])
    def days(self, req):
        """list of date to appointment and its value"""
        days = []
        start_day = 0
        now = timezone.now().astimezone()
        start_hour = now.hour - 9 + 2  # 9 start working hour, accept reserve from 2 hour later
        if start_hour >= 8:  # if there is no enough time to get reservation, don't show a day
            start_day = 1
        for i in range(start_day, 10):
            d = timezone.now() + timedelta(days=i)
            if d.weekday() == 4:  # Exclude Weekend
                continue
            days.append(d)
        data = DateSerializer(days, many=True).data
        return Response(data)

    @list_route(methods=['GET'])
    def hours(self, req):
        """
           day -- insert the "value" of last api response in this value box
        """
        day = req.GET['day']
        day = day + ' 09:00'
        day = datetime.strptime(day, '%Y%m%d %H:%M')
        hours = []
        now = timezone.now().astimezone()
        start_hour = 0
        end_hour = 9
        if now.day == day.day:
            start_hour = now.hour - 9 + 0  # 9 start working hour, accept reserve from 2 hour later
        if day.weekday() == 3:  # Thursday working hour must be reduce.
            start_hour = start_hour + 1
            end_hour = 6
        for i in range(start_hour, end_hour):
            d = day + timedelta(hours=i)
            hours.append(d)
            d = d + timedelta(minutes=30)
            hours.append(d)

        data = HoursSerializer(hours, branch=req.GET['branch'], many=True).data
        return Response(data)


router = routers.DefaultRouter()
router.register(r'appointments', AppointmentViewSet, 'Appointments')

back_office_router = routers.SimpleRouter()
back_office_router.register(r'appointments', AppointmentBackOfficeViewSet, 'Appointments')
back_office_router.routes.append(
    routers.Route(
        url=r'^{prefix}/(?P<pk>[^/]+)/message/(?P<message_id>[^/]+){trailing_slash}',
        name='{basename}-message',
        mapping={
            'post': 'message',
        },
        initkwargs={}
    ),
)
