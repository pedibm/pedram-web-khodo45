from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from jalali_date import datetime2jalali

from apps.auctions.models import Auction


class Appointment(models.Model):
    """
        All request store in here,
        difference between request, appointment, auction and ... you hear all day long, came from status and state field
        by merging status and state you could find all types of request, please refer to Product Team Document to find out
    """
    STATUS_CHECK = '0'
    STATUS_RESERVE = '1'
    STATUS_CONFIRM = '2'
    STATUS_AUCTION = '3'
    STATUS_BUY = '4'
    STATUS_FINISH_INSPECTOR = '5'

    STATUS_TYPE = (
        (STATUS_CHECK, _('Register')),
        (STATUS_RESERVE, _('Reserve')),
        (STATUS_CONFIRM, _('Confirm')),
        (STATUS_AUCTION, _('Auction')),
        (STATUS_BUY, _('Buy')),
        (STATUS_FINISH_INSPECTOR, _('Finish Inspection')),
    )

    STATE_NOT_RESPONSE_REGISTER = '0'
    STATE_SOLD_REGISTER = '1'
    STATE_MISTAKE_SUBMIT_REGISTER = '2'
    STATE_MISTAKE_CAR_REGISTER = '3'
    STATE_City_REGISTER = '4'
    STATE_BAD_ROUTE_RESERVE = '5'
    STATE_CANCEL_RESERVE = '6'
    STATE_ZERO_REGISTER = '7'
    STATE_OLD_REGISTER = '8'
    STATE_CANCEL_REGISTER = '9'
    STATE_Call_FOR_PRICE_REGISTER = '10'
    STATE_BACHELOR_INSPECTOR = '11'
    STATE_TRANSFER_INSPECTOR = '12'
    STATE_BAD_PRICE_AUCTION = '13'
    STATE_NOT_RESPONSE_RESERVE = '14'
    STATE_BAD_ROUTE_REGISTER = '15'
    STATE_BAD_DOLLAR_RESERVE = '16'
    STATE_SOLD_RESERVE = '17'
    STATE_BAD_MEETING_INSPECTION = '18'
    STATE_Call_FOR_PRICE_AUCTION = '19'
    STATE_BAD_MEETING_AUCTION = '20'
    STATE_CANCEL_MORTGAGE_REGISTER = '21'
    STATE_OWNER_ABSENCE_REGISTER = '22'
    STATE_LACK_OF_DOCUMENTS_REGISTER = '23'
    STATE_INAPPROPRIATE_CAR_REGISTER = '24'
    STATE_CALL_AGAIN_REGISTER = '25'

    STATE_CANCEL_MORTGAGE_INSPECTION = '26'
    STATE_OWNER_ABSENCE_INSPECTION = '27'
    STATE_LACK_OF_DOCUMENTS_INSPECTION = '28'
    STATE_INAPPROPRIATE_CAR_INSPECTION = '29'

    STATE_TYPE = (
        (STATE_NOT_RESPONSE_REGISTER, _('جواب نداد')),
        (STATE_SOLD_REGISTER, _('فروخته شد')),
        (STATE_MISTAKE_SUBMIT_REGISTER, _('ثبت اشتباه')),
        (STATE_MISTAKE_CAR_REGISTER, _('به دنبال خرید ماشین')),
        (STATE_City_REGISTER, _('شهرستان')),
        (STATE_BAD_ROUTE_RESERVE, _('دسترسی نامناسب')),
        (STATE_CANCEL_RESERVE, _('انصراف از فروش')),
        (STATE_ZERO_REGISTER, _('صفر')),
        (STATE_OLD_REGISTER, _('قدیمی')),
        (STATE_CANCEL_REGISTER, _('انصراف از فروش')),
        (STATE_Call_FOR_PRICE_REGISTER, _('استعلام قیمت')),
        (STATE_BACHELOR_INSPECTOR, _('قرار کارشناسی')),
        (STATE_TRANSFER_INSPECTOR, _('انتقال به کارشناسی')),
        (STATE_BAD_PRICE_AUCTION, _('قیمت نامناسب')),
        (STATE_NOT_RESPONSE_RESERVE, _('جواب نداد')),
        (STATE_BAD_ROUTE_REGISTER, _('دسترسی نامناسب')),
        (STATE_BAD_DOLLAR_RESERVE, _('نوسانات ارز')),
        (STATE_SOLD_RESERVE, _('فروخته شد')),
        (STATE_BAD_MEETING_INSPECTION, _('برخورد نامناسب')),
        (STATE_Call_FOR_PRICE_AUCTION, _('استعلام قیمت')),
        (STATE_BAD_MEETING_AUCTION, _('برخورد نامناسب')),
        (STATE_CANCEL_MORTGAGE_REGISTER, _('فک رهن')),
        (STATE_OWNER_ABSENCE_REGISTER, _('عدم حضور مالک')),
        (STATE_LACK_OF_DOCUMENTS_REGISTER, _('کسری مدارک')),
        (STATE_INAPPROPRIATE_CAR_REGISTER, _('خودرو نامناسب جذب')),
        (STATE_CALL_AGAIN_REGISTER, _('تماس مجدد')),
        (STATE_CANCEL_MORTGAGE_INSPECTION, _('فک رهن')),
        (STATE_OWNER_ABSENCE_INSPECTION, _('عدم حضور مالک')),
        (STATE_LACK_OF_DOCUMENTS_INSPECTION, _('کسری مدارک')),
        (STATE_INAPPROPRIATE_CAR_INSPECTION, _('خودرو نامناسب جذب')),
    )

    REAL_CASE = [STATE_NOT_RESPONSE_REGISTER, STATE_Call_FOR_PRICE_REGISTER, STATE_BAD_ROUTE_REGISTER,
                 STATE_CANCEL_REGISTER]
    NOT_REAL_CASE = [STATE_OLD_REGISTER, STATE_MISTAKE_SUBMIT_REGISTER, STATE_City_REGISTER, STATE_ZERO_REGISTER,
                     STATE_OLD_REGISTER,
                     STATE_MISTAKE_CAR_REGISTER, STATE_INAPPROPRIATE_CAR_REGISTER]
    NOT_REAL_CASE_RESERVE = [STATE_NOT_RESPONSE_RESERVE, STATE_BAD_ROUTE_RESERVE, STATE_CANCEL_RESERVE,
                             STATE_BAD_DOLLAR_RESERVE, STATE_OLD_REGISTER]

    status = models.CharField(max_length=1,
                              choices=STATUS_TYPE,
                              default=STATUS_CHECK)

    state = models.CharField(max_length=2,
                             choices=STATE_TYPE,
                             blank=True,
                             null=True)

    # Customer Info
    name = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(max_length=200, null=True, blank=True)
    phone = models.CharField(max_length=11)
    city = models.ForeignKey('locations.City', null=True)
    MALE = '0'
    FEMALE = '1'
    SEX = (
        (MALE, _('مذکر')),
        (FEMALE, _('مونث')),
    )
    sex = models.CharField(max_length=1, choices=SEX, blank=True)
    age = models.IntegerField(null=True, blank=True)
    job = models.CharField(max_length=100, blank=True)
    address = models.TextField(max_length=300, blank=True)

    reminder = models.DateTimeField(blank=True, null=True)

    # Car Details
    option = models.ForeignKey('cars.Option')
    year = models.IntegerField()
    klm = models.IntegerField()
    user = models.ForeignKey('users.User', related_name='appointments_user', null=True, blank=True,
                             help_text="user that changed this appointment last time")
    reminder_user = models.ForeignKey('users.User', related_name='reminder_user', null=True, blank=True,
                                      help_text="user that changed reminder last time")
    reminder_seen = models.BooleanField(default=False)

    # Reserve Detail
    branch = models.ForeignKey('locations.Branch', null=True)
    reserve = models.DateTimeField(null=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{0}->{1}'.format(self.status, self.name)

    def jdate(self):
        return datetime2jalali(self.reserve).strftime('%Y/%m/%d')

    def jtime(self):
        return datetime2jalali(self.reserve).strftime('%H:%M')

    def change_status(self, status, user=None):
        # TODO: check if status exists in lists
        # if not status in self.STATUS_TYPE:
        #     raise Exception('Status Must choose from list')
        self.status = status
        self.save()
        self.create_log(user)

    def change_state(self, state, user=None):
        # TODO: check if state exists in lists
        # if not state in self.STATE_TYPE:
        #     raise Exception('State Must choose from list')
        self.state = state
        self.save(raise_exception=True)
        self.create_log(user)

    def change_state_status(self, state, status, user=None):
        # TODO: check if state and status exists in lists
        # if not state in self.STATE_TYPE:
        #     raise Exception('State Must choose from list')
        # if not status in self.STATUS_TYPE:
        #     raise Exception('Status Must choose from list')
        self.state = state
        self.status = status
        self.save()
        self.create_log(user)

    def create_log(self, user=None):
        log = AppointmentLog(appointment=self)
        log.status = self.status
        log.state = self.state
        if user:
            log.user = user
        log.save()

    def in_auction(self):
        try:
            Auction.objects.get(inspection__appointment=self, end_time__gte=timezone.now())
            return True
        except Auction.DoesNotExist:
            return False


class AppointmentLog(models.Model):
    """
    Log all staff action on Appointment
    """
    appointment = models.ForeignKey(Appointment)

    status = models.CharField(max_length=2,
                              blank=True,
                              null=True)

    state = models.CharField(max_length=2,
                             blank=True,
                             null=True)
    user = models.ForeignKey('users.User', null=True, blank=True)

    created_time = models.DateTimeField(auto_now_add=True)


class Message(models.Model):
    """
    Store Default message for appointment
    each message could show on specific status and/or change
    state and status to specific status
    """
    status = models.CharField(max_length=2,
                              blank=True,
                              null=True)
    state = models.CharField(max_length=2,
                             blank=True,
                             null=True)
    text = models.TextField(max_length=500,
                            blank=True,
                            null=True)

    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text


class MessageLog(models.Model):
    """"
    Log Message sent by Staff to customer
    """
    appointment = models.ForeignKey(Appointment)
    message = models.ForeignKey(Message, blank=True, null=True)

    created_time = models.DateTimeField(auto_now_add=True)
