# -*- coding: utf-8 -*-

import re
from datetime import datetime
from django.conf import settings
from django.utils import timezone
from rest_framework import serializers

from apps.cars.models import Year
from apps.cars.serializers import OptionSerializer
from apps.inspections.models import Inspection
from apps.inspections.serializers import InspectionMiniSerializer
from apps.locations.serializers import BranchSerializer, CitySerializer
from .models import Appointment, Message


class AppointmentSerializer(serializers.ModelSerializer):
    inspection = serializers.SerializerMethodField()
    option = OptionSerializer()
    branch = BranchSerializer()
    status = serializers.SerializerMethodField()
    state = serializers.SerializerMethodField()
    is_publishable = serializers.SerializerMethodField()
    qc_url = serializers.SerializerMethodField()
    share_url = serializers.SerializerMethodField()
    _inspection = None
    reminder = serializers.SerializerMethodField()
    to_reserve = serializers.SerializerMethodField()
    to_buy = serializers.SerializerMethodField()
    to_confirm = serializers.SerializerMethodField()
    again_inspection = serializers.SerializerMethodField()
    city = CitySerializer()

    class Meta:
        model = Appointment
        fields = '__all__'

    def get_inspection(self, obj):
        if obj.inspection_set.count() > 0:
            inspection = obj.inspection_set.last()
            d = InspectionMiniSerializer(inspection)
            return d.data
        return None

    def get_status(self, obj):
        return {"id": obj.status, "text": Appointment.STATUS_TYPE[int(obj.status)][1]}

    def get_state(self, obj):
        if obj.state:
            return {"id": obj.state, "text": obj.get_state_display()}
        else:
            return None

    def get_is_publishable(self, obj):
        self._inspection = Inspection.objects.filter(appointment=obj).last()
        if self._inspection and self._inspection.status == Inspection.STATUS_AUDIT:
            if (obj.status == Appointment.STATUS_FINISH_INSPECTOR) \
                or (obj.status == Appointment.STATUS_AUCTION and not obj.in_auction()):
                return True
        return False

    def get_qc_url(self, obj):
        if self._inspection:
            return "{0}/view/{1}".format(settings.BASE_URL, self._inspection.slug)
        return None

    def get_share_url(self, obj):
        if obj.status in [Appointment.STATUS_AUCTION, Appointment.STATUS_BUY]:
            auc = self._inspection.auction_set.last()
            if auc:
                return "{0}/app/main/carDetails/{1}/{2}".format(settings.BASE_URL, self._inspection.slug, auc.id)
        return None

    def get_reminder(self, obj):
        if obj.reminder:
            return timezone.localtime(obj.reminder)

    def get_to_reserve(self, obj):
        today = timezone.now()
        if obj.reserve:
            if obj.name and obj.status == Appointment.STATUS_CHECK and obj.reserve >= today and obj.branch:
                return True

        return False

    def get_to_buy(self, obj):
        if (obj.status == Appointment.STATUS_AUCTION) and (not obj.in_auction()):
            return True
        return False

    def get_to_confirm(self, obj):
        if obj.status == Appointment.STATUS_RESERVE:
            return True
        return False

    def get_again_inspection(self, obj):
        if ((obj.status == Appointment.STATUS_AUCTION) and (
            not obj.in_auction())) or obj.status == Appointment.STATUS_FINISH_INSPECTOR:
            return True
        else:
            return False


class AppointmentCreateSerializer(serializers.ModelSerializer):
    status = serializers.CharField(read_only=True)

    class Meta:
        model = Appointment
        exclude = ['created_time', 'modified_time']

    def validate_phone(self, value):
        if not re.search("09" + "\\d{9}", value):
            raise serializers.ValidationError("WrongNumber")
        return value


class AppointmentPublicCreateSerializer(serializers.ModelSerializer):
    # city = serializers.CharField(required=True)

    class Meta:
        model = Appointment
        fields = ['id', 'phone', 'option', 'klm', 'year', 'city', 'name', 'email', 'branch', 'reserve']

    def validate_phone(self, value):
        if not re.search("09" + "\\d{9}", value):
            raise serializers.ValidationError("WrongNumber")
        return value

    def validate_year(self, value):
        if value > 1000:
            return value
        try:
            year = Year.objects.get(id=value)
        except Year.DoesNotExist:
            raise serializers.ValidationError("Year Not Valid")
        return year.title

    def validate(self, data):
        if 'phone' in data:
            today = datetime.today()
            cars = Appointment.objects.filter(created_time__date=today, phone=data['phone'],
                                              option__id=data['option'].id)
            if cars.count() >= 1:  # FIXME: handle those have change in status or state
                # if cars[0].city_id == data['city']:
                #     raise serializers.ValidationError("Duplicate")
                self.instance = cars[0]
        if self.instance and not (
                        self.instance.reserve is None and self.instance.status == Appointment.STATUS_CHECK and
                (self.instance.state is None or self.instance.state is Appointment.STATE_City_REGISTER)):
            raise serializers.ValidationError("Can not Change, Reserved")

        return data


class AppointmentPublicSerializer(serializers.ModelSerializer):
    option = OptionSerializer()
    branch = BranchSerializer()

    class Meta:
        model = Appointment
        fields = ['id', 'option', 'klm', 'year', 'name', 'branch', 'reserve']


class CheckCreatedTimeSerializer(serializers.Serializer):
    from_date = serializers.DateTimeField()
    to_date = serializers.DateTimeField()


class MessageListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        exclude = ['created_time', 'state', ]
