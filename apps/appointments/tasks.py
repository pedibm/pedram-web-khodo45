# -*- coding: utf-8 -*-
import urllib.error
import urllib.parse
import urllib.request

from celery import task
from django.conf import settings
from django.template import Context
from django.template.loader import get_template

from apps.inspections.models import Inspection, InspectionData
from carpars.utilities import random_generate


@task
def send_notify_sms(appointment_id):
    from .models import Appointment

    appointment = Appointment.objects.get(id=appointment_id)
    if appointment.reserve:
        return

    variables = {'appointment': appointment}

    template = get_template('notification/first_sms.txt')
    context = Context(variables)
    send_sms(appointment, context, template, "appointment")


@task
def send_notify_complete(appointment):
    appointment.refresh_from_db()
    variables = {'appointment': appointment}

    template = get_template('notification/reserve_sms.txt')
    context = Context(variables)
    send_sms(appointment, context, template, "appointment")


@task
def send_notify_other_cities(appointment):
    appointment.refresh_from_db()
    from apps.appointments.models import Message
    from django.template import Template

    if appointment.city_id == 1 or appointment.state == appointment.STATE_City_REGISTER:
        # Tehran City ID or State already changed, Do Nothing
        return
    variables = {'appointment': appointment}

    message = Message.objects.filter(status=appointment.status, state=appointment.STATE_City_REGISTER).first()
    if not message:
        return
    template = Template(message.text)
    context = Context(variables)
    string = template.render(context)

    send_custom_sms(appointment.phone, string)
    appointment.state = message.state
    appointment.save()


def initial_params(params):
    if settings.DEBUG:
        params['message'] = settings.STAGING_NOTIF_IDENTIFIER + params['message'].decode('utf8')

    if settings.SMS_OPERATOR == 'payam':
        params['from'] = settings.SMS_PAYAM['from']
        params['apikey'] = settings.SMS_PAYAM['apikey']
        params['content'] = params['message']
    else:
        params['senderNumber'] = settings.SMS_NIK['src']

        params['username'] = settings.SMS_NIK['username']
        params['password'] = settings.SMS_NIK['password']
    return params


@task
def send_custom_sms(phone, text, var=None):
    params = {}
    url = "https://niksms.com/fa/publicapi/ptpSms"
    if var == "new_auction":
        url = "https://niksms.com/fa/publicapi/groupsms"

        params['numbers'] = ','.join(phone)
    else:
        params['numbers'] = str(phone)
    params['message'] = text.encode('utf8')
    params = initial_params(params)
    if settings.SMS_OPERATOR == 'payam':
        url = 'https://s3.payamsms.com/rest/send'
        params['to'] = params['numbers']
    params = urllib.parse.urlencode(params).encode()
    req = urllib.request.Request(url, data=params)
    f = urllib.request.urlopen(req)
    print(f.read())


@task
def send_sms(input_model, context, template, var):
    params = {}
    if var == "appointment":
        phone = str(input_model.phone).replace('-', '')
        params['yourMessageIds'] = input_model.id
    elif var == "User":
        phone = str(input_model.username)
    elif var == "login_token":
        phone = str(input_model)
    text = template.render(context)
    send_custom_sms(phone=phone, text=text)


@task
def copy_inspection_data(appointment_id, user):
    from .models import Appointment

    # change appointment obj
    appointment_obj = Appointment.objects.get(id=appointment_id)
    appointment_obj.change_state_status(Appointment.STATE_TRANSFER_INSPECTOR, Appointment.STATUS_CONFIRM, user)

    # get last inspection id
    last_inspection = Inspection.objects.filter(appointment=appointment_id).order_by('id').last()
    # change last inspection slug
    last_inspection.slug = last_inspection.id
    last_inspection.save()
    # make new inspection
    inspection_obj = Inspection()
    inspection_obj.appointment = appointment_obj
    inspection_obj.status = Inspection.STATUS_NEW
    inspection_obj.views = 1
    inspection_obj.slug = generate_slug()
    inspection_obj.save()

    # copy last inspection data to new inspection
    last_inspection_data = InspectionData.objects.filter(inspection=last_inspection)
    for item in last_inspection_data:
        item.inspection = inspection_obj
        item.pk = None
        item.save()


def generate_slug():
    slug = random_generate(7)
    try:
        Inspection.objects.get(slug=slug)
    except Inspection.DoesNotExist:
        return slug
    # Slug Already Exist regenerate slug
    return generate_slug()
