from django.contrib import admin

from .models import Auction, Bid


class AuctionAdmin(admin.ModelAdmin):
    list_display = ('id', 'appointment_name', 'appointment_id', 'price', 'start_price', 'end_time', 'user')
    search_fields = ('id', 'user__username', 'inspection__appointment__id', 'inspection__appointment__name')
    readonly_fields = ("created_time", "modified_time",)
    raw_id_fields = ('inspection',)

    def appointment_name(self, obj):
        return obj.inspection.appointment.name

    def appointment_id(self, obj):
        return obj.inspection.appointment.id


class BidAdmin(admin.ModelAdmin):
    list_display = ('appointment_name', 'appointment_id', 'price', 'user')
    search_fields = ('id', 'user__username', 'auction__inspection__appointment__name')
    readonly_fields = ("created_time",)
    raw_id_fields = ('auction',)

    def appointment_name(self, obj):
        return obj.auction.inspection.appointment.name

    def appointment_id(self, obj):
        return obj.auction.inspection.appointment.id


admin.site.register(Auction, AuctionAdmin)
admin.site.register(Bid, BidAdmin)
