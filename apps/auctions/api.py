from django.db.models import Max
from django.utils import timezone
import django_filters.rest_framework
from rest_framework import viewsets, routers, mixins, filters
from rest_framework.decorators import detail_route, list_route
from apps.appointments.api import LargeResultsSetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.inspections.models import Inspection
from apps.appointments.models import Appointment
from .models import Auction, Bid
from .serializers import AuctionListSerializer, BidSerializer, BidBackOfficeSerializer, BidingSerializer, \
    AuctionBackOfficeSerializer, BidsBackOfficeSerializer, AuctionBackOfficeReadSerializer, \
    AuctionInInspectionListSerializer


class AuctionViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    model = Auction
    serializer_class = AuctionListSerializer
    queryset = Auction.objects.all().order_by('-id')
    permission_classes = (IsAuthenticated,)
    pagination_class = None

    def list(self, request, custom=False, *args, **kwargs):
        if not custom:
            now = timezone.now()
            self.queryset = self.queryset.filter(end_time__gte=now)
        if request.path.endswith('/mine/') or request.path.endswith('/winner/'):
            self.pagination_class = LargeResultsSetPagination
        return super(AuctionViewSet, self).list(request, *args, **kwargs)

    @list_route(methods=['GET', ])
    def inspections(self, req, *args, **kwargs):
        self.serializer_class = AuctionInInspectionListSerializer
        self.pagination_class = LargeResultsSetPagination
        self.queryset = Inspection.objects.filter(status=Inspection.STATUS_NEW)
        return self.list(req, custom=True, *args, **kwargs)

    @detail_route(methods=['GET', 'POST'])
    def bid(self, req, pk, *args, **kwargs):
        """retrive and post bid for auction bid details by pk."""
        data = {}
        status = 200
        obj = self.get_object()
        if req.method == 'POST':
            ser = BidingSerializer(data=req.data, auction=obj, context={'request': req})
            if not ser.is_valid():
                data.update({'errors': ser.errors})
                status = 403
            else:
                # FIXME: make transaction
                bid = Bid(auction=obj, user=req.user)
                bid.step = ser.validated_data['step']
                bid.price = bid.step + ser.validated_data['price']  # Calculate new price
                obj.last_price = bid.price
                bid.save()
                obj.save()
                status = 201

        d = BidSerializer(obj, context={'request': req})
        data.update(**d.data)
        res = Response(data, status=status)
        res['Cache-Control'] = 'no-cache'
        return res

    @list_route(methods=['GET', ])
    def mine(self, req, *args, **kwargs):
        user = req.user
        auctions_ids = Bid.objects.filter(user=user).distinct('auction_id').values('auction_id')
        self.queryset = self.queryset.filter(id__in=auctions_ids)
        res = self.list(req, custom=True, *args, **kwargs)
        res['Cache-Control'] = 'no-cache'
        return res

    @list_route(methods=['GET', ])
    def winner(self, req, *args, **kwargs):
        user = req.user
        au = Bid.objects.filter(user=user)
        a = self.queryset
        au = au.distinct('auction_id').order_by('-auction_id', '-id')
        au = au.values('id')
        a = a.annotate(latest=Max('bid__id')).filter(latest__in=au)

        self.queryset = a
        res = self.list(req, custom=True, *args, **kwargs)
        res['Cache-Control'] = 'no-cache'
        return res


class OrderFilter(django_filters.FilterSet):
    id = django_filters.NumberFilter(method='handle_option_branch')
    branch = django_filters.NumberFilter(method='handle_option_branch')
    brand = django_filters.NumberFilter(method='handle_option_branch')
    model = django_filters.NumberFilter(method='handle_option_branch')
    option = django_filters.NumberFilter(method='handle_option_branch')

    class Meta:
        model = Auction
        fields = {
            'end_time': ['lte', 'gte'],
            'created_time': ['lte', 'gte'],
        }

    def handle_option_branch(self, queryset, name, value):
        names = {"id": "inspection__appointment_id",
                 "option": "inspection__appointment__option",
                 "model": "inspection__appointment__option__model",
                 "brand": "inspection__appointment__option__model__brand",
                 "branch": "inspection__appointment__branch"}

        return queryset.filter(**{
            names[name]: value,
        })


class AuctionBackOfficeViewSet(mixins.CreateModelMixin,
                               mixins.ListModelMixin,
                               mixins.RetrieveModelMixin,
                               viewsets.GenericViewSet):
    model = Auction
    serializer_class = AuctionBackOfficeReadSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = LargeResultsSetPagination
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       filters.OrderingFilter,
                       )
    filter_class = OrderFilter
    ordering_fields = '__all__'

    def get_queryset(self):
        """
        Optionally restricts the returned auctions based on user role
        """
        user = self.request.user
        queryset = Auction.objects.all().order_by('-id')
        if user.role in [user.ROLE_SELLS, user.ROLE_QC]:
            queryset = queryset.filter()
        elif user.is_superuser:
            queryset = queryset.filter()
        else:
            queryset = queryset.filter(id=0)
        return queryset

    def list(self, request, custom=False, *args, **kwargs):
        self.serializer_class = AuctionBackOfficeSerializer
        return super(AuctionBackOfficeViewSet, self).list(request, *args, **kwargs)

    def perform_create(self, serializer):
        # Include the user attribute directly, rather than from request data.
        instance = serializer.save(user=self.request.user)

    def create(self, request, *args, **kwargs):
        """
        Send command to device

        Parameters:
        - command: string
        - params: string (JSON encoded list or dict)
        """
        self.serializer_class = AuctionBackOfficeSerializer
        ser = AuctionBackOfficeSerializer(data=request.data)
        if ser.is_valid():
            # for transfer status to STATUS_AUCTION
            inspection_id = request.data['inspection']

            inspection = Inspection.objects.filter(id=inspection_id).first()
            inspection.appointment.change_state_status(None, Appointment.STATUS_AUCTION, request.user)

            # FIXME: Do Some Fix HERE :D , Handle Cache Varnish For Auction
            # FIXME: Make Task or Whatever Else :D
            import http.client as httplib
            conn = httplib.HTTPConnection("varnish:6085")
            conn.request("PURGE", "/*/{}/*".format(inspection_id))
            r1 = conn.getresponse()
            conn.close()

            return super(AuctionBackOfficeViewSet, self).create(request, *args, **kwargs)
        else:
            return Response(ser.errors, status=400)

    @detail_route(methods=['GET', ])
    def bids(self, req, pk, *args, **kwargs):
        """retrive and post bid for auction bid details by pk."""
        data = {}
        obj = self.get_object()

        bds = obj.bid_set.all().order_by('-id')
        d = BidsBackOfficeSerializer(bds, many=True)
        data.update({'result': d.data})
        return Response(data)

    @detail_route(methods=['GET', ])
    def bid(self, req, pk, *args, **kwargs):
        """retrive bid for auction bid details by pk."""
        data = {}
        obj = self.get_object()
        d = BidBackOfficeSerializer(obj)
        data.update(**d.data)
        res = Response(data)
        res['Cache-Control'] = 'no-cache'
        return res

    @detail_route(methods=['POST', ])
    def finish(self, req, pk, *args, **kwargs):
        """finish auction before times end."""
        if not req.user.is_superuser:
            return Response({"error": "Not Valid User"}, status=403)
        obj = self.get_object()
        now = timezone.now()
        if obj.end_time < now:
            return Response({"error": "Auction Already Ended"}, status=403)
        obj.end_time = now
        obj.save()
        # TODO: Update Varnish cache server for list and object
        ser = AuctionBackOfficeSerializer(obj)
        res = Response(ser.data, status=200)
        res['Cache-Control'] = 'no-cache'
        return res


router = routers.DefaultRouter()
router.register(r'auctions', AuctionViewSet, 'Auctions')

back_office_router = routers.SimpleRouter()
back_office_router.register(r'auctions', AuctionBackOfficeViewSet, 'Auctions')
