from datetime import timedelta
from django.utils import timezone
from django.dispatch.dispatcher import receiver
from django.db.models.signals import pre_save, post_save
from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.auctions.tasks import handle_post_save_bid
from apps.firebase.tasks import send_push, send_push_5min, send_push_end_time, send_push_sms
from apps.inspections.models import InspectionData


class Auction(models.Model):
    inspection = models.ForeignKey('inspections.Inspection')
    step = models.IntegerField(default=500000)
    start_price = models.BigIntegerField()
    price = models.BigIntegerField(null=True)
    last_price = models.BigIntegerField(null=True, blank=True)
    end_time = models.DateTimeField(default=(timezone.now() + timedelta(minutes=30)))
    user = models.ForeignKey('users.User', help_text=_('Creator User'))

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "ins->{0}, step->{1}, price->{2}".format(self.inspection.appointment.name, self.step, self.price)

    def get_notif_body(self):
        appointment = self.inspection.appointment
        try:
            year = InspectionData.objects.get(inspection=self.inspection,
                                              question_id=self.inspection.YEAR_ID).answer
        except InspectionData.DoesNotExist:
            year = appointment.year
        body = "{2} | {1} | {0}".format(year, appointment.option.model.title, appointment.option.model.brand.title)
        return body

    def countdown(self):
        now = timezone.now()
        countdown = (self.end_time - now).total_seconds()
        if countdown > 0:
            return int(countdown)
        return 0


@receiver(post_save, sender=Auction)
def push_notification_auction(sender, instance, created, **kwargs):
    if not created:
        return
    title = "خودرو جدید ثبت شده ✅"
    body = instance.get_notif_body()
    data_message = {'auction_id': instance.id, 'inspection_id': instance.inspection_id}
    send_push.apply_async(args=(title, body,),
                          kwargs={"data_message": data_message,
                                  "time_to_live": instance.countdown(), "instance": instance},
                          countdown=60)
    send_push_5min.apply_async(args=(instance,), countdown=instance.countdown() - (60 * 5))
    send_push_end_time.apply_async(args=(instance,), countdown=instance.countdown())
    send_push_sms.apply_async(args=(title, body,),
                              kwargs={"instance": instance},
                              countdown=60)


class Bid(models.Model):
    auction = models.ForeignKey('Auction')
    user = models.ForeignKey('users.User')
    step = models.BigIntegerField()
    price = models.BigIntegerField()

    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "ins->{0}, step->{1}, price->{2}".format(self.auction.inspection.appointment.name, self.step, self.price)


@receiver(post_save, sender=Bid)
def push_notification_bid(sender, instance, created, **kwargs):
    if not created:
        return
    handle_post_save_bid.delay(instance)
