from datetime import datetime
from rest_framework import serializers

from apps.appointments.models import Appointment
from apps.cars.serializers import OptionSerializer
from apps.fields.models import Question
from apps.fields.serializers import QuestionSerializer
from apps.images.models import Image
from apps.images.serializers import ImageAuctionSerializer
from apps.inspections.models import Inspection, InspectionData
from apps.inspections.serializers import InspectionDealerImageSerializer, VIEWS_PLUS
from apps.locations.serializers import BranchMiniSerializer
from apps.users.serializers import UserMiniSerializer
from .models import Auction, Bid


class AuctionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Auction
        exclude = ('created_time',)


class BidSerializerBase(serializers.Serializer):
    highest = serializers.SerializerMethodField()
    end = serializers.SerializerMethodField()
    step = serializers.SerializerMethodField()

    def get_highest(self, obj):
        if obj.last_price:
            return obj.last_price
        return obj.start_price

    def get_end(self, obj):
        return obj.end_time.timestamp()

    def get_step(self, obj):
        return obj.step


class BidBackOfficeSerializer(BidSerializerBase):
    user = serializers.SerializerMethodField()

    def get_user(self, obj):
        b = Bid.objects.filter(auction=obj).last()
        if b:
            return UserMiniSerializer(b.user).data
        return None


class BidSerializer(BidSerializerBase):
    user_latest = serializers.SerializerMethodField()

    def get_user_latest(self, obj):
        user = self.context['request'].user
        b = Bid.objects.filter(user=user, auction=obj).last()
        if b:
            return b.price
        return None


class AuctionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Auction
        fields = (
            'id', 'option', 'year', 'name', 'cover_image', 'bid',
            'klm', 'views', 'price', 'branch', 'inspection_id',
            'positive_comments_count', 'document', 'main_images'
        )

    inspection_id = serializers.SerializerMethodField()
    option = serializers.SerializerMethodField()
    year = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    cover_image = serializers.SerializerMethodField()
    klm = serializers.SerializerMethodField()
    views = serializers.SerializerMethodField()
    branch = serializers.SerializerMethodField()
    bid = serializers.SerializerMethodField()
    positive_comments_count = serializers.SerializerMethodField()
    document = serializers.SerializerMethodField()
    main_images = serializers.SerializerMethodField()

    def get_inspection_id(self, obj):
        return obj.inspection.slug

    def get_option(self, obj):
        return OptionSerializer(obj.inspection.appointment.option).data

    def get_year(self, obj):
        return obj.inspection.appointment.year

    @staticmethod
    def get_klm(obj):
        try:
            return InspectionData.objects.get(inspection=obj.inspection, question_id=Inspection.KLM_ID).answer
        except InspectionData.DoesNotExist:
            return obj.inspection.appointment.klm

    def get_name(self, obj):
        return obj.inspection.appointment.name

    def get_cover_image(self, obj):
        image = InspectionDealerImageSerializer(obj.inspection).data  # FIXME
        return image['image']['cover']

    def get_views(self, obj):
        return obj.inspection.views + VIEWS_PLUS

    def get_branch(self, obj):
        return BranchMiniSerializer(obj.inspection.appointment.branch).data

    def get_bid(self, obj):
        return BidSerializer(obj, context=self.context).data

    def get_positive_comments_count(self, obj):  # FIXME
        inspection_obj = Inspection.objects.get(id=obj.inspection.id)
        qs = Question.objects.filter(question__field_type=Question.TYPE_POSITIVE).exclude(private=True)
        qs = QuestionSerializer(qs, many=True, inspection=inspection_obj)
        positive_comments_count = 0
        for question in qs.data:
            for item in question['answers']:
                if item['title'] == 'بلی' and item['default'] == True:
                    positive_comments_count += 1
        data = ({'positive_comments_count': positive_comments_count})
        return data

    @staticmethod
    def get_document(obj):
        if not type(obj) is Inspection:
            obj = obj.inspection
        try:
            data = InspectionData.objects.get(inspection=obj, question__id=1979)
            return data.answer
        except InspectionData.DoesNotExist:
            return "1252"

    @staticmethod
    def get_main_images(obj):
        if not type(obj) is Inspection:
            obj = obj.inspection
        qs = Question.objects.filter(field_type=Question.TYPE_IMAGE_MAIN).exclude(private=True)
        inspection_data = InspectionData.objects.filter(question__in=qs, inspection=obj).order_by('question__order')
        images_id = []
        for inspection in inspection_data:
            images_id.append(int(inspection.answer))
        main_images = Image.objects.filter(id__in=images_id)
        main_images_list = list(main_images)
        main_images_list.sort(key=lambda t: images_id.index(t.pk))
        images = []
        for i in range(0, len(main_images_list)):
            image = {'url': main_images_list[i].file, 'id': main_images_list[i].id}
            images.append(image)

        main_images = ImageAuctionSerializer(images, many=True).data
        return main_images


class AuctionInInspectionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inspection
        fields = (
            'id', 'option', 'year', 'klm', 'branch',
        )

    id = serializers.SerializerMethodField()
    option = serializers.SerializerMethodField()
    year = serializers.SerializerMethodField()
    klm = serializers.SerializerMethodField()
    branch = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.slug

    def get_option(self, obj):
        return OptionSerializer(obj.appointment.option).data

    def get_year(self, obj):
        return obj.appointment.year

    @staticmethod
    def get_klm(obj):
        try:
            return InspectionData.objects.get(inspection=obj, question_id=Inspection.KLM_ID).answer
        except InspectionData.DoesNotExist:
            return obj.appointment.klm

    def get_name(self, obj):
        return obj.appointment.name

    def get_branch(self, obj):
        return BranchMiniSerializer(obj.appointment.branch).data


class AuctionBackOfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Auction
        exclude = ('user',)

    last_bid = serializers.SerializerMethodField()
    option = serializers.SerializerMethodField()
    appointment_id = serializers.SerializerMethodField()
    views = serializers.SerializerMethodField()
    bid_count = serializers.SerializerMethodField()

    def get_last_bid(self, obj):
        if obj.bid_set.count() > 0:
            return BidsBackOfficeSerializer(obj.bid_set.last()).data
        return None

    def get_option(self, obj):
        return OptionSerializer(obj.inspection.appointment.option).data

    def get_appointment_id(self, obj):
        return obj.inspection.appointment.id

    def validate_end_time(self, value):
        now = datetime.utcnow()
        req_end_time = datetime.strptime(str(value)[:16], "%Y-%m-%d %H:%M")
        diff_minates = (req_end_time - now).total_seconds() / 60
        if diff_minates < 15:
            raise serializers.ValidationError("your auction time must be more than 15 minates")

        return value

    def validate(self, data):
        if data['inspection'].appointment.in_auction():
            raise serializers.ValidationError("Already In Auction")

        if data['inspection'].appointment.status == Appointment.STATUS_FINISH_INSPECTOR or \
            (data['inspection'].appointment.status == Appointment.STATUS_AUCTION and
                 not (data['inspection'].appointment.in_auction())):
            pass
        else:
            raise serializers.ValidationError("Status Error")
        return data

    def get_views(self, obj):
        return obj.inspection.views

    def get_bid_count(self, obj):
        return obj.bid_set.count()


class AuctionBackOfficeReadSerializer(AuctionBackOfficeSerializer):
    class Meta:
        model = Auction
        exclude = ('user', 'inspection')


class BidsBackOfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bid
        fields = '__all__'

    user = UserMiniSerializer()


class BidingSerializer(serializers.Serializer):
    class Meta:
        fields = ['price', 'step']

    price = serializers.IntegerField()
    step = serializers.IntegerField()

    def __init__(self, *args, **kwargs):
        self._auction = kwargs.pop('auction', None)
        super(BidingSerializer, self).__init__(*args, **kwargs)

    def validate(self, data):
        """
        Check if last bid own by bidder, then reject
        """
        user = self.context['request'].user
        if user.is_staff:
            return data
        if not user._is_confirmed:
            raise serializers.ValidationError("User Not Confirmed!")
        if self._auction.last_price:
            bid = Bid.objects.filter(auction=self._auction).order_by('-id').first()
            if bid.user == user:
                raise serializers.ValidationError("Already Apply")
        return data

    def validate_price(self, value):
        """
        Check if price is latest
        """
        if self._auction.last_price and not self._auction.last_price == value:
            raise serializers.ValidationError("Old Price, Update Data")
        return value

    def validate_step(self, value):
        """
        Check if step is correct
        """
        if self._auction.step > value:
            raise serializers.ValidationError("Step!?")
        return value
