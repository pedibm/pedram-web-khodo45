# -*- coding: utf-8 -*-

from celery import task
from datetime import timedelta

from apps.firebase.tasks import send_push


@task
def handle_post_save_bid(obj):
    from .models import Bid
    bid = Bid.objects.exclude(id=obj.id).last()
    if obj.auction.countdown() <= 60:
        obj.auction.end_time = obj.auction.end_time + timedelta(minutes=1)
        obj.auction.save()
    if bid:
        title = "قیمت بالاتری ثبت شده ⬆"
        body = obj.auction.get_notif_body()

        data_message = {'auction_id': obj.auction_id, 'inspection_id': obj.auction.inspection_id}

        send_push.delay(title, body, user=bid.user, data_message=data_message,
                        time_to_live=obj.auction.countdown())
