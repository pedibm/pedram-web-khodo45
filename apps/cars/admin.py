from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse

from .models import Brand, Model, Option, OptionInspection, CarContent, OthersPrice, Year, Provider, ProviderModel, \
    PriceBackOffice, LogBrandOffice, LogModelOffice, LogYearOffice, LogOptionOffice
from functools import partial



class BrandAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    readonly_fields = ("created_time", "modified_time",)


class ModelAdmin(admin.ModelAdmin):
    search_fields = ('title', 'brand__title')
    readonly_fields = ("created_time", "modified_time",)


class OptionAdmin(admin.ModelAdmin):
    search_fields = ('title', 'model__title',)
    raw_id_fields = ('model',)
    readonly_fields = ("created_time", "modified_time",)


class OptionInspectionAdmin(admin.ModelAdmin):
    search_fields = ('title', 'option',)
    raw_id_fields = ('option',)
    readonly_fields = ("created_time", "modified_time",)


class CarContentAdmin(admin.ModelAdmin):
    list_display = ('title', 'brand', 'actions_html')
    search_fields = ('title', 'brand__title',)

    def actions_html(self, obj):
        return format_html(
            '<button class="button btn" type="button" onclick="window.open(\'{url}/\')" > * View '
            '</button>',
            slug=obj.brand.slug, url=reverse('landing', args=(obj.brand.title,)))

    actions_html.allow_tags = True
    actions_html.short_description = "Actions"


class YearAdmin(admin.ModelAdmin):
    search_fields = ('title', 'model__title', 'brand__title')


class ProviderAdmin(admin.ModelAdmin):
    search_fields = ('name', 'title',)


def model_links(self):
    return '<a href="%s">%s</a>' % (reverse('admin:cars_model_change', args=(self.model.id,)), self.model.title)


model_links.allow_tags = True
model_links.short_description = u'Model'


class ProviderModelAdmin(admin.ModelAdmin):
    search_fields = ('key', 'provider__title', 'model__title')
    raw_id_fields = ('model',)
    list_display = ('id', 'provider', 'key', model_links, 'option')
    list_display_links = ('id', 'provider',)

    def get_form(self, request, obj=None, **kwargs):
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield, request=request, obj=obj)
        return super(ProviderModelAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        instance = kwargs.pop('obj', None)
        formfield = super(ProviderModelAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "option":
            if instance and instance.model_id:
                formfield.queryset = Option.objects.filter(model_id=instance.model_id)
            else:
                formfield.queryset = Option.objects.filter(id=0)
        return formfield


class OtherPriceAdmin(admin.ModelAdmin):
    # raw_id_fields = ('brand', 'model', 'option',)
    list_display = ('id', 'title', 'provider', 'price', 'provider_model', 'year', 'kilometer')
    list_filter = ('model__model__brand', 'provider')
    search_fields = ('model__model__brand__title', 'model__model__title', 'title', 'url')
    date_hierarchy = 'created_time'

    def get_form(self, request, obj=None, **kwargs):
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield, request=request, obj=obj)
        return super(OtherPriceAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        instance = kwargs.pop('obj', None)
        formfield = super(OtherPriceAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "model" and instance:
            formfield.queryset = ProviderModel.objects.filter(provider_id=instance.provider_id)
        return formfield

    def provider_model(self, obj):
        return obj.model.model


class PriceBackOfficeModelAdmin(admin.ModelAdmin):
    list_display = ('option', 'year', 'from_price', 'to_price',)


class LogBrandOfficeModelAdmin(admin.ModelAdmin):
    list_display = ('brand', 'user', 'modified_time', )


class LogModelOfficeModelAdmin(admin.ModelAdmin):
    list_display = ('model', 'user', 'modified_time', )


class LogYearOfficeModelAdmin(admin.ModelAdmin):
    list_display = ('year', 'user', 'modified_time', )


class LogOptionOfficeModelAdmin(admin.ModelAdmin):
    list_display = ('option', 'user', 'modified_time', )


admin.site.register(Brand, BrandAdmin)
admin.site.register(Model, ModelAdmin)
admin.site.register(Option, OptionAdmin)
admin.site.register(OptionInspection, OptionInspectionAdmin)
admin.site.register(CarContent, CarContentAdmin)
admin.site.register(OthersPrice, OtherPriceAdmin)
admin.site.register(Year, YearAdmin)
admin.site.register(Provider, ProviderAdmin)
admin.site.register(ProviderModel, ProviderModelAdmin)
admin.site.register(PriceBackOffice, PriceBackOfficeModelAdmin)
admin.site.register(LogBrandOffice, LogBrandOfficeModelAdmin)
admin.site.register(LogModelOffice, LogModelOfficeModelAdmin)
admin.site.register(LogYearOffice, LogYearOfficeModelAdmin)
admin.site.register(LogOptionOffice, LogOptionOfficeModelAdmin)
