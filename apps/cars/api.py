import django_filters.rest_framework
from celery.exceptions import TimeoutError
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, routers, mixins, filters
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from apps.appointments.models import Appointment

from apps.appointments.api import LargeResultsSetPagination
from .models import Brand, Model, Option, Year, PriceBackOffice, LogBrandOffice, LogModelOffice, LogYearOffice, \
    LogOptionOffice
from .serializers import BrandWebSerializer, ModelWebSerializer, OptionWebSerializer, YearSerializer, \
    PriceBackOfficeReadSerializer, PriceBackOfficeSerializer, BrandCreateSerializer, ModelCreateSerializer, \
    BrandBackOfficeSerializer, ModelBackOfficeSerializer, OptionCreateBackOfficeSerializer, OptionWebRetriveSerializer
from .serializers import SuggestSerializer
from .tasks import suggest


class BrandViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """List of brands of a car"""
    model = Brand
    serializer_class = BrandWebSerializer
    queryset = Brand.objects.all().exclude(is_hidden=True)


class ModelViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """ models List of a car"""
    model = Model
    serializer_class = ModelWebSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('brand__id',)

    def get_queryset(self):
        """
        Optionally restricts the returned models to a given brands,
        by filtering against a `brand_id` query parameter in the URL.
        """
        options_with_years = Option.objects.all().exclude(years__isnull=True).values('model_id')
        queryset = Model.objects.filter(id__in=options_with_years)
        brand_id = self.request.query_params.get('brand_id', None)
        if brand_id is not None:
            queryset = queryset.filter(brand_id=brand_id)
        return queryset


class OptionViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """List of options of a car"""
    model = Option
    serializer_class = OptionWebSerializer
    queryset = Option.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('model__id',)

    def get_queryset(self):
        """
        Optionally restricts the returned models to a given models,
        by filtering against a `model_id` query parameter in the URL.
        """
        queryset = Option.objects.all()
        year_id = self.request.query_params.get('year_id', None)
        model_id = self.request.query_params.get('model_id', None)
        if year_id is not None:
            queryset = queryset.filter(model_id=model_id, years=year_id)
        return queryset


class YearViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """existing years in the system that car brand depend this years"""
    model = Year
    serializer_class = YearSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned models to a given brands,
        by filtering against a `brand_id` query parameter in the URL.
        """
        queryset = Year.objects.all()
        model_id = self.request.query_params.get('model_id', None)
        if model_id is not None:
            model = Model.objects.get(id=model_id)
            option_years = model.option_set.all().values('years')
            queryset = queryset.filter(id__in=option_years)
        return queryset


class PriceApp(viewsets.GenericViewSet):
    """
    """
    getC = None

    serializer_class = SuggestSerializer

    @list_route(methods=['GET'])
    def get(self, req):
        #  TODO: Remove this line, sells team need
        # return Response({'min': None, 'max': None})

        try:
            ap = Appointment.objects.get(id=req.query_params['appointment_id'])
        except Appointment.DoesNotExist:
            return Response({'min': None, 'max': None})

        try:
            price = PriceBackOffice.objects.get(option=ap.option, year__title=ap.year)
        except PriceBackOffice.DoesNotExist:
            return Response({'min': None, 'max': None})

        result = {'min': price.from_price, 'max': price.to_price}
        return Response(result)

    def get_numpy(self, req):
        # TODO: use numpy and our data set
        print(req.query_params)
        # data = req.query_params
        ap = Appointment.objects.get(id=req.query_params['appointment_id'])
        data = {}
        data['carModel'] = ap.option.model_id
        data['carMake'] = ap.option.model.brand_id
        data['productionYear'] = ap.year
        data['carKM'] = ap.klm
        serializer = self.serializer_class(data=data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=400)
        result = suggest(serializer.validated_data)
        return Response(result) # TODO: Use single instance in celery

        async_result = suggest.delay(serializer.validated_data)
        try:
            result = async_result.get()
        except TimeoutError:
            result = None
        status = async_result.status
        traceback = async_result.traceback
        if isinstance(result, Exception):
            return Response({
                'status': status,
                'error': str(result),
                'traceback': traceback,
            })
        else:
            return Response(result)


class PriceOrderFilter(django_filters.FilterSet):
    class Meta:
        model = PriceBackOffice
        fields = {
            'id': ['exact'],
            'year__title': ['exact'],
            'option__title': ['icontains', 'exact'],
            'option__model__title': ['icontains', 'exact'],
            'option__model__brand__title': ['icontains', 'exact'],
            'from_price': ['exact'],
            'to_price': ['exact'],
        }


class PriceBackOfficeViewSet(mixins.ListModelMixin,
                             mixins.RetrieveModelMixin,
                             mixins.CreateModelMixin,
                             mixins.UpdateModelMixin,
                             viewsets.GenericViewSet):
    """"""
    model = PriceBackOffice
    read_serializer_class = PriceBackOfficeReadSerializer
    write_serializer_class = PriceBackOfficeSerializer
    queryset = PriceBackOffice.objects.all()
    permission_classes = (IsAuthenticated,)
    pagination_class = LargeResultsSetPagination
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       filters.OrderingFilter,
                       )
    ordering_fields = '__all__'
    filter_class = PriceOrderFilter

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.write_serializer_class
        return self.read_serializer_class


def log_update(obj, attr, log_model, user):
    var_log = log_model
    # var_log.brand = obj
    setattr(var_log, attr, obj)
    var_log.user = user
    var_log.opration = var_log.UPDATE
    var_log.save()


def log_create(attr, log_model, model, user, data):
    var_log = log_model
    # var_log.brand = Brand.objects.get(id=data['id'])
    setattr(var_log, attr, model.objects.get(id=data))
    var_log.user = user
    var_log.opration = var_log.CREATE
    var_log.save()


class BrandOrderFilter(django_filters.FilterSet):
    class Meta:
        model = Brand
        fields = {
            'id': ['exact'],
            'title': ['icontains', 'exact'],
        }


class BrandBackOfficeViewSet(mixins.ListModelMixin,
                             mixins.RetrieveModelMixin,
                             mixins.CreateModelMixin,
                             mixins.UpdateModelMixin,
                             mixins.DestroyModelMixin,
                             viewsets.GenericViewSet):
    """"""
    model = Brand
    read_serializer_class = BrandBackOfficeSerializer
    write_serializer_class = BrandCreateSerializer
    queryset = Brand.objects.all()
    permission_classes = (IsAuthenticated,)
    pagination_class = LargeResultsSetPagination
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       filters.OrderingFilter,
                       )
    filter_class = BrandOrderFilter

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        log_update(instance, 'brand', LogBrandOffice(), request.user)
        return super(BrandBackOfficeViewSet, self).update(request, *args, **kwargs)

    def perform_create(self, serializer):
        super(BrandBackOfficeViewSet, self).perform_create(serializer)
        log_create('brand', LogBrandOffice(), Brand, self.request.user, serializer.data['id'])

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        related_appointment = Appointment.objects.filter(option__model__brand=instance)
        if related_appointment:
            return Response({"error": "this brand use in some appointments"}, status=400)
        return super(BrandBackOfficeViewSet, self).destroy(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.write_serializer_class
        return self.read_serializer_class


class ModelOrderFilter(django_filters.FilterSet):
    class Meta:
        model = Model
        fields = {
            'id': ['exact'],
            'title': ['icontains', 'exact'],
            'brand__id': ['exact'],
        }


class ModelBackOfficeViewSet(mixins.ListModelMixin,
                             mixins.RetrieveModelMixin,
                             mixins.CreateModelMixin,
                             mixins.UpdateModelMixin,
                             mixins.DestroyModelMixin,
                             viewsets.GenericViewSet):
    """"""
    model = Model
    read_serializer_class = ModelBackOfficeSerializer
    write_serializer_class = ModelCreateSerializer
    queryset = Model.objects.all()
    permission_classes = (IsAuthenticated,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       filters.OrderingFilter,
                       )
    filter_class = ModelOrderFilter
    pagination_class = LargeResultsSetPagination

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        log_update(instance, 'model', LogModelOffice(), request.user)
        return super(ModelBackOfficeViewSet, self).update(request, *args, **kwargs)

    def perform_create(self, serializer):
        super(ModelBackOfficeViewSet, self).perform_create(serializer)
        log_create('model', LogModelOffice(), Model, self.request.user, serializer.data['id'])

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        related_appointment = Appointment.objects.filter(option__model=instance)
        if related_appointment:
            return Response({"error": "this model use in some appointments"}, status=400)
        return super(ModelBackOfficeViewSet, self).destroy(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.write_serializer_class
        return self.read_serializer_class


class YearBackOfficeViewSet(mixins.ListModelMixin,
                            mixins.RetrieveModelMixin,
                            mixins.CreateModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.DestroyModelMixin,
                            viewsets.GenericViewSet):
    """"""
    model = Year
    read_serializer_class = YearSerializer
    write_serializer_class = YearSerializer
    queryset = Year.objects.all()
    permission_classes = (IsAuthenticated,)
    pagination_class = LargeResultsSetPagination

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        log_update(instance, 'year', LogYearOffice(), request.user)
        return super(YearBackOfficeViewSet, self).update(request, *args, **kwargs)

    def perform_create(self, serializer):
        super(YearBackOfficeViewSet, self).perform_create(serializer)
        log_create('year', LogYearOffice(), Year, self.request.user, serializer.data['id'])

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        related_appointment = Appointment.objects.filter(option__years__id=instance.id)
        if related_appointment:
            return Response({"error": "this year use in some appointments"}, status=400)
        return super(YearBackOfficeViewSet, self).destroy(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.write_serializer_class
        return self.read_serializer_class

    def get_queryset(self):
        """
        Optionally restricts the returned models to a given brands,
        by filtering against a `brand_id` query parameter in the URL.
        """
        queryset = Year.objects.all()
        model_id = self.request.query_params.get('model__id', None)
        if model_id is not None:
            model_id = model_id.replace("/", '')
            model = Model.objects.get(id=model_id)
            option_years = model.option_set.all().values('years')
            queryset = queryset.filter(id__in=option_years)
        return queryset


class OptionOrderFilter(django_filters.FilterSet):
    class Meta:
        model = Option
        fields = {
            'id': ['exact'],
            'title': ['icontains', 'exact'],
            'model__id': ['exact'],
            'model__brand__id': ['exact'],
            'years__id': ['exact'],

        }


class OptionBackOfficeViewSet(mixins.ListModelMixin,
                              mixins.RetrieveModelMixin,
                              mixins.CreateModelMixin,
                              mixins.UpdateModelMixin,
                              mixins.DestroyModelMixin,
                              viewsets.GenericViewSet):
    """"""
    model = Option
    read_serializer_class = OptionWebSerializer
    write_serializer_class = OptionCreateBackOfficeSerializer
    queryset = Option.objects.all()
    permission_classes = (IsAuthenticated,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       filters.OrderingFilter,
                       )
    filter_class = OptionOrderFilter
    pagination_class = LargeResultsSetPagination

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        log_update(instance, 'option', LogOptionOffice(), request.user)
        return super(OptionBackOfficeViewSet, self).update(request, *args, **kwargs)

    def perform_create(self, serializer):
        super(OptionBackOfficeViewSet, self).perform_create(serializer)
        log_create('option', LogOptionOffice(), Option, self.request.user, serializer.data['id'])

    def retrieve(self, request, *args, **kwargs):
        self.read_serializer_class = OptionWebRetriveSerializer
        return super(OptionBackOfficeViewSet, self).retrieve(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        related_appointment = Appointment.objects.filter(option=instance)
        if related_appointment:
            return Response({"error": "this option use in some appointments"}, status=400)
        return super(OptionBackOfficeViewSet, self).destroy(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.write_serializer_class
        return self.read_serializer_class


router = routers.DefaultRouter()
router.register(r'brands', BrandViewSet, 'Brands')
router.register(r'models', ModelViewSet, 'Models')
router.register(r'options', OptionViewSet, 'Options')
router.register(r'years', YearViewSet, 'Years')
router.register(r'price', PriceApp, 'Price')


back_office_router = routers.SimpleRouter()
back_office_router.register(r'price_back_office', PriceBackOfficeViewSet, 'PriceBackOffice')
back_office_router.register(r'brands', BrandBackOfficeViewSet, 'Brands')
back_office_router.register(r'models', ModelBackOfficeViewSet, 'Models')
back_office_router.register(r'years', YearBackOfficeViewSet, 'Years')
back_office_router.register(r'options', OptionBackOfficeViewSet, 'Options')
