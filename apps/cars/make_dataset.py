# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import datetime


def str2Null(x):
    """
    check if price or model is null,drop that row
    :param x:
    :return:
    """
    if x == "":
        return None
    else:
        return x


def year2Digitsfix(x):
    if 10 < x < 100:
        return 1300 + x
    else:
        return x


def yearOutOfRange(x):
    if (1300 < x < 1400) or (1940 < x < 2020):
        return x
    else:
        return None


def _fixedProductionYear(date, kharejiIrani):
    '''fixed date base on KharejiIrani '''
    if (date < 1500) and (kharejiIrani == 1): return (date + 621)
    if (date > 1500) and (kharejiIrani == -1):
        return date - 621
    else:
        return date


def kharejiIrani(model):
    '''tabdil model mashin be khareji va irani b komak sal tolid'''

    kharejiList = ['هایلوکس', 'کپچر', 'آزرا', 'النترا', 'اسپورتیج',
                   'سراتو', 'سوناتا', 'اپتیما', 'راو4', 'کمری',
                   'سانتافه (iX45)', 'پرادو', 'کوپه',
                   'لندکروز', 'فلوئنس', 'کرولا', 'اپیروس',
                   'سیمبل', 'جنسیس کوپه', 'داستر'
        , 'اکسنت', 'ولوستر', 'جی 3 هاچ بک', 'سورنتو']

    if model in kharejiList:
        return 1
    else:
        return -1


def saalCleaning(saal):
    '''transfer shamsi date to eng'''
    if saal < 1500: saal = saal + 621
    return saal


def kmToSaalRatio(df):
    '''
    get DataFram and return km to saal ration
    :param df: datafram
    :return:
    '''

    return (df.kilometer / (datetime.date.today().year - df.saalMiladi)).replace([np.inf, -np.inf], np.nan).fillna(0)


def dataMaker(input_filepath=r'data.csv'):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    df = pd.read_csv(input_filepath)
    df = df.applymap(str2Null).dropna(subset=['price', 'model'], axis=0)

    # str2int
    df['price'] = pd.to_numeric(df.price)
    df = df.drop_duplicates()

    # delet demand lists
    df = df[~((df.title.str.contains('خرید') |
               (df.description.str.contains('اقساط')) |
               (df.description.str.contains('درخواست')) |
               (df.title.str.contains('درخواست'))) |
              (df.title.str.contains('حواله')) |
              (df.title.str.contains('چک')) |
              (df.title.str.contains('منطقه')) |
              (df.description.str.contains('منطقه')))]
    # delet contais 'saayer'
    # df = df[~df.model.str.contains('سایر')]

    # drop nulls
    df = df.dropna()

    # drop tavafoqi lists
    df = df[~(df.kilometer == 0)]
    df = df[~(df.price == 0)]

    # drop rare cases
    # alternative => we can use percentile here
    # moreThen20 = df.model.value_counts()[df.model.value_counts() > 20] # TODO: need this at 5
    # df = df[df.model.isin(moreThen20.index)]

    # drop less then 2 std distance
    sigma = 2
    for model in df.model.unique():
        temp = df[df.model == model]

        if np.isnan(temp[(temp.kilometer == 0)].price.median()):
            newCarPrice = temp[temp.kilometer == temp.kilometer.min()].price.mean()
        else:
            newCarPrice = temp[(temp.kilometer == 0)].price.median()
        index = temp[temp.price > 1.1 * newCarPrice].index

        df.drop(index[:], inplace=True)

        temp = df[df.model == model]
        index = temp[(np.abs(temp.price - temp.price.mean()) > (sigma * temp.price.std()))].index
        df.drop(index, inplace=True)

    # fix km out of range
    df = df[df.kilometer < 1000000]


    # fix date
    df['year'] = df.year.apply(year2Digitsfix).apply(yearOutOfRange)
    print(str(len(df)) + ' DF LEN 1')


    # df['kharejiIrani'] = df.model.apply(kharejiIrani)
    # df['fixedProductionYear'] = df.apply(lambda row: _fixedProductionYear(row['year'], row['kharejiIrani']),
    #                                      axis=1)

    df['saalMiladi'] = df.year.apply(saalCleaning)
    df['km2SaalRatio'] = kmToSaalRatio(df)

    return df
