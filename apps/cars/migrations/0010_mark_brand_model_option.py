# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from django.db.models import Value
from django.db.models.functions import Concat


# Mark all old brand, model, option in production, so we could delete them


def mark_brands(apps, schema_editor):
    Brand = apps.get_model('cars', 'Brand')
    Brand.objects.all().update(slug=Concat('slug', Value('_old')), title=Concat('title', Value('_old')))


def mark_models(apps, schema_editor):
    Model = apps.get_model('cars', 'Model')
    Model.objects.all().update(title=Concat('title', Value('_old')))


def mark_options(apps, schema_editor):
    Option = apps.get_model('cars', 'Option')
    Option.objects.all().update(title=Concat('title', Value('_old')))


class Migration(migrations.Migration):
    dependencies = [
        ('cars', '0009_auto_20180807_1141'),
    ]

    operations = [
        migrations.RunPython(mark_brands),
        migrations.RunPython(mark_models),
        migrations.RunPython(mark_options),
    ]
