# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-09-17 09:17
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0012_auto_20180912_1119_squashed_0015_auto_20180915_1749'),
    ]

    operations = [
        migrations.AddField(
            model_name='othersprice',
            name='created_time',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='othersprice',
            name='description',
            field=ckeditor.fields.RichTextField(default=' '),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='othersprice',
            name='title',
            field=models.CharField(default=' ', max_length=300),
            preserve_default=False,
        ),
    ]
