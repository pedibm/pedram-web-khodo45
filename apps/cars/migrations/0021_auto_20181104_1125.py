# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-11-04 11:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0020_pricebackoffice'),
    ]

    operations = [
        migrations.AddField(
            model_name='pricebackoffice',
            name='created_time',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False),
        ),
        migrations.AddField(
            model_name='pricebackoffice',
            name='modified_time',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
