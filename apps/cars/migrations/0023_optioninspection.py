# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-12-10 11:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0022_auto_20181105_1348'),
    ]

    operations = [
        migrations.CreateModel(
            name='OptionInspection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, help_text='Option Title', max_length=200, verbose_name='title')),
                ('created_time', models.DateTimeField(auto_now=True)),
                ('modified_time', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('option', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cars.Option')),
                ('years', models.ManyToManyField(to='cars.Year')),
            ],
            options={
                'ordering': ['title'],
            },
        ),
    ]
