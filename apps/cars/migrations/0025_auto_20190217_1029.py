# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2019-02-17 06:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0024_logbrandoffice_logmodeloffice_logoptionoffice_logyearoffice'),
    ]

    operations = [
        migrations.AddField(
            model_name='logbrandoffice',
            name='opration',
            field=models.CharField(blank=True, choices=[('0', 'Create'), ('1', 'Update'), ('2', 'Delete')], max_length=1, null=True),
        ),
        migrations.AddField(
            model_name='logmodeloffice',
            name='opration',
            field=models.CharField(blank=True, choices=[('0', 'Create'), ('1', 'Update'), ('2', 'Delete')], max_length=1, null=True),
        ),
        migrations.AddField(
            model_name='logoptionoffice',
            name='opration',
            field=models.CharField(blank=True, choices=[('0', 'Create'), ('1', 'Update'), ('2', 'Delete')], max_length=1, null=True),
        ),
        migrations.AddField(
            model_name='logyearoffice',
            name='opration',
            field=models.CharField(blank=True, choices=[('0', 'Create'), ('1', 'Update'), ('2', 'Delete')], max_length=1, null=True),
        ),
    ]
