import anyjson
import datetime
import numpy as np
from ckeditor.fields import RichTextField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from math import ceil
from sklearn import preprocessing
from sklearn.neighbors import NearestNeighbors

from apps.seo.models import SEO
from . import make_dataset


class Brand(models.Model):
    MUST_USED_LIST = ["peugeot", "toyota", "jac", "chery", "dena", "renault", "kia", "lexus", "hyundai", "brilliance"]

    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Field Title'),
    )

    title_en = models.CharField(
        _('title English'), max_length=200,
        help_text=_('English Title'), null=True
    )

    slug = models.CharField(
        _('slug'), max_length=50,
        help_text=_('must include, `-_`'),
        unique=True
    )

    is_hidden = models.BooleanField(_('is hidden'),
                                   blank=True, default=False)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']


class Model(models.Model):
    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Answer Title'),
    )

    title_en = models.CharField(
        _('title English'), max_length=200,
        help_text=_('English Title'), null=True
    )

    slug = models.CharField(
        _('slug'), max_length=50,
        help_text=_('must include, `-_`'),
    )

    brand = models.ForeignKey('Brand', blank=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = [('slug', 'brand')]
        ordering = ['title']

    def __str__(self):
        return "{0}/{1}".format(self.brand, self.title)


class Year(models.Model):
    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Year Title'),
        blank=True, unique=True
    )

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-title']


class Option(models.Model):
    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Option Title'),
        blank=True,
    )

    years = models.ManyToManyField('Year', blank=True, null=True)

    model = models.ForeignKey('Model', blank=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        if self.title:
            return "{0}/{1}".format(self.model, self.title)
        return "{0}".format(self.model)


class OptionInspection(models.Model):
    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Option Title'),
        blank=True,
    )

    years = models.ManyToManyField('Year')

    option = models.ForeignKey('Option')

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return "{0}/{1}".format(self.option, self.title)


class OthersPrice(models.Model):
    """"
    Store Bama & Sheypoor Crawled data
    """
    title = models.CharField(max_length=300)
    provider = models.ForeignKey('Provider')
    url = models.URLField(blank=True, null=True)

    model = models.ForeignKey('ProviderModel', blank=True, null=True)

    year = models.IntegerField(blank=True, null=True)
    mobile = models.BigIntegerField(blank=True, null=True)
    kilometer = models.IntegerField(blank=True, null=True)
    price = models.BigIntegerField(blank=True, null=True)
    description = RichTextField()
    plain = RichTextField(blank=True)

    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class CarContent(SEO):
    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Content Title'),
    )

    description = RichTextField(_('Content'))
    image = models.ImageField(upload_to="car_landing")

    brand = models.OneToOneField('Brand')

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Provider(models.Model):
    """"
    Store Bama And Sheypoor
    """
    name = models.CharField(
        _('name'), max_length=200,
        help_text=_('Provider Name'),
    )
    title = models.CharField(
        _('provider title'), max_length=200,
        help_text=_('Provider Title'),
        null=True, blank=True,
    )

    rizzo_plugin = models.IntegerField(_('Plugin ID in rizzo'), null=True, blank=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class ProviderModel(models.Model):
    key = models.CharField(
        _('Provider Key'), max_length=200,
        help_text=_('Provider id or title to match field'),
    )
    model = models.ForeignKey(Model)
    option = models.ForeignKey(Option, null=True, blank=True)
    provider = models.ForeignKey(Provider)

    condition = RichTextField(blank=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.key


class PriceSuggest():
    """
    Not a real Model, just to calculate price based on Crawled data
    """

    @staticmethod
    def maker():
        print('RECalculate everything :D ')
        df = make_dataset.dataMaker()
        print(str(len(df)) + 'IN LEN')
        print(df)
        km2SaalRationMdeol = preprocessing.StandardScaler().fit(df.km2SaalRatio.reshape(-1, 1))
        df['km2SaalRatioScale'] = km2SaalRationMdeol.transform(df.km2SaalRatio.reshape(-1, 1))

        df['age'] = datetime.date.today().year - df.saalMiladi
        df = df[df.age >= 0]
        ageModel = preprocessing.StandardScaler().fit(df.age.reshape(-1, 1))
        df['age'] = ageModel.transform(df.age.reshape(-1, 1))
        return df, ageModel, km2SaalRationMdeol

    df, ageModel, km2SaalModel = None, None, None

    @classmethod
    def carList(cls):
        if cls.df is None:
            cls.df, cls.ageModel, cls.km2SaalModel = cls.maker()

        indices = cls.df.groupby(['brand', 'model']).count()
        d = {'{}'.format(level): list(indices.xs(level).index.values) for level in indices.index.levels[0]}
        print(d)

        return anyjson.dumps(d)

    @classmethod
    def KNN(cls, carMake, carModel, productionYear, carKM):
        if cls.df is None:
            cls.df, cls.ageModel, cls.km2SaalModel = cls.maker()

        train = cls.df
        print('Train Object:')
        print(str(len(train)))

        # transform data
        kharejiIrani = make_dataset.kharejiIrani(carModel)
        fixedYear = make_dataset._fixedProductionYear(productionYear, kharejiIrani)
        # compute age
        age = datetime.date.today().year - make_dataset.saalCleaning(fixedYear)
        print('----- age')
        print(str(age))
        # from age compute km2SaalRatio
        km2SaalRatio = (carKM * 1.) / (age + 1)
        km2SaalRatioScale = cls.km2SaalModel.transform([[km2SaalRatio]])[0][0]
        age = cls.ageModel.transform([[age]])[0][0]
        print('local Age')
        print(age)
        print('----- km2sallRatioScale')
        print(km2SaalRatioScale)

        test = np.array([km2SaalRatioScale, age])

        k = 7
        model = NearestNeighbors(n_neighbors=k, metric='minkowski', leaf_size=40)
        print('Everything')
        print(carMake, carModel)


        XX = train[(train.model == carModel)]
        print('----- XX')
        print(XX)
        if len(XX) < 7:
            return {'min': None, 'max': None}

        model.fit(XX[['km2SaalRatioScale', 'age']])
        index = model.kneighbors(test.reshape(1, -1), return_distance=False)[0]

        temp = XX.iloc[index].sort_values('price')
        mid_ = temp.iloc[[int(ceil(5. / 2))]]['price'].values[0]
        min_ = temp['price'].min()
        max_ = temp['price'].max()

        if np.abs(max_ - mid_) > mid_ / 5.: temp = temp[temp.price != max_]
        if np.abs(min_ - mid_) > mid_ / 5.: temp = temp[temp.price != min_]
        print('Mid_', mid_)
        result = {'min': temp['price'].min(), 'mid': mid_, 'max': temp['price'].max()}
        return result

        # @classmethod
        # def KNN(cls, brand, carModel, year, kilometer):
        #     if cls.df is None:
        #         cls.df, cls.ageModel, cls.km2SaalModel = cls.maker()
        #
        #     train = cls.df
        #     print('Train Object:')
        #     print(str(len(train)))
        #
        #     # transform data
        #     khareji_irani = kharejiIrani(carModel)
        #     fixedYear = _fixedProductionYear(year, khareji_irani)
        #     # compute age
        #     age = datetime.date.today().year - saalCleaning(year)
        #     print('----- age')
        #     print(str(age))
        #     # from age compute km2SaalRatio
        #     km2SaalRatio = (kilometer * 1.) / (age + 1)
        #     km2SaalRatioScale = cls.km2SaalModel.transform([[km2SaalRatio]])[0][0]
        #     age = cls.ageModel.transform([[age]])[0][0]
        #     print('local Age')
        #     print(age)
        #     print('----- km2sallRatioScale')
        #     print(km2SaalRatioScale)
        #
        #     test = np.array([km2SaalRatioScale, age])
        #
        #     k = 7
        #     model = NearestNeighbors(n_neighbors=k, metric='minkowski', leaf_size=40)
        #     print('Everything')
        #     print(brand, carModel)
        #
        #     XX = train[(train.model == carModel) & (train.brand == brand)]
        #     print('----- XX')
        #     print(XX)
        #
        #     model.fit(XX[['km2SaalRatioScale', 'age']])
        #     print(test)
        #     index = model.kneighbors(test.reshape(-1, 1), return_distance=False)[0]
        #
        #     temp = XX.iloc[index].sort_values('price')
        #     mid_ = temp.iloc[[int(ceil(5. / 2))]]['price'].values[0]
        #     min_ = temp['price'].min()
        #     max_ = temp['price'].max()
        #
        #     if np.abs(max_ - mid_) > mid_ / 5.: temp = temp[temp.price != max_]
        #     if np.abs(min_ - mid_) > mid_ / 5.: temp = temp[temp.price != min_]
        #
        #     result = {'min': str(temp['price'].min()), 'max': str(temp['price'].max())}
        #     return result


class PriceBackOffice(models.Model):
    option = models.ForeignKey(Option)
    year = models.ForeignKey(Year)
    from_price = models.BigIntegerField()
    to_price = models.BigIntegerField()

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = [('option', 'year')]
        ordering = ['-modified_time']


class LogBrandOffice(models.Model):
    brand = models.ForeignKey(Brand, null=True, related_name='brand_user_log')
    user = models.ForeignKey('users.User')

    CREATE = '0'
    UPDATE = '1'
    DELETE = '2'
    OPRATION_TYPE = (
        (CREATE, _('Create')),
        (UPDATE, _('Update')),
        (DELETE, _('Delete')),
    )
    opration = models.CharField(max_length=1,
                                choices=OPRATION_TYPE,
                                blank=True,
                                null=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)


class LogModelOffice(models.Model):
    model = models.ForeignKey(Model, null=True, related_name='model_user_log')
    user = models.ForeignKey('users.User')

    CREATE = '0'
    UPDATE = '1'
    DELETE = '2'
    OPRATION_TYPE = (
        (CREATE, _('Create')),
        (UPDATE, _('Update')),
        (DELETE, _('Delete')),
    )
    opration = models.CharField(max_length=1,
                                choices=OPRATION_TYPE,
                                blank=True,
                                null=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)


class LogYearOffice(models.Model):
    year = models.ForeignKey(Year, null=True, related_name='year_user_log')
    user = models.ForeignKey('users.User')

    CREATE = '0'
    UPDATE = '1'
    DELETE = '2'
    OPRATION_TYPE = (
        (CREATE, _('Create')),
        (UPDATE, _('Update')),
        (DELETE, _('Delete')),
    )
    opration = models.CharField(max_length=1,
                                choices=OPRATION_TYPE,
                                blank=True,
                                null=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)


class LogOptionOffice(models.Model):
    option = models.ForeignKey(Option, null=True, related_name='option_user_log')
    user = models.ForeignKey('users.User')

    CREATE = '0'
    UPDATE = '1'
    DELETE = '2'
    OPRATION_TYPE = (
        (CREATE, _('Create')),
        (UPDATE, _('Update')),
        (DELETE, _('Delete')),
    )
    opration = models.CharField(max_length=1,
                                choices=OPRATION_TYPE,
                                blank=True,
                                null=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)
