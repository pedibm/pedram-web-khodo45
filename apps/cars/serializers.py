# -*- coding: utf-8 -*-

from rest_framework import serializers
import re

from .models import Model, Brand, Option, Year, PriceBackOffice


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        exclude = ['created_time', 'title_en', 'slug', 'modified_time', 'is_hidden']


class ModelSerializer(serializers.ModelSerializer):
    brand = BrandSerializer()

    class Meta:
        model = Model
        exclude = ['created_time', 'title_en', 'slug', 'modified_time']


class YearSerializer(serializers.ModelSerializer):
    class Meta:
        model = Year
        fields = ['title', 'id']


class OptionSerializer(serializers.ModelSerializer):
    model = ModelSerializer()
    years = YearSerializer(many=True)

    class Meta:
        model = Option
        exclude = ['created_time', 'modified_time']


class OptionSerializerBackOffice(serializers.ModelSerializer):
    model = ModelSerializer()

    class Meta:
        model = Option
        exclude = ['created_time', 'modified_time', 'years']


class BrandWebSerializer(serializers.ModelSerializer):
    must_used = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        exclude = ['created_time', 'title_en', 'slug', 'modified_time']

    def get_must_used(self, obj):
        try:
            Brand.MUST_USED_LIST.index(obj.slug)
            return True
        except ValueError:
            return False


class BrandBackOfficeSerializer(serializers.ModelSerializer):
    must_used = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        exclude = ['created_time', 'modified_time']

    def get_must_used(self, obj):
        try:
            Brand.MUST_USED_LIST.index(obj.slug)
            return True
        except ValueError:
            return False


class BrandCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        exclude = ['created_time', 'modified_time']


class ModelWebSerializer(serializers.ModelSerializer):
    # brand = BrandSerializer()

    class Meta:
        model = Model
        exclude = ['created_time', 'title_en', 'slug', 'modified_time']


class ModelBackOfficeSerializer(serializers.ModelSerializer):
    brand = BrandBackOfficeSerializer()

    class Meta:
        model = Model
        exclude = ['created_time', 'modified_time']


class ModelCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Model
        exclude = ['created_time', 'modified_time']


class OptionWebSerializer(serializers.ModelSerializer):
    model = ModelBackOfficeSerializer()
    years = YearSerializer(many=True)

    class Meta:
        model = Option
        exclude = ['created_time', 'modified_time', ]


class OptionWebRetriveSerializer(serializers.ModelSerializer):
    model = ModelBackOfficeSerializer()

    class Meta:
        model = Option
        exclude = ['created_time', 'modified_time', ]


class OptionCreateBackOfficeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Option
        exclude = ['created_time', 'modified_time', ]

    def validate(self, data):
        """
        Check model id not null
        """
        if "model" not in data and self.context['request'].method == 'POST':
                raise serializers.ValidationError("model require")
        return data


class BaseCarsSerializer(serializers.Serializer):
    """Serializer for the cars."""
    brand = serializers.SerializerMethodField()
    model = serializers.SerializerMethodField()
    date = serializers.SerializerMethodField()
    mobile = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    kilometer = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()

    def to_int(self, v):
        if v == '' or v is None:
            return None
        try:
            return int(float(v))
        except ValueError:
            v = re.sub(r'[^\d]+', '', v)
            print(v)
            if v == '' or v is None:
                return None
            return int(v)

    def get_kilometer(self, obj):
        return self.to_int(obj['kilometer'])

    def get_price(self, obj):
        v = self.to_int(obj['price'])
        if v < 0:
            v = 0
        return v

    def get_mobile(self, obj):
        return self.to_int(obj['mobile'])

    def get_date(self, obj):
        return self.to_int(obj['date'])

    def get_title(self, obj):
        return obj['title']

    def get_description(self, obj):
        return obj['description']

    def get_url(self, obj):
        return obj['url']


class SheypoorSerializer(BaseCarsSerializer):
    id = serializers.SerializerMethodField()

    def get_name(self, obj):
        return "sheypoor"

    def get_id(self, obj):
        return obj['id']

    def get_brand(self, obj):
        return obj['brand']

    def get_model(self, obj):
        return obj.get('model', 'دیگر')

    def get_option(self, obj):
        return None

    def get_kilometer(self, obj):
        if not obj.get('kilometer', None):  # TODO: do something better, it's Optional
            return 0
        if obj['kilometer'].find('صفر') > -1:
            return 0
        return super(SheypoorSerializer, self).get_kilometer(obj)


class BamaSerializer(BaseCarsSerializer):
    option = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return obj['result'][9]

    def get_brand(self, obj):
        return obj['result'][2]

    def get_model(self, obj):
        return obj['result'][3]

    def get_option(self, obj):
        return obj['result'][4]

    def get_date(self, obj):
        obj['date'] = obj['result'][5]
        return super(BamaSerializer, self).get_date(obj)

    def get_mobile(self, obj):
        obj['mobile'] = obj['result'][6]
        return super(BamaSerializer, self).get_mobile(obj)

    def get_name(self, obj):
        return "bama"

    def get_kilometer(self, obj):
        l = str(obj['result'][7][3])

        l = l.replace(u"کیلومتر", "")
        l = l.replace(",", "")
        l = l.replace("\r", "")
        l = l.replace("\n", "")
        l = l.replace(" ", "")
        obj['kilometer'] = l
        l = super(BamaSerializer, self).get_kilometer(obj)
        return l

    def get_price(self, obj):
        if len(obj['result'][8]) < 1: # TODO: do something better, it's Optional
            return 0
        p = str(obj['result'][8][0])
        p = p.replace(u"قیمت پیشنهادی فروشنده", "")
        p = p.replace(u"تومان", "")
        p = p.replace(",", "")
        p = p.replace("\r", "")
        p = p.replace("\n", "")
        p = p.replace(" ", "")
        p = p.replace(":", "")
        obj['price'] = p
        p = super(BamaSerializer, self).get_price(obj)
        return p

    def get_title(self, obj):
        print(obj['result'][len(obj['result'])-1])
        try:# FIXME: remove this line
            obj['title'] = obj['result'][len(obj['result'])-1]['title']
        except:
            obj['title'] = ''
        return super(BamaSerializer, self).get_title(obj)

    def get_description(self, obj):
        try: # FIXME: remove this line
            obj['description'] = obj['result'][len(obj['result'])-1]['description']
        except:
            obj['description'] = ''
        return super(BamaSerializer, self).get_description(obj)


class Serializer(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        super(Serializer, self).__init__(self, *args, **kwargs)

    def to_representation(self, obj):
        # ret = super(Serializer, self).to_representation(obj)

        if obj.initial['spider']['name'] == 'sheypoor':
            ret = SheypoorSerializer(obj.initial).data
        elif obj.initial['spider']['name'] == 'bama':
            ret = BamaSerializer(obj.initial).data
        else:
            raise Exception()
        return ret


class SuggestSerializer(serializers.Serializer):
    carMake = serializers.IntegerField()
    carModel = serializers.IntegerField()
    productionYear = serializers.IntegerField()
    carKM = serializers.IntegerField()


class PriceBackOfficeReadSerializer(serializers.ModelSerializer):
    option = OptionSerializer()
    year = YearSerializer()

    class Meta:
        model = PriceBackOffice
        exclude = ['created_time', 'modified_time']


class PriceBackOfficeSerializer(serializers.ModelSerializer):

    class Meta:
        model = PriceBackOffice
        exclude = ['created_time', 'modified_time']
