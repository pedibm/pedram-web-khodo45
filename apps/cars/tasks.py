# -*- coding: utf-8 -*-
import datetime
import re

import pandas as pd
import requests
from anyjson import loads
from celery import task
from django.conf import settings
from django.db.models import Q

from .models import OthersPrice, ProviderModel, Provider, PriceSuggest, Brand, Model, Option
from .serializers import Serializer


@task
def crawl_bama_sheypoor():
    headers = {'Authorization': settings.RIZZO_TOKEN}
    items = requests.get(settings.RIZZO_BASE_URL + '/api/tasks/pop/?count=30', headers=headers)

    items.encoding = 'utf-8'

    if items is None:
        return
    i = 0
    items = loads(items.text)
    while i < len(items):
        car = items[i]
        spider = car['spider']
        i = i + 1
        save.delay(car, spider['name'])


@task
def save(car, provider, retry=0):
    print(car)
    # FIXME: error handler
    if retry + 1 > settings.RETRY_SAVE_NEWS:
        # FIXME: notify someone, admin
        return

    se = Serializer(initial=car).data
    print(se)
    others = OthersPrice()
    key = se['brand']
    if se['model']:
        key += '-' + se['model']
    if se.get('option', None):
        key += '-' + se['option']

    p = Provider.objects.get(name=provider)
    try:
        o = ProviderModel.objects.get(provider=p, key=key)
    except ProviderModel.DoesNotExist:
        model_id, option_id = find_best_model(se['brand'], se['model'], se.get('option', None))
        o = ProviderModel(provider=p)
        o.key = key
        o.model_id = model_id  # FIXME: Nothing option
        if option_id:
            o.option_id = option_id
        o.save()

    others.model = o
    others.provider = p

    others.kilometer = se['kilometer']
    others.price = se['price']
    others.year = se['date']
    others.mobile = se['mobile']
    if not(se['mobile']):
        descriptions = se['description']
        patterns = ['09', '91', '93', '90', '92', '۰۹', '۹۱', '۹۳', '۹۰', '۹۲']
        for pattern in patterns:
            if re.search(pattern+"\\d{9}", descriptions):
                others.mobile = int(re.search(pattern+"\\d{9}", descriptions).group())

    others.description = se['description']
    others.title = se['title']
    others.url = se['url']

    others.plain = car

    others.save()


def find_best_model(brand, model, option):
    default_model_id = 907  # FIXME: change in production
    b = Brand.objects.filter(Q(title=brand) | Q(title_en=brand) | Q(slug=brand))
    if b.count() == 0:
        return default_model_id, None
    b = b.last()
    m = Model.objects.filter(Q(title=model) | Q(title_en=model) | Q(slug=model), Q(brand=b))
    if m.count() == 0:
        return default_model_id, None
    m = m.last()

    # Some provider not have a option
    if not option:
        return default_model_id, None

    o = Option.objects.filter(Q(title=option) & Q(model=m))
    if o.count() == 0:
        return m.id, None
    o = o.last()
    return m.id, o.id


@task
def logFetcher(days=5):

    start_date = (datetime.date.today() - datetime.timedelta(days=days)).isoformat()
    q = OthersPrice.objects.filter(created_time__gte=start_date).values_list("id", "price", "model__model__brand", "kilometer", "year",
                                                                        "model__model", "title", "description")

    df = pd.DataFrame(list(q))
    df.columns = ["id", "price", "brand", "kilometer", "year", "model", "title", "description"]

    df.to_csv('data.csv', encoding='utf-8-sig')

    print('Done fetching...good to go')


@task
def suggest(data):
    p = PriceSuggest()
    r = p.KNN(**data)
    return r
