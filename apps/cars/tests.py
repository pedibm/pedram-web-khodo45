"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from .models import Brand, Model, Option


class SimpleTest(TestCase):
    def test_brands(self):
        # TODO: write some real test, test serialzier and object creation
        brand = Brand()
        self.assertIsInstance(brand, Brand)

    def test_models(self):
        # TODO: write some real test, test serialzier and object creation
        model = Model()
        self.assertIsInstance(model, Model)

    def test_options(self):
        # TODO: write some real test, test serialzier and object creation
        option = Option()
        self.assertIsInstance(option, Option)
