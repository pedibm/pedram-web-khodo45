from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^appointment$', views.appointment, name='appointment'),
    url(r'^a/(?P<appointment_id>\d{0,50})', views.appointment, name='appointment_continue'),
    url(r'^cars/(?P<brand_keyword>[\w|\W]{0,50})', views.landing, name='landing'),
]
