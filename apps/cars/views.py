# -*- coding: utf-8 -*-
import datetime
import re

from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse

from apps.appointments.models import Appointment
from apps.cars.models import CarContent, Model, Option, Year, Brand
from apps.testimonials.models import Testimonials
from apps.appointments.tasks import send_notify_sms


def appointment(request, appointment_id=None):
    appoint = {}
    allow = True
    if not appointment_id:
        model = request.GET['model']
        model = Model.objects.get(id=model)

        option_id = request.GET.get('option', None)
        option = None
        if option_id:
            try:
                option = Option.objects.get(id=option_id)
            except Option.DoesNotExist:
                pass
        if not option:
            option = Option()
            option.model = model
            option.save()
        if not re.search("09" + "\\d{9}", request.GET['phone']):
            variables = {'appointment': "WrongNumber"}
            return render(request, 'main/index.html', variables)

        today = datetime.datetime.today()
        cars = Appointment.objects.filter(created_time__date=today, phone=request.GET['phone'], option=option)
        if cars.count() > 0:
            allow = False

        if allow:
            appoint = Appointment()
            appoint.year = Year.objects.get(id=request.GET['year']).title
            appoint.klm = request.GET['klm'].split(' ')[0]
            appoint.option = option
            appoint.phone = request.GET['phone']
            appoint.save()
            if request.user.is_anonymous:
                send_notify_sms.apply_async(args=(appoint.id,), countdown=900)
    else:
        try:
            appoint = Appointment.objects.get(id=appointment_id, status=Appointment.STATUS_CHECK)
        except Appointment.DoesNotExist:
            return redirect(reverse('index'))
    if appoint:
        return redirect('/step2/?id={}'.format(appoint.id))
    else:
        variables = {'appointment': "duplicated"}
        return render(request, 'main/index.html', variables)


def landing(request, brand_keyword):
    brand_keyword = brand_keyword.replace('/', '')
    try:
        brand = Brand.objects.get(title=brand_keyword)
    except Brand.DoesNotExist:
        try:
            brand = Brand.objects.get(slug=brand_keyword)
            return redirect(reverse('landing', kwargs={'brand_keyword': brand.title}), permanent=True)
        except Brand.DoesNotExist:
            return redirect(reverse('index'))

    content = CarContent.objects.get(brand=brand)
    customers = Testimonials.objects.all()
    variables = {'testimonials': customers, 'content': content}
    return render(request, 'cars/landing.html', variables)
