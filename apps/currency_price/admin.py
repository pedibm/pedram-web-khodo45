from django.contrib import admin

from .models import TgjuPrices


class PricesAdmin(admin.ModelAdmin):
    list_display = ('coin', 'euro_Price', 'gold18_ct', 'gold_Samqal', 'price_dollar_soleymani', 'rate_dollar_coin',)


admin.site.register(TgjuPrices, PricesAdmin)

