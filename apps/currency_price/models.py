from django.db import models


class TgjuPrices(models.Model):
    """"
    Store Dollar, Gold, ... price
    """

    coin = models.IntegerField(blank=True)
    euro_Price = models.IntegerField(blank=True)
    gold18_ct = models.IntegerField(blank=True)
    gold_Samqal = models.IntegerField(blank=True)
    price_dollar_soleymani = models.IntegerField(blank=True)
    rate_dollar_coin = models.IntegerField(blank=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

