# -*- coding: utf-8 -*-
from rest_framework import serializers


class TgjuSerializer(serializers.Serializer):
    sekee = serializers.SerializerMethodField()
    mesghal = serializers.SerializerMethodField()
    geram18 = serializers.SerializerMethodField()
    price_eur = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField()
    price_dollar_soleymani = serializers.SerializerMethodField()
    rate_dollar_coin = serializers.SerializerMethodField()

    def get_sekee(self, obj):
        sekee = obj['sekee'].replace(",", "")
        return sekee

    def get_mesghal(self, obj):
        mesghal = obj['mesghal'].replace(",", "")
        return mesghal

    def get_geram18(self, obj):
        geram18 = obj['geram18'].replace(",", "")
        return geram18

    def get_price_eur(self, obj):
        price_eur = obj['price_eur'].replace(",", "")
        return price_eur

    def get_time(self, obj):
        time = obj['spider']['time'].replace(",", "")
        return time

    def get_price_dollar_soleymani(self, obj):
        price_dollar_soleymani = obj['price_dollar_soleymani'].replace(",", "")
        return price_dollar_soleymani

    def get_rate_dollar_coin(self, obj):
        rate_dollar_coin = obj['rate_dollar_coin'].replace(",", "")
        return rate_dollar_coin


class CrawlerSerializer(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        self._index = kwargs.pop('initial', None)

        super(CrawlerSerializer, self).__init__(self, *args, **kwargs)

    def to_representation(self, obj):
        ret = super(CrawlerSerializer, self).to_representation(obj)

        if self._index['spider']['name'] == 'tgju':
            ret = TgjuSerializer(self._index).data
        else:
            raise Exception()
        return ret
