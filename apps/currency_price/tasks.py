# -*- coding: utf-8 -*-
import requests
from anyjson import loads
from celery import task
from django.conf import settings

from .models import TgjuPrices
from .serializers import TgjuSerializer, CrawlerSerializer


@task
def save_currency():
    headers = {'Authorization': settings.RIZZO_TOKEN}
    items = requests.get(settings.RIZZO_BASE_URL + '/api/tasks/pop/?count=1', headers=headers)

    items.encoding = 'utf-8'
    if items is None:
        return
    i = 0
    items = loads(items.text)
    # print("+++", items)
    while i < len(items):
        currency = items[i]
        spider = currency['spider']
        i = i + 1
        currency_save(currency, spider['name'])

@task
def currency_save(currency, provider=None, retry=0):

    # FIXME: error handler
    if retry + 1 > settings.RETRY_SAVE_NEWS:
        # FIXME: notify someone, admin
        return

    se = CrawlerSerializer(initial=currency).data
    # print(se)
    tgju = TgjuPrices()
    tgju.gold_Samqal = se['mesghal']
    tgju.coin = se['sekee']
    tgju.gold18_ct = se['geram18']
    tgju.euro_Price = se['price_eur']
    tgju.created_time = se['time']
    tgju.modified_time = se['time']
    tgju.price_dollar_soleymani = se['price_dollar_soleymani']
    tgju.rate_dollar_coin = se['rate_dollar_coin']

    tgju.save()
