from django.contrib import admin
from .models import Dealer
from django.utils.translation import ugettext, ugettext_lazy as _


class DealerAdmin(admin.ModelAdmin):
    # add_form_template = 'admin/auth/user/add_form.html'
    # change_user_password_template = None
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'),
         {'fields': ('first_name', 'last_name', 'email', 'mobile', 'phone', 'address', 'city', 'car_exhibition', 'work_type', 'person_trust', 'financial_strength', 'transaction_per_month', 'activity_factor', 'activity_auction',)}),
        (_('Permissions'), {'fields': ('agent', 'role', 'is_active', 'is_staff', 'is_superuser', 'is_dealer', 'groups',
                                       'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password')
        }),
    )


admin.site.register(Dealer, DealerAdmin)
