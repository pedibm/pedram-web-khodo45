from rest_framework import viewsets, routers, mixins, filters
from rest_framework.decorators import list_route, detail_route
from rest_framework.permissions import IsAuthenticated
import django_filters.rest_framework
from django.conf import settings
from apps.appointments.api import LargeResultsSetPagination
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from apps.auctions.models import Bid
from carpars.utilities import Redis
from apps.users.models import User
from apps.users.tasks import send_notify_dealer
from apps.users.serializers import UserMiniSerializer

from .models import Dealer
from .serializers import DealerSerializer, DealerEditSerializer, DealerListSerializer, DealerMobileSerializer
from django.db.models import Count, Min


class OrderFilter(django_filters.FilterSet):
    online = django_filters.rest_framework.CharFilter(method='filter_online')
    is_active = django_filters.rest_framework.BooleanFilter()

    def filter_online(self, queryset, name, value):
        r = Redis.get_instance().conn
        online_list = []
        pattern = settings.ONLINE_PREFIX.format('*')
        replace_pattern = settings.ONLINE_PREFIX.format('')
        for key in r.scan_iter(match=pattern):
            key = key.decode('utf-8')
            int_key = key.replace(replace_pattern, '')
            online_list.append(int_key)
        if value is '1':
            return queryset.filter(id__in=online_list)
        elif value is '0':
            tokens = Token.objects.all().values('user_id')
            return queryset.filter(id__in=tokens).exclude(id__in=online_list)
        elif value is '2':
            tokens = Token.objects.all().values('user_id')
            return queryset.exclude(id__in=tokens)
        return queryset

    class Meta:
        model = Dealer
        fields = {
            'id': ['exact'],
            'first_name': ['icontains', 'exact'],
            'last_name': ['icontains', 'exact'],
            'mobile': ['icontains', 'exact'],
            'is_active': ['exact'],
            'is_confirmed': ['exact'],
            'agent': ['exact'],
            'online': ['exact'],
        }


class DealerViewSet(mixins.CreateModelMixin,
                    viewsets.GenericViewSet):
    model = Dealer
    serializer_class = DealerMobileSerializer

    def perform_create(self, serializer):
        dealer = serializer.save()
        send_notify_dealer.delay(dealer)


class DealerBackOfficeViewSet(mixins.ListModelMixin,
                              mixins.RetrieveModelMixin,
                              mixins.CreateModelMixin,
                              mixins.UpdateModelMixin,
                              mixins.DestroyModelMixin,
                              viewsets.GenericViewSet):
    model = Dealer
    read_serializer_class = DealerSerializer
    write_serializer_class = DealerEditSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = LargeResultsSetPagination
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       filters.OrderingFilter,
                       )
    filter_class = OrderFilter
    ordering_fields = ('id', 'first_name', 'last_name', 'username',
                       'person_trust', 'work_type', 'address',
                       'mobile', 'bids_count', 'device_count', 'is_confirmed')

    def list(self, request, *args, **kwargs):
        self.read_serializer_class = DealerListSerializer
        return super(DealerBackOfficeViewSet, self).list(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Dealer.objects.all().annotate(
            bids_count=Count('bid'))
        user = self.request.user
        if user.role is user.ROLE_SELLS:
            queryset = queryset.filter(agent=user)
        elif user.role is user.ROLE_SELLS_SUPPORT:
            pass
        elif user.is_superuser:
            # Show All dealer to admin
            pass
        else:
            queryset = queryset.filter(id=0)
        return queryset.order_by('-id')

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.write_serializer_class
        return self.read_serializer_class

    def destroy(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return Response({"error": "Only SuperUser Could do that"}, status=403)

        instance = self.get_object()
        related_appointment = Bid.objects.filter(user=instance)
        if related_appointment.count() > 0:
            return Response({"error": "this dealer has bid. can't be deleted"}, status=400)
        return super(DealerBackOfficeViewSet, self).destroy(request, *args, **kwargs)

    @list_route(methods=['GET'])
    def agents(self, request):
        queryset = User.objects.filter(role=User.ROLE_SELLS)
        data = UserMiniSerializer(queryset, many=True)
        return Response(data.data)

    @detail_route(methods=['GET'])
    def token(self, request, pk, *args, **kwargs):
        #  TODO: Create log to know who is requested token for whom
        dealer = self.get_object()

        r = Redis.get_instance().conn
        g = r.get('login_token_' + str(dealer.mobile))
        if g:
            token = g.decode('utf-8')
        else:
            token = None
        return Response({'success': True, 'token': token})


router = routers.DefaultRouter()
router.register(r'dealers', DealerViewSet, base_name='Dealers')

back_office_router = routers.SimpleRouter()
back_office_router.register(r'dealers', DealerBackOfficeViewSet, 'Dealer')
