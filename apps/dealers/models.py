from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.users.models import User


class Dealer(User):
    """"
    Dealer Model created Based on User model, some specific dealer field defined here
    """

    SERIALIZER_BASE_EXCLUDE = ('password', 'created_time', 'modified_time', 'date_joined',
                               'verify_code', 'groups', 'user_permissions',
                               'is_superuser', 'last_login', 'is_staff', 'username')
    car_exhibition = models.CharField(max_length=200, blank=True, null=True)
    phone = models.CharField(max_length=11, blank=True, null=True)
    city = models.ForeignKey('locations.City', blank=True, null=True)
    address = models.TextField(max_length=500)

    KOREA = '0'
    JAPANESE = '1'
    GERMAN = '2'
    CHINESE = '3'
    FOREIGN = '4'
    ROUND = '5'
    BADMARKET = '6'
    HEAVY = '7'
    IRTONI = '8'
    ALL = '9'
    RENAULT = '10'
    ASSEMBLY = '11'
    IRANIANTREND = '12'
    IRANIAN = '13'
    INTERNAL = '14'
    ZEROIRANIAN = '15'
    ZEROFOREIGN = '16'

    KIND_OF_work = (
        (KOREA, _('کره ای')),
        (JAPANESE, _('ژاپنی')),
        (GERMAN, _('آلمانی')),
        (CHINESE, _('چینی')),
        (FOREIGN, _('خارجی')),
        (ROUND, _('روند')),
        (BADMARKET, _('بد بازار')),
        (HEAVY, _('سنگین')),
        (IRTONI, _('ایرتنی')),
        (ALL, _('همه')),
        (RENAULT, _('رنو')),
        (ASSEMBLY, _('مونتاژ')),
        (IRANIANTREND, _('روند ایرانی')),
        (IRANIAN, _('ایرانی')),
        (INTERNAL, _('داخلی')),
        (ZEROIRANIAN, _('صفر ایرانی')),
        (ZEROFOREIGN, _('صفر خارجی')),
    )
    work_type = models.CharField(max_length=2, choices=KIND_OF_work, blank=True)

    ONE_STAR = '0'
    TWO_STAR = '1'
    THREE_STAR = '2'
    FOUR_STAR = '3'
    FIVE_STAR = '4'
    PERSON_TRUST = (
        (ONE_STAR, _('*')),
        (TWO_STAR, _('**')),
        (THREE_STAR, _('***')),
        (FOUR_STAR, _('****')),
        (FIVE_STAR, _('******')),
    )
    person_trust = models.CharField(max_length=1, choices=PERSON_TRUST, blank=True)

    LESS_50 = '0'
    FROM_50_100 = '1'
    FROM_100_150 = '2'
    FROM_150_250 = '3'
    UP_TO_250 = '4'
    FINANCIAL_STRENGHT = (
        (LESS_50, _('<50 م ت ')),
        (FROM_50_100, _('50-100 م ت')),
        (FROM_100_150, _('100-150 م ت')),
        (FROM_150_250, _('150-250 م ت')),
        (UP_TO_250, _('>250 م ت')),
    )
    financial_strength = models.CharField(max_length=1, choices=FINANCIAL_STRENGHT, blank=True, null=True)

    FROM_1_5 = '0'
    FROM_5_10 = '1'
    FROM_10_20 = '2'
    UP_TO_20 = '3'
    TRANSACTION_PER_MONTH = (
        (FROM_1_5, _('1 - 5')),
        (FROM_5_10, _('5 - 10')),
        (FROM_10_20, _('10 - 20')),
        (UP_TO_20, _('20 به بالا')),
    )
    transaction_per_month = models.CharField(max_length=1, choices=TRANSACTION_PER_MONTH, blank=True)

    activity_factor = models.IntegerField(blank=True, null=True)
    activity_auction = models.IntegerField(blank=True, null=True)

    is_confirmed = models.BooleanField(_('is confirmed'), default=False)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)
