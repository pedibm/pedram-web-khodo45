from rest_framework import serializers
from django.conf import settings

from apps.firebase.models import Device
from apps.firebase.serializers import DeviceMiniSerializer
from apps.users.models import User
from apps.users.serializers import UserMiniSerializer
from carpars.utilities import Redis, convert_fa_numbers
from .models import Dealer


class DealerListSerializer(serializers.ModelSerializer):
    online = serializers.SerializerMethodField()
    bids_count = serializers.IntegerField()
    devices = serializers.SerializerMethodField()
    first_login = serializers.SerializerMethodField()
    device_count = serializers.SerializerMethodField()

    agent = serializers.SerializerMethodField()

    _devices = None

    class Meta:
        model = Dealer
        fields = ('id', 'last_name', 'first_name', 'username', 'person_trust', 'is_active', 'agent', 'devices',
                  'work_type', 'address', 'mobile', 'device_count', 'online', 'bids_count', 'is_confirmed',
                  'first_login')

    def get_online(self, obj):
        r = Redis.get_instance().conn
        key = settings.ONLINE_PREFIX.format(obj.id)
        if r.get(key):
            return True
        return False

    def get_agent(self, obj):
        if obj.agent:
            agent_data = UserMiniSerializer(obj.agent).data
        else:
            # FIXME: REMOVE THIS LINE ASAP
            agent_data = {"username": None, "mobile": None}
        return agent_data

    def get_devices(self, obj):
        self._devices = Device.objects.filter(user_id=obj.id)
        ser = DeviceMiniSerializer(self._devices, many=True)
        return ser.data

    def get_first_login(self, obj):
        if self._devices:
            return self._devices[0].created_time
        return None

    def get_device_count(self, obj):
        return len(self._devices)

class DealerSerializer(DealerListSerializer):
    os = serializers.SerializerMethodField()

    class Meta:
        model = Dealer
        fields = DealerListSerializer.Meta.fields + ('os', 'car_exhibition', 'agent',
                                                     'city', 'phone', 'is_active', 'email',
                                                     'transaction_per_month', 'financial_strength', 'is_confirmed')

    def get_os(self, obj):
        d = Device.objects.filter(user__username=obj.username).last()
        if d:
            return d.os
        return None


class DealerEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dealer
        exclude = Dealer.SERIALIZER_BASE_EXCLUDE

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['password'] = '!khodro45'  # Set Unusable password
        validated_data['is_dealer'] = True
        validated_data['is_active'] = True
        validated_data['agent'] = user
        validated_data['username'] = validated_data['mobile']
        return super(DealerEditSerializer, self).create(validated_data)

    def validate_mobile(self, value):
        value = convert_fa_numbers(value)
        try:
            int(value)
        except:
            raise serializers.ValidationError("Mobile number must be number not string")
        if value[0: 2] != "09":
            raise serializers.ValidationError("Mobile number must start with 09")
        if len(value) != 11:
            raise serializers.ValidationError("Mobile number digits not right")

        try:
            Dealer.objects.get(mobile=value)
            raise serializers.ValidationError("Dealer with this Mobile number already exist")
        except Dealer.DoesNotExist:
            pass
        return value


class DealerMobileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dealer
        fields = ['mobile', 'first_name', 'last_name', 'city']

    def create(self, validated_data):
        validated_data['password'] = '!khodro45'  # Set Unusable password
        validated_data['is_dealer'] = True
        validated_data['is_active'] = False
        validated_data['username'] = validated_data['mobile']
        return super(DealerMobileSerializer, self).create(validated_data)

    def validate_mobile(self, value):
        value = convert_fa_numbers(value)
        try:
            int(value)
        except:
            raise serializers.ValidationError("Mobile number must be number not string")
        if value[0: 2] != "09":
            raise serializers.ValidationError("Mobile number must start with 09")
        if len(value) != 11:
            raise serializers.ValidationError("Mobile number digits not right")
        return value


class DealerRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dealer
        exclude = Dealer.SERIALIZER_BASE_EXCLUDE + ('is_confirmed', )

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['password'] = '!khodro45'  # Set Unusable password
        validated_data['is_dealer'] = True
        validated_data['is_active'] = True
        if user.is_authenticated and user.role == User.ROLE_SELLS:
            validated_data['agent'] = user
        validated_data['username'] = validated_data['mobile']
        return super(DealerRegisterSerializer, self).create(validated_data)

    def validate_mobile(self, value):
        value = convert_fa_numbers(value)
        try:
            int(value)
        except:
            raise serializers.ValidationError("Mobile number must be number not string")
        if value[0: 2] != "09":
            raise serializers.ValidationError("Mobile number must start with 09")
        if len(value) != 11:
            raise serializers.ValidationError("Mobile number digits not right")

        try:
            Dealer.objects.get(mobile=value)
            raise serializers.ValidationError("Dealer with this Mobile number already exist")
        except Dealer.DoesNotExist:
            pass
        try:
            Dealer.objects.get(username=value)
            raise serializers.ValidationError("Dealer with this Mobile number already exist")
        except Dealer.DoesNotExist:
            pass

        return value


class DealerInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dealer
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'is_confirmed',
                  'mobile', 'agent_info', 'is_agent', 'center', 'date_joined')

    agent_info = serializers.SerializerMethodField()
    is_agent = serializers.SerializerMethodField()
    center = serializers.SerializerMethodField()
    is_confirmed = serializers.SerializerMethodField()

    def get_agent_info(self, obj):
        if obj.agent:
            agent_data = UserMiniSerializer(obj.agent).data
        else:
            # FIXME: REMOVE THIS LINE ASAP
            agent_data = {"username": None, "mobile": None}
        return agent_data

    def get_center(self, obj):
        center = {"title": u"ملاصدرا", "phone": "021-91004502", "phone_without_dash": "02191004502"}
        return center

    def get_is_agent(self, obj):
        if obj.is_staff and obj.role == User.ROLE_SELLS:
            return True
        return False

    def get_is_confirmed(self, obj):
        return obj._is_confirmed
