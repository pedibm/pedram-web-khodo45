"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APIClient

from apps.locations.models import City
from apps.users.models import User
from .models import Dealer


class APIDealerTest(APITestCase):
    _city = None

    @property
    def city(self):
        if self._city:
            return self._city
        c = City()
        c.title = 'test'
        c.save()
        self._city = c
        return c

    def test_register_api(self):
        """
        Test Register Dealer with API
        :return:
        """

        data = {'mobile': '09111111111', "city": self.city.pk, "address": "nowhere"}

        url = reverse('User-register')
        req = self.client.post(url, data, format='json')
        self.assertEqual(req.status_code, status.HTTP_200_OK)
        self.assertIsNot(req.data['success'], None)

        data = {'mobile': '09111111111', "city": self.city.pk, "address": "nowhere"}

        url = reverse('User-register')
        req = self.client.post(url, data, format='json')
        self.assertEqual(req.status_code, 400)

        dealer = Dealer.objects.last()

        self.assertEqual(dealer.agent, None)
        self.assertEqual(dealer.is_confirmed, False)

        user = 'admin'
        password = 'admin'
        agent = User()
        agent.username = user
        agent.set_password(password)
        agent.role = User.ROLE_SELLS
        agent.save()
        agent_token, created = Token.objects.get_or_create(user=agent)

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + agent_token.key)
        data = {'mobile': '09111111112', "city": self.city.pk, "address": "nowhere",
                "first_name": "first name test", "last_name": "last name test"}

        url = reverse('User-register')
        req = client.post(url, data, format='json')
        self.assertEqual(req.status_code, 200)

        dealer = Dealer.objects.last()

        self.assertNotEqual(dealer.agent, None)
        self.assertEqual(dealer.is_confirmed, False)

        dealer_token, created = Token.objects.get_or_create(user=dealer)

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + dealer_token.key)

        url = reverse('User-dealer')
        req = client.get(url, format='json')
        self.assertEqual(req.status_code, 200)
        self.assertIsNotNone(req.data['id'])
        self.assertIsNotNone(req.data['username'])
        self.assertIsNotNone(req.data['first_name'])
        self.assertIsNotNone(req.data['last_name'])
        self.assertIsNotNone(req.data['mobile'])
        self.assertIsNotNone(req.data['date_joined'])
        self.assertIsNotNone(req.data['is_confirmed'])
        self.assertIsNotNone(req.data['is_agent'])

        # this dealer must not be confirmed
        self.assertEqual(req.data['is_confirmed'], False)

        url = reverse('Dealer-detail', kwargs={"pk": dealer.id})
        data = req.data
        data.pop('mobile')
        data['address'] = 'No Where'
        data['is_confirmed'] = True
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + agent_token.key)
        req = client.put(url, data, format='json')
        self.assertEqual(req.status_code, 200)
        self.assertEqual(req.data['is_confirmed'], True)

