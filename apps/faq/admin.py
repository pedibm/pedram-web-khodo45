# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Faq


class FaqAdmin(admin.ModelAdmin):
    list_display = ('answer', 'response',)
    search_fields = ('answer', 'response',)


admin.site.register(Faq, FaqAdmin)