# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

from django.db import models


class Faq(models.Model):
    """"
    Used in Old Site, No Need now
    """
    answer = models.TextField(max_length=300)
    response = models.TextField(max_length=500)

    def __str__(self):
        return self.answer

    def __unicode__(self):
        return self.answer
