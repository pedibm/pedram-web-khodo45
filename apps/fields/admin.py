from django.contrib import admin
from import_export import resources
from import_export.fields import Field
from import_export.admin import ImportExportActionModelAdmin
from django.utils.translation import ugettext_lazy as _

from carpars.utilities import admin_changelist_link, admin_link
from .models import Question, Answer
from django.db.models import Q


class QuestionResource(resources.ModelResource):
    question = Field()

    class Meta:
        model = Question
        exclude = ('order', 'title', 'private', 'segment', 'field_type', 'answers', 'created_time', 'modified_time')
        export_order = ('id',)

    def export(self, queryset=None, *args, **kwargs):
        queryset = Question.objects.filter(Q(field_type=Question.TYPE_IMAGE_ADDITIONAL)
                                           | Q(field_type=Question.TYPE_CHECK)
                                           | Q(field_type=Question.TYPE_BOOL)).exclude(
            question__field_type=Question.TYPE_POSITIVE)
        return super(QuestionResource, self).export(queryset, *args, **kwargs)

    def dehydrate_question(self, obj):
        str = ""
        str_list = []
        qs = obj
        while qs:
            if qs.question:
                str_list.append(qs.title)
                qs = qs.question
            else:
                break
        str_list.reverse()
        i = 0
        for s in str_list:
            i += 1
            if i == len(str_list):
                str = str + " : " + s
            else:
                str = str + " " + s
        if str:
            return '%s' % (str,)
        else:
            return obj.title


class AnswerInline(admin.TabularInline):
    model = Question.answers.through


class QuestionAdmin(ImportExportActionModelAdmin):
    list_display = ('title', 'segment', 'order', 'field_type', 'question_count', 'answer_count', 'question')
    search_fields = ('title', 'field_type',)
    list_filter = ('segment',)
    filter_horizontal = ('answers',)
    readonly_fields = ("created_time", "modified_time",)
    raw_id_fields = ('question',)
    # exclude = ('answers',)
    # inlines = [
    #     AnswerInline,
    # ]
    obj = None
    resource_class = QuestionResource

    @admin_changelist_link(
        'answers',
        _('Answers'),
        query_string=lambda q: 'question={}'.format(q.pk)
    )
    def answer_count(self, answers):
        return answers.count()

    @admin_changelist_link(
        'questions',
        _('Questions'),
        query_string=lambda q: 'question_id={}'.format(q.pk)
    )
    def question_count(self, questions):
        return questions.count()

    def get_object(self, request, object_id, from_field=None):
        # Hook obj for use in formfield_for_manytomany
        self.obj = super(QuestionAdmin, self).get_object(request, object_id, from_field)
        return self.obj

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # TODO: fix query set
        # if db_field.name == "answers" and getattr(self, 'obj', None):
        #     kwargs["queryset"] = self.obj.answers
        return super(QuestionAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('title', 'order', 'default', 'question_count')
    search_fields = ('title',)
    filter_horizontal = ('questions',)
    readonly_fields = ("created_time", "modified_time",)

    obj = None

    @admin_changelist_link('questions', _('Child Question'),
                           query_string=lambda q: 'answer={}'.format(q.pk)
                           )
    def question_count(self, question):
        return question.count()

    def get_object(self, request, object_id, from_field=None):
        # Hook obj for use in formfield_for_manytomany
        self.obj = super(AnswerAdmin, self).get_object(request, object_id, from_field)
        return self.obj

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # TODO: fix query set
        # if db_field.name == "questions" and getattr(self, 'obj', None):
        #     kwargs["queryset"] = self.obj.questions
        return super(AnswerAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
