from rest_framework import viewsets, routers, mixins, status
from .models import Question, Answer
from .serializers import QuestionSerializer, QuestionSerializerEdit, AnswerSerializerEdit, QuestionSerializerCreate
from rest_framework.response import Response



class QuestionViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """list of any question in the system about cars."""
    model = Question
    serializer_class = QuestionSerializer
    queryset = Question.objects.all().exclude(is_hidden=True)


class QuestionBackOfficeViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet,
                                mixins.CreateModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    """list of any question in the system about cars."""
    model = Question
    serializer_class = QuestionSerializer
    queryset = Question.objects.all().exclude(is_hidden=True)

    def partial_update(self, request, *args, **kwargs):
        self.serializer_class = QuestionSerializerEdit
        return super(QuestionBackOfficeViewSet, self).partial_update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        self.serializer_class = QuestionSerializerCreate
        return super(QuestionBackOfficeViewSet, self).create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_hidden = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AnswerBackOfficeViewSet(mixins.UpdateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    model = Answer
    serializer_class = AnswerSerializerEdit
    queryset = Answer.objects.all()


router = routers.DefaultRouter()
router.register(r'questions', QuestionViewSet, 'Questions')

back_office_router = routers.SimpleRouter()
back_office_router.register(r'questions', QuestionBackOfficeViewSet, 'Questions')
back_office_router.register(r'answer', AnswerBackOfficeViewSet, 'Answer')
