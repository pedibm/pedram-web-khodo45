# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-07-10 13:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fields', '0008_auto_20180704_0643'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='private',
            field=models.BooleanField(default=False),
        ),
    ]
