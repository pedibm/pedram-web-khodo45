# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2019-04-27 07:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fields', '0012_auto_20180807_1141'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='help',
        ),
        migrations.AddField(
            model_name='question',
            name='color',
            field=models.CharField(default='404344', help_text='Question ', max_length=6, verbose_name='Color'),
        ),
        migrations.AlterField(
            model_name='question',
            name='field_type',
            field=models.CharField(choices=[('0', 'Integer'), ('1', 'String'), ('2', 'Boolean'), ('3', 'Option'), ('4', 'List Options'), ('5', 'Check Box'), ('6', 'Rating'), ('7', 'Main Images'), ('8', 'Additional Images'), ('9', 'PopUp list'), ('10', 'Tyre'), ('11', 'Positive Comments'), ('12', 'License Plate'), ('13', 'Text Area'), ('14', 'API CALL')], default='0', max_length=2),
        ),
    ]
