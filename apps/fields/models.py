from django.db import models
from django.utils.translation import ugettext_lazy as _


class Question(models.Model):
    TYPE_INTEGER = '0'
    TYPE_STRING = '1'
    TYPE_BOOL = '2'
    TYPE_OPTION = '3'
    TYPE_OPTIONS = '4'
    TYPE_CHECK = '5'
    TYPE_RATING = '6'
    TYPE_IMAGE_MAIN = '7'
    TYPE_IMAGE_ADDITIONAL = '8'
    TYPE_POPUP = '9'
    TYPE_TIER = '10'
    TYPE_POSITIVE = '11'
    TYPE_PLATE = '12'
    TYPE_TEXTAREA = '13'
    TYPE_API_CALL = '14'

    FIELD_TYPE = (
        (TYPE_INTEGER, _('Integer')),
        (TYPE_STRING, _('String')),
        (TYPE_BOOL, _('Boolean')),
        (TYPE_OPTION, _('Option')),
        (TYPE_OPTIONS, _('List Options')),
        (TYPE_CHECK, _('Check Box')),
        (TYPE_RATING, _('Rating')),
        (TYPE_IMAGE_MAIN, _('Main Images')),
        (TYPE_IMAGE_ADDITIONAL, _('Additional Images')),
        (TYPE_POPUP, _('PopUp list')),
        (TYPE_TIER, _('Tyre')),
        (TYPE_POSITIVE, _('Positive Comments')),
        (TYPE_PLATE, _('License Plate')),
        (TYPE_TEXTAREA, _('Text Area')),
        (TYPE_API_CALL, _('API CALL')),
    )

    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Field Title'),
    )
    order = models.IntegerField(null=True, blank=True)
    private = models.BooleanField(default=False, blank=True)
    is_hidden = models.BooleanField(default=False, blank=True)

    segment = models.ForeignKey('segments.Segment', null=True, blank=True)
    field_type = models.CharField(max_length=2,
                                  choices=FIELD_TYPE,
                                  default=TYPE_INTEGER)

    answers = models.ManyToManyField('Answer', blank=True)
    question = models.ForeignKey('self', related_name='questions',
                                 blank=True, null=True, help_text=_('Parent question'))

    color = models.CharField(
        _('Color'), max_length=6,
        help_text=_('Question '),
        default='404344'
    )

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}_{}".format(self.title, self.field_type)

    def question_count(self):
        return self.questions.count()

    def answer_count(self):
        return self.answers.count()

    class Meta:
        ordering = ['order']


class Answer(models.Model):
    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Answer Title'),
    )

    questions = models.ManyToManyField('Question', blank=True)
    order = models.IntegerField(null=True, blank=True)
    default = models.BooleanField(default=False)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def question_count(self):
        return self.questions.count()

    class Meta:
        ordering = ['order', 'title']
