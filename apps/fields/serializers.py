# -*- coding: utf-8 -*-

from rest_framework import serializers

from apps.images.models import Image
from apps.images.serializers import Image100x100Serializer
from apps.inspections.models import InspectionData
from .models import Question, Answer


class BaseQuestionSerializer(serializers.Serializer):
    id = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    color = serializers.SerializerMethodField()
    field_type = serializers.SerializerMethodField()
    questions = serializers.SerializerMethodField()

    def get_questions(self, obj):
        questions = obj.questions.all().exclude(is_hidden=True)
        if questions.count() < 1:
            return

        return QuestionSerializer(questions, many=True, inspection=self._inspection).data

    def __init__(self, *args, **kwargs):
        self._inspection = kwargs.pop('inspection', None)
        super(BaseQuestionSerializer, self).__init__(*args, **kwargs)

    def get_title(self, obj):
        return obj.title

    def get_id(self, obj):
        return obj.id

    def get_color(self, obj):
        return obj.color

    def get_field_type(self, obj):
        return obj.field_type


class QuestionWithAnswerSerializer(BaseQuestionSerializer):
    answers = serializers.SerializerMethodField()

    def get_answers(self, obj):
        answers = obj.answers.all()
        if len(answers) > 0:
            try:
                v = InspectionData.objects.get(inspection_id=self._inspection, question=obj.id)
                l = v.answer
                i = 0
                while i < len(answers):
                    if answers[i].id == int(l):  # FIXME: handle list too
                        answers[i].default = True
                    elif answers[i].default:  # for option, boolean and true false we store int, so just one item could
                        #  be true
                        answers[i].default = False
                    i = i + 1
            except InspectionData.DoesNotExist:
                pass

        return AnswerSerializer(answers, many=True, inspection=self._inspection, question=obj).data


class QuestionPopUpSerializer(BaseQuestionSerializer):
    pass


class QuestionWithoutAnswerSerializer(BaseQuestionSerializer):
    value = serializers.SerializerMethodField()

    def get_value(self, obj):
        try:
            v = InspectionData.objects.get(inspection_id=self._inspection, question=obj)
        except InspectionData.DoesNotExist:
            return None
        return v.answer


class QuestionAPICallSerializer(BaseQuestionSerializer):
    value = serializers.SerializerMethodField()

    def get_value(self, obj):
        try:
            v = InspectionData.objects.get(inspection_id=self._inspection, question=obj)
        except InspectionData.DoesNotExist:
            return None
        return v.answer


class QuestionWithImageSerializer(BaseQuestionSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        try:
            v = InspectionData.objects.get(inspection_id=self._inspection, question=obj)
        except InspectionData.DoesNotExist:
            return None
        try:
            i = Image.objects.get(id=v.answer)
        except Image.DoesNotExist:
            return None
        return Image100x100Serializer(i).data


class QuestionWithImagesSerializer(BaseQuestionSerializer):
    images = serializers.SerializerMethodField()

    def get_images(self, obj):
        try:
            v = InspectionData.objects.get(inspection_id=self._inspection, question=obj)
        except InspectionData.DoesNotExist:
            return None
        v.answer = v.answer.split(',')
        i = Image.objects.filter(id__in=v.answer)
        return Image100x100Serializer(i, many=True).data


class QuestionSerializer(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        self._inspection = kwargs.pop('inspection', None)
        super(QuestionSerializer, self).__init__(*args, **kwargs)

    def to_representation(self, obj):
        super(QuestionSerializer, self).to_representation(obj)
        serializer_class = QuestionWithoutAnswerSerializer
        if obj.field_type in [Question.TYPE_OPTIONS, Question.TYPE_OPTION, Question.TYPE_BOOL, Question.TYPE_CHECK]:
            serializer_class = QuestionWithAnswerSerializer
        elif obj.field_type in [Question.TYPE_IMAGE_MAIN, Question.TYPE_IMAGE_ADDITIONAL]:
            serializer_class = QuestionWithImageSerializer
        elif obj.field_type is Question.TYPE_POPUP:
            serializer_class = QuestionPopUpSerializer
        elif obj.field_type is Question.TYPE_API_CALL:
            serializer_class = QuestionAPICallSerializer

        ret = serializer_class(obj, inspection=self._inspection).data

        return ret


class AnswerSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()

    class Meta:
        model = Answer
        exclude = ['created_time', ]

    def __init__(self, *args, **kwargs):
        self._question = kwargs.pop('question', None)
        self._inspection = kwargs.pop('inspection', None)
        super(AnswerSerializer, self).__init__(*args, **kwargs)

    def get_questions(self, obj):
        filtered_questions = obj.questions.all().exclude(is_hidden=True)
        return QuestionSerializer(filtered_questions, many=True, inspection=self._inspection).data


class QuestionSerializerEdit(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ['title', 'color']


class QuestionSerializerCreate(serializers.ModelSerializer):
    answer = serializers.IntegerField(required=True)
    # answer = serializers.SerializerMethodField()

    class Meta:
        model = Question
        fields = '__all__'

    # def get_answer(self):
    #     return Answer.objects.get(id=self.initial_data['answer'])

    def create(self, validated_data):
        parent_question = validated_data.get('question', None)
        # parent_answer = self.get_answer()
        answer_id = validated_data.pop('answer', None)
        if parent_question is None and answer_id is None:
            raise Exception('error')
        field_type = validated_data.get('field_type')
        child = None
        parent_answer = None
        if parent_question is not None:
            child = parent_question.questions.all().exclude(is_hidden=True).first()
        elif answer_id is not None:
            try:
                parent_answer = Answer.objects.get(id=answer_id)
            except Answer.DoesNotExist:
                raise Exception('error')
            child = parent_answer.questions.all().exclude(is_hidden=True).first()
        if child is not None and child.field_type != field_type:
            raise Exception('error')
        result_instance = super(QuestionSerializerCreate,self).create(validated_data)
        if parent_answer:
            parent_answer.questions.add(result_instance)
            parent_answer.save()
        if field_type in ['2', '5']:
            yes_value = False
            no_value = False
            if field_type is '2':
                yes_value = True
            else:
                no_value = True
            a1 = Answer.objects.create(title='بلی', order=1, default=yes_value)
            result_instance.answers.add(a1)
            a2 = Answer.objects.create(title='خیر', order=2, default=no_value)
            result_instance.answers.add(a2)
        return result_instance


class AnswerSerializerEdit(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ['title']
