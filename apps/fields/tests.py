"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from .models import Question, Answer


class SimpleTest(TestCase):
    def test_question(self):
        # TODO: write some real test, test serialzier and object creation
        question = Question()
        self.assertIsInstance(question, Question)

    def test_answer(self):
        # TODO: write some real test, test serialzier and object creation
        answer = Answer()
        self.assertIsInstance(answer, Answer)
