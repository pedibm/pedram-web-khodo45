from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.utils.html import format_html
from django.core.urlresolvers import reverse
from django.conf.urls import url
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse

from .forms import PushNotificationForm
from .models import Device


class DeviceAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'device', 'account_actions')
    list_filter = ('created_time',)
    search_fields = ('device', 'token',)
    ordering = ('-id',)
    readonly_fields = ("created_time",)
    raw_id_fields = ('user',)


    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<device_id>.+)/notification/$',
                self.admin_site.admin_view(self.push_notification),
                name='push_notification',
            ),
        ]
        return custom_urls + urls

    def account_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Push Notification</a>&nbsp;',
            reverse('admin:push_notification', args=[obj.pk]),
        )

    account_actions.short_description = 'Account Actions'
    account_actions.allow_tags = True

    def push_notification(self, request, device_id, *args, **kwargs):
        return self.process_action(
            request=request,
            device_id=device_id,
            action_form=PushNotificationForm,
            action_title='Push Notification',
        )

    def process_action(
        self,
        request,
        device_id,
        action_form,
        action_title
    ):
        device = self.get_object(request, device_id)
        if request.method != 'POST':
            form = action_form()
        else:
            form = action_form(request.POST)
            if form.is_valid():
                form.save(device, request.user)

                self.message_user(request, 'Push Notification It\'s On his Way, Wait for it :)')
                url = reverse(
                    'admin:firebase_device_change',
                    args=[device.pk],
                    current_app=self.admin_site.name,
                )
                return HttpResponseRedirect(url)
        context = self.admin_site.each_context(request)
        context['opts'] = self.model._meta
        context['form'] = form
        context['device'] = device
        context['title'] = action_title
        return TemplateResponse(
            request,
            'firebase/device_action.html',
            context,
        )


admin.site.register(Device, DeviceAdmin)
