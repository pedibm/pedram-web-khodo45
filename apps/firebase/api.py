from rest_framework import viewsets, routers, mixins
from rest_framework.permissions import IsAuthenticated

from .models import Device
from .serializers import DeviceSerializer


class DeviceViewSet(mixins.CreateModelMixin,
                    viewsets.GenericViewSet):
    """
    Device resource [Authentication needed].
    """

    model = Device
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        # Include the user attribute directly, rather than from request data.
        try:
            instance = Device.objects.get(token=serializer.validated_data['token'])
            instance = Device(pk=instance.pk, user=self.request.user, **serializer.validated_data)
            instance.save()
        except Device.DoesNotExist:
            instance = serializer.save(user=self.request.user)
        serializer.instance = instance


router = routers.DefaultRouter()
router.register(r'devices', DeviceViewSet, base_name='Device')
