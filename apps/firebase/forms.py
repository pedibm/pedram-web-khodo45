from django import forms
import json
from .tasks import send_push


class PushNotificationForm(forms.Form):
    title = forms.CharField(
        required=False,
    )
    body = forms.CharField(
        required=False,
    )
    data = forms.CharField(widget=forms.Textarea, required=False)

    def clean_data(self):
        data = self.cleaned_data['data']
        if data:
            try:
                data = json.loads(data)
            except Exception as e:
                print(e)
                raise forms.ValidationError("Data Must be JSON Object")
        return data

    field_order = (
        'title',
        'body',
        'data',
    )

    def save(self, device, user):
        title = self.cleaned_data['title']
        body = self.cleaned_data['body']
        data = self.cleaned_data['data']
        send_push.delay(title, body, user=device.user, data_message=data)
        return None
