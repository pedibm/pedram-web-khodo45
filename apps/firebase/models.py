from django.db import models


class Device(models.Model):
    """"
    Register user device and firebase token, store for report and push-notification
    """
    user = models.ForeignKey('users.User')
    token = models.CharField(max_length=152, unique=True)
    build_number = models.IntegerField(null=True, blank=True)
    udid = models.CharField(max_length=250, null=True, blank=True)
    device = models.CharField(max_length=250, null=True, blank=True)
    os = models.CharField(max_length=250, null=True, blank=True)

    created_time = models.DateTimeField(auto_now_add=True,
                                        db_index=True)
