from rest_framework import serializers

from .models import Device


class DeviceSerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=152)

    class Meta:
        model = Device
        exclude = ('user',)


class DeviceMiniSerializer(serializers.ModelSerializer):

    class Meta:
        model = Device
        fields = ('device', 'os', 'build_number')
