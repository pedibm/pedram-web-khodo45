import datetime
from celery import task
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db.models.query import QuerySet
from pyfcm import FCMNotification

from apps.appointments.tasks import send_custom_sms
from apps.users.models import User


@task
def send_push(title, body, user=None, data_message=None, time_to_live=None, instance=None):
    if instance:
        instance.refresh_from_db()
        if instance.countdown() <= 0:  # if there is no time, auction canceled before send message
            return True

    if settings.DEBUG:
        body = settings.STAGING_NOTIF_IDENTIFIER + body
    data = {
        "message_title": title,
        "message_body": body,
        # "message_icon": obj.icon,
        # "click_action": obj.action
        "sound": "default",
    }
    if data_message:
        data['data_message'] = data_message

    if time_to_live:
        data['time_to_live'] = time_to_live

    gte_time = datetime.date.today() - relativedelta(days=1)
    lte_time = datetime.date.today() - relativedelta(days=1)

    if user:
        list_devices = get_devices(gte_time, lte_time, user)
    else:
        list_devices = get_devices(gte_time, lte_time)
    device_tokens = {}
    b = -1
    device_tokens[b] = []
    for i, item in enumerate(list_devices):
        if (i % 1000) == 0:
            b = b + 1
            device_tokens[b] = []

        device_tokens[b].append(item[0])

    print(device_tokens)
    send_push_message(data, device_tokens)


@task
def send_push_sms(title, body, instance=None):
    if instance:
        instance.refresh_from_db()
        if instance.countdown() <= 0:  # if there is no time, auction canceled before send message
            return True
    data = {
        "message_title": title,
        "message_body": body
    }
    phone_numbers = get_phone_numbers()
    numbers = []
    for i, item in enumerate(phone_numbers):
        if (i % 20) == 0:
            send_sms_message(data, numbers)

            numbers = []

        numbers.append(item)
    if 20 > len(numbers) > 0:
        send_sms_message(data, numbers)


def get_devices(gte_time, lte_time, user=None):
    from .models import Device
    data = Device.objects.all().exclude(token__startswith='2019').values_list('token')
    if user:
        if type(user) is User:
            data = data.filter(user=user)
        elif type(user) is QuerySet:
            data = data.filter(user__in=user)
    return data


def get_phone_numbers(user=None):
    from .models import Device
    data = Device.objects.filter(user__mobile__isnull=False, user__is_active=True).exclude(user__mobile='').values_list(
        'user__mobile').distinct('user_id')
    if user:
        if type(user) is User:
            data = data.filter(user=user)
        elif type(user) is QuerySet:
            data = data.filter(user__in=user)
    numbers = [p[0] for p in data]
    return numbers


def send_sms_message(data, dealers):
    text = "{}\n{}".format(data['message_title'], data['message_body'])

    if len(dealers) > 0:
        send_custom_sms(dealers, text, "new_auction")


def send_push_message(data, device_tokens):
    push_service = FCMNotification(api_key=settings.FCM_TOKEN)

    notification_extra_kwargs = {
        'android_channel_id': "default"
    }

    for i, tokens in device_tokens.items():
        if len(tokens) > 0:
            result = push_service.notify_multiple_devices(registration_ids=tokens,
                                                          extra_notification_kwargs=notification_extra_kwargs,
                                                          content_available=True, **data)
            print(result)


@task
def send_push_5min(instance):
    instance.refresh_from_db()
    if instance.countdown() > (60 * 4):  # if there is more than 4 minutes until end, return and set new task
        send_push_5min.apply_async(args=(instance,), countdown=instance.countdown() - (60 * 5))
        return True
    if instance.countdown() <= 0:  # if there is no time, auction canceled before end time
        return True

    title = 'تنها پنج دقیقه مانده🕚'
    body = instance.get_notif_body()

    # if no one has bid yet, it's just crash and get out ;)
    user = instance.bid_set.last().user
    users = instance.bid_set.all().distinct('user').exclude(user=user).values('user')
    data_message = {'auction_id': instance.id, 'inspection_id': instance.inspection_id}

    send_push(title, body, users, data_message=data_message, time_to_live=instance.countdown())


@task
def send_push_end_time(instance):
    instance.refresh_from_db()
    if instance.countdown() > 0:  # if there is an time
        send_push_end_time.apply_async(args=(instance,), countdown=instance.countdown())
        return True

    title = 'اتمام زمان، شروع مذاکره ⛔'
    body = instance.get_notif_body()

    user = instance.bid_set.last().user
    data_message = {'auction_id': instance.id, 'inspection_id': instance.inspection_id}

    send_push(title, body, user, data_message=data_message)


def sms():
    # Send Last Message To all dealer
    from apps.dealers.models import Dealer
    from apps.appointments.tasks import send_custom_sms
    from apps.appointments.models import Message
    m = Message.objects.last()

    dc = Dealer.objects.filter(is_confirmed=True, is_dealer=True).values('username')
    d = [p['username'] for p in dc]

    ps = []
    l = []
    for p in d:
        if len(l) > 19:
            ps.append(l)
            l = []
        l.append(p)

        for s in ps: send_custom_sms(s, m.text, 'new_auction')
