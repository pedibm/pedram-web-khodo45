from rest_framework import viewsets, routers, mixins

from apps.inspections.models import InspectionData, Inspection
from .models import Image
from .serializers import ImageSerializer


class ImageViewSet(mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    model = Image
    serializer_class = ImageSerializer
    queryset = Image.objects.all()

    def create(self, request, *args, **kwargs):
        """
        Create a new image
            ---
            parameters:
                - name: file
                  description: you must choose an image file.
                  required: True
                  type: file
        """
        return super(ImageViewSet, self).create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        """Delete an image with its pk."""
        #  TODO: delete cache for question segment
        question = request.GET.get('question', None)
        if question:
            try:
                idata = InspectionData.objects.get(
                    question_id=question, answer=self.get_object().id, inspection__status=Inspection.STATUS_NEW)
                idata.delete()
            except InspectionData.DoesNotExist:
                pass
        return super(ImageViewSet, self).destroy(request, *args, **kwargs)


router = routers.DefaultRouter()
router.register(r'images', ImageViewSet, 'Images')
