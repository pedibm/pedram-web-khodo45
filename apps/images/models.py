from django.db import models


class Image(models.Model):
    """
    All Image uploaded and stored by thid model, and we user this model PK as reference
    """

    file = models.ImageField(upload_to="images")

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)
