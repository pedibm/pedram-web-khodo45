from imagekit import ImageSpec
from imagekit.processors import ResizeToFill, ResizeToFit, TrimBorderColor, Adjust


class Thumbnail(ImageSpec):
    processors = [ResizeToFill(100, 50)]
    format = 'JPEG'
    options = {'quality': 60}


class Watermark(object):
    def process(self, image):
        # Code for adding the watermark goes here.
        return image


class CarResize500(ImageSpec):

    format = 'JPEG'
    options = {'quality': 80}

    @property
    def processors(self):
        w = self.source.width
        h = self.source.height
        o = int(h * 500 / w)
        return [ResizeToFill(500, o),
                # TrimBorderColor(),
                Adjust(contrast=1.1, sharpness=1.2),
                # Watermark(),
                ]
