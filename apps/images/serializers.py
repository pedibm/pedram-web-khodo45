from django.conf import settings
from rest_framework import serializers
from imagekit.cachefiles import ImageCacheFile

from carpars.utilities import Base64ImageField
from .models import Image
from .processors import CarResize500

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('file', 'id')

    file = Base64ImageField(max_length=None, use_url=True)


class Image100x100Serializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('file', 'id')

    file = serializers.SerializerMethodField()

    def get_file(self, obj):
        return settings.BASE_URL + obj.file.url


class ImageDealerSerializer(serializers.Serializer):
    class Meta:
        fields = ('file', 'file_high', 'title', 'id', 'color')

    id = serializers.SerializerMethodField()
    file = serializers.SerializerMethodField()
    file_high = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    color = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj['id']

    def get_file(self, obj):
        image_generator = ImageCacheFile(CarResize500(source=obj['file']))
        image_generator.generate()
        return settings.BASE_URL + image_generator.url

    def get_file_high(self, obj):
        return settings.BASE_URL + obj['file'].url

    def get_title(self, obj):
        return obj['title']

    def get_color(self, obj):
        if obj.get('question_child', None):
            return obj['question_child'].color
        return None


class ImageAuctionSerializer(serializers.Serializer):
    class Meta:
        fields = ('id', 'url',)

    id = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj['id']

    def get_url(self, obj):
        image_generator = ImageCacheFile(CarResize500(source=obj['url']))
        image_generator.generate()
        return settings.BASE_URL + image_generator.url


class RFDealerSerializer(serializers.Serializer):
    from apps.segments.serializers import SegmentMiniSerializer

    class Meta:
        fields = ('images', 'segment')

    images = ImageDealerSerializer(many=True)
    segment = SegmentMiniSerializer()
