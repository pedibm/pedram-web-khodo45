from django.contrib import admin
from django.utils.html import format_html
from .models import Inspection, InspectionData, InspectionView


class InspectionAdmin(admin.ModelAdmin):
    list_display = ('status', 'modified_time', 'actions_html')
    search_fields = ('status', 'appointment__name')
    list_filter = ('status',)
    readonly_fields = ("created_time", "modified_time",)
    raw_id_fields = ('appointment',)

    def actions_html(self, obj):
        return format_html(
            '<button class="button btn" type="button" onclick="window.open(\'/view/{pk}/\')" > Publish </button>',
            pk=obj.id)

    actions_html.allow_tags = True
    actions_html.short_descgription = "Actions"


class InspectionDataAdmin(admin.ModelAdmin):
    list_display = ('inspection', 'question', 'answer', 'modified_time')
    search_fields = ('answer', 'question__title')
    readonly_fields = ("created_time", "modified_time",)
    raw_id_fields = ('inspection', 'question')


class InspectionViewAdmin(admin.ModelAdmin):
    list_display = ('inspection', 'user', 'created_time')
    search_fields = ('inspection_id', 'inspection__slug', 'user_id', 'user__username')
    readonly_fields = ("created_time",)
    raw_id_fields = ('inspection', 'user')


admin.site.register(Inspection, InspectionAdmin)
admin.site.register(InspectionData, InspectionDataAdmin)
admin.site.register(InspectionView, InspectionViewAdmin)
