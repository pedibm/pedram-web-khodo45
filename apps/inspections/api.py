import json
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.http import Http404
from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, routers, mixins
from rest_framework.decorators import detail_route
from rest_framework.response import Response
import django_filters.rest_framework
from apps.cars.models import OptionInspection
from apps.fields.models import Question
from apps.inspections.tasks import submit_view
from apps.segments.models import Segment
from apps.segments.serializers import SegmentSerializer, SegmentDealerSerializer
from carpars.utilities import Redis

from apps.appointments.models import Appointment
from .models import Inspection, InspectionData
from .serializers import InspectionSerializer, InspectionListSerializer, BasePostSerializer, InspectionDealerSerializer, \
    InspectionSummarySerializer, InspectionDealerListSerializer, InspectionDealerImageSerializer, \
    InspectionDealerPositiveCommentsSerializer, VIEWS_PLUS
from django.db import transaction
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import BasePermission, IsAuthenticated
from collections import OrderedDict


class IsInspector(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == request.user.ROLE_INSPECTOR


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'size'

    def get_paginated_response(self, data):
        status = self.request.query_params['status']
        count = Inspection.objects.filter(status=status).count()
        return Response(OrderedDict([
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('count', count),
            ('results', data),
        ]))


class OrderFilter(django_filters.FilterSet):
    q = django_filters.rest_framework.CharFilter(method='appointment_name')

    def appointment_name(self, queryset, name, value):
        return queryset.filter(Q(appointment__name__icontains=value) |
                               Q(appointment__id__icontains=value))

    class Meta:
        model = Inspection
        fields = {
            'q': ['exact'],
            'status': ['exact'],
            'id': ['lt'],
        }


class InspectionViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated, IsInspector)
    model = Inspection
    serializer_class = InspectionSerializer
    queryset = Inspection.objects.all().order_by('-id')
    prefix = 'inspection_'
    pagination_class = LargeResultsSetPagination
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_class = OrderFilter

    def retrieve(self, request, *args, **kwargs):
        """Retrive an inspection for inspection app by pk"""
        return super(InspectionViewSet, self).retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        """list the inspections for inspection app"""
        self.serializer_class = InspectionListSerializer
        return super(InspectionViewSet, self).list(request, *args, **kwargs)

    @detail_route(methods=['POST'])
    def done(self, request, pk, *args, **kwargs):
        """By run this api all the field are fix but status field set to '1'"""
        obj = self.get_object()
        if obj.appointment.status == Appointment.STATUS_CONFIRM and obj.status == Inspection.STATUS_NEW:
            obj.status = Inspection.STATUS_AUDIT
            obj.save()
            obj.appointment.change_state_status(None, Appointment.STATUS_FINISH_INSPECTOR, request.user)
            return Response({'success': True})
        else:
            return Response({'success': False})

    @detail_route(methods=['GET'])
    def summary(self, request, pk, *args, **kwargs):
        """show the summary for inspection table"""
        obj = self.get_object()
        d = InspectionSummarySerializer(obj)
        return Response(d.data)

    def options(self, request, pk, question, *args, **kwargs):
        """Return list of options for"""
        obj = self.get_object()
        question = get_object_or_404(Question, pk=question)
        options = OptionInspection.objects.filter(option=obj.appointment.option, years__title=obj.appointment.year)
        answers = []
        for option in options:
            answers.append({
                "id": option.title,
                "questions": [],
                "title": option.title,
                "default": False,
            })
        other = {
            "id": 1466,
            "title": "سایر",
            "order": None,
            "default": False,
            "questions": [
                {
                    "id": 2461,
                    "title": "آپشن را وارد کنید",
                    "field_type": "1",
                    "questions": None,
                    "value": None
                }
            ],
        }

        try:
            v = InspectionData.objects.get(inspection=obj, question=question)
        except InspectionData.DoesNotExist:
            v = None
        if v:
            set_default = False
            for i, k in enumerate(answers):
                if k['title'] == v.answer:
                    answers[i]['default'] = True
                    set_default = True
                    break
            if not set_default:
                other['default'] = True
                other['questions'][0]['value'] = v.answer
        elif len(answers) > 0:
            answers[0]['default'] = True
        else:
            other['default'] = True

        answers.append(other)

        d = {
            "id": question.id,
            "title": question.title,
            "field_type": "4",
            "questions": None,
            "answers": answers
        }
        return Response(d)

    def segments(self, request, slug, segment, *args, **kwargs):
        """You must fill segments by segment id of this api '/api/v1/inspection/{pk}/'"""
        pk = slug
        obj = get_object_or_404(Inspection, pk=slug)
        segment = get_object_or_404(Segment, pk=segment)

        r = Redis.get_instance().conn
        key = '{}_i{}_s{}'.format(self.prefix, slug, segment.id)
        time = Question.objects.filter(segment=segment).order_by('-modified_time').only('modified_time')[
            0].modified_time
        time = int(time.timestamp())

        ins_time = InspectionData.objects.filter(inspection=obj).order_by(
            '-modified_time').only('modified_time').first()
        if ins_time and int(ins_time.modified_time.timestamp()) > time:
            time = int(ins_time.modified_time.timestamp())
        r_time = r.get('time_' + key)
        if r_time and int(r_time) >= time:
            data = r.get(key)
            if data:
                data = data.decode('utf-8')
                return Response(json.loads(data))

        self.serializer_class = SegmentSerializer
        sg_data = self.serializer_class(segment, inspection=obj)
        data = sg_data.data
        r.set('time_' + key, time, settings.KEY_TIMEOUT)
        r.set(key, json.dumps(data), settings.KEY_TIMEOUT)
        return Response(data)

    def save_segment(self, request, slug, segment):
        """You must fill segments by segment id of this api '/api/v1/inspection/{pk}/
        ---
          parameters:
           - name: body
             paramType: body
             defaultValue: '{"answers": [{"qId":, "aId":""}], "inspectionsId": , "segmentId": }'
             description: in response of /api/b/inspection/{pk}/segments/{segment}/ in questions list select id and put it into qId and put replace value in aId
        """
        pk = slug
        obj = get_object_or_404(Inspection, pk=slug)
        segment = get_object_or_404(Segment, pk=segment)
        if obj.status == Inspection.STATUS_AUDIT and not request.user.is_superuser:
            return Response({'success': False, 'error': _('Inspection Closed, cannot be changed')}, status=403)
        self.serializer_class = BasePostSerializer
        d = self.serializer_class(request.data['answers'], many=True, segment=segment, inspection=obj)
        for item in d.data:
            p, c = InspectionData.objects.get_or_create(question_id=item['question'], inspection=obj)
            p.answer = item['answer']
            p.save()

        return Response('Object: {}, Segment {}'.format(obj.created_time, segment))


class InspectionDealerViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    model = Inspection
    serializer_class = InspectionDealerSerializer
    # TODO: this line is temp for few days, remove exclude soon
    queryset = Inspection.objects.all()
    prefix = 'insview_'
    lookup_field = "slug"

    # def get_object(self):
    #     """
    #         Returns the object the view is displaying.
    #
    #         You may want to override this if you need to provide non-standard
    #         queryset lookups.  Eg if objects are referenced using multiple
    #         keyword arguments in the url conf.
    #         """
    #     queryset = self.filter_queryset(self.get_queryset())
    #
    #     # Perform the lookup filtering.
    #     lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
    #
    #     assert lookup_url_kwarg in self.kwargs, (
    #         'Expected view %s to be called with a URL keyword argument '
    #         'named "%s". Fix your URL conf, or set the `.lookup_field` '
    #         'attribute on the view correctly.' %
    #         (self.__class__.__name__, lookup_url_kwarg)
    #     )
    #
    #     filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
    #
    #     try:
    #         return queryset.get(**filter_kwargs)
    #     except Inspection.DoesNotExist:
    #         try:
    #             return queryset.get(id=self.kwargs[lookup_url_kwarg])
    #         except Inspection.DoesNotExist:
    #             pass
    #     raise Http404

    def list(self, request, *args, **kwargs):
        """list the inspections for dealer app"""
        self.queryset = self.queryset.filter(status=Inspection.STATUS_AUDIT)
        if request.GET.get('q', None):
            self.queryset = self.queryset.filter(
                Q(appointment__name__icontains=request.GET['q']) | Q(id__icontains=request.GET['q']))

        if request.GET.get('last_id', None):
            self.queryset = self.queryset.filter(id__lt=request.GET['last_id'])

        size = int(request.GET.get('size', 10))
        if size > 20:
            size = 10
        self.queryset = self.queryset[:size]

        self.serializer_class = InspectionDealerListSerializer
        return super(InspectionDealerViewSet, self).list(request, *args, **kwargs)

    def retrieve(self, request, slug, *args, **kwargs):
        """Retrieve an inspection for dealer app by pk"""
        instance = self.get_object()
        if not instance.status == Inspection.STATUS_AUDIT:
            return Response({'success': False, 'error': _('Inspection in progress, cannot be viewed')}, status=403)

        with transaction.atomic():
            instance.views += 1
            instance.save()
            serializer = self.get_serializer(instance)
            return Response(serializer.data)

    @detail_route(methods=['GET'])
    def views(self, request, slug, *args, **kwargs):
        """Count Inspection views"""
        obj = self.get_object()
        obj.views += 1
        obj.save()
        submit_view.delay(obj, request.user)
        res = Response(obj.views + VIEWS_PLUS)
        res['Cache-Control'] = 'no-cache'
        return res

    def segments(self, request, slug, segment, *args, **kwargs):
        """You must fill segments by segment id of this api '/api/v1/inspection_dealer/{pk}/'"""
        obj = self.get_object()
        segment = get_object_or_404(Segment, pk=segment)

        r = Redis.get_instance().conn
        key = '{}_i{}_s{}'.format(self.prefix, slug, segment.id)
        time = Question.objects.filter(segment=segment).order_by('-modified_time').only('modified_time')[
            0].modified_time
        time = int(time.timestamp())

        ins_time = InspectionData.objects.filter(inspection=obj).order_by(
            '-modified_time').only('modified_time').first()
        if ins_time and int(ins_time.modified_time.timestamp()) > time:
            time = int(ins_time.modified_time.timestamp())
        r_time = r.get('time_' + key)
        if r_time and int(r_time) >= time:
            data = r.get(key)
            if data:
                data = data.decode('utf-8')
                return Response(json.loads(data))

        self.serializer_class = SegmentDealerSerializer
        sg_data = self.serializer_class(segment, inspection=obj)
        data = sg_data.data
        r.set('time_' + key, time, settings.KEY_TIMEOUT)
        r.set(key, json.dumps(data), settings.KEY_TIMEOUT)
        return Response(data)

    @detail_route(methods=['GET'])
    def image(self, request, slug, *args, **kwargs):
        """show images of the dealer by pk."""
        obj = self.get_object()
        d = InspectionDealerImageSerializer(obj)
        return Response(d.data)

    @detail_route(methods=['GET'])
    def positive_comments(self, request, slug, *args, **kwargs):
        obj = self.get_object()
        d = InspectionDealerPositiveCommentsSerializer(obj)
        return Response(d.data)


router = routers.DefaultRouter()
router.routes.append(
    routers.Route(
        url=r'^{prefix}/(?P<slug>[^/]+)/segments/(?P<segment>[^/]+){trailing_slash}',
        name='{basename}-segments',
        mapping={
            'get': 'segments',
            'put': 'save_segment',
            'post': 'save_segment',
        },
        initkwargs={}
    ),
)
router.routes.append(
    routers.Route(
        url=r'^{prefix}/(?P<pk>[^/]+)/options/(?P<question>[^/]+){trailing_slash}',
        name='{basename}-options',
        mapping={
            'get': 'options',
        },
        initkwargs={}
    ),
)
router.register(r'inspection', InspectionViewSet, 'Inspections')
router.register(r'inspection_dealer', InspectionDealerViewSet, 'InspectionsDealer')
