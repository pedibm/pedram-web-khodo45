# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def save_id_to_slug(apps, schema_editor):
    Inspection = apps.get_model('inspections', 'Inspection')
    last = Inspection.objects.filter(slug="")
    for item in last:
        item.slug = item.id
        item.save()


class Migration(migrations.Migration):
    dependencies = [
        ('inspections', '0009_inspection_slug'),
    ]

    operations = [
        migrations.RunPython(save_id_to_slug),
    ]
