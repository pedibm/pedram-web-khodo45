from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.fields.models import Question, Answer
from apps.images.models import Image


class Inspection(models.Model):
    CITY_ID = 1968
    BRANCH_ID = 1969
    BRAND_ID = 1970
    OPTION_ID = 1971
    MODEL_ID = 1972
    KLM_ID = 1974
    YEAR_ID = 2137
    CUSTOMER_NAME_ID = 1982
    OWNER_NAME_ID = 1983
    RECEPTION_NUMBER_ID = 1967

    STATUS_NEW = '0'
    STATUS_AUDIT = '1'
    STATUS_CLOSED = '2'
    STATUS_SOLD = '3'

    INSPECTION_STATUS = (
        (STATUS_NEW, _('New')),
        (STATUS_AUDIT, _('Audit')),
        (STATUS_CLOSED, _('Closed')),
        (STATUS_SOLD, _('Sold')),
    )

    status = models.CharField(max_length=1,
                              choices=INSPECTION_STATUS,
                              default=STATUS_NEW)

    # Car Details
    appointment = models.ForeignKey('appointments.Appointment')
    views = models.IntegerField(default=1)
    slug = models.CharField(max_length=10, blank=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    @staticmethod
    def create_from_appointment(appointment):
        from apps.appointments.models import Appointment

        try:
            inspect = Inspection.objects.get(appointment=appointment).last()
            inspect.slug = inspect.id
            inspect.save()
        except Inspection.DoesNotExist:
            pass
        inspect = Inspection()
        # inspect.name = obj.name
        # inspect.email = obj.email
        # inspect.phone = obj.phone
        #
        # inspect.option = obj.model.option_set.first()
        # inspect.klm = obj.klm
        # inspect.year = obj.year
        inspect.appointment = appointment
        inspect.appointment.state = Appointment.STATE_TRANSFER_INSPECTOR
        from apps.appointments.tasks import generate_slug
        inspect.slug = generate_slug()
        inspect.save()

        inspection_preload = [Inspection.CITY_ID, Inspection.BRANCH_ID, Inspection.BRAND_ID, Inspection.OPTION_ID,
                              Inspection.MODEL_ID, Inspection.KLM_ID, Inspection.CUSTOMER_NAME_ID,
                              Inspection.OWNER_NAME_ID, Inspection.YEAR_ID, Inspection.RECEPTION_NUMBER_ID]
        # inspection_preload_field = ['city', 'branch', 'brand', 'option', 'model', 'klm', 'name', 'name']
        for i in inspection_preload:
            insdata = InspectionData(inspection=inspect, question_id=i)
            if i is Inspection.CITY_ID:
                a = appointment.branch.city.title
            elif i is Inspection.BRANCH_ID:
                a = appointment.branch.title
            elif i is Inspection.BRAND_ID:
                a = appointment.option.model.brand.title
            elif i is Inspection.OPTION_ID:
                a = appointment.option.title
            elif i is Inspection.MODEL_ID:
                a = appointment.option.model.title
            elif i is Inspection.KLM_ID:
                a = appointment.klm
            elif i is Inspection.YEAR_ID:
                a = appointment.year
            elif i is Inspection.RECEPTION_NUMBER_ID:
                a = appointment.id
            elif i is Inspection.CUSTOMER_NAME_ID or i is Inspection.OWNER_NAME_ID:
                a = appointment.name
            else:
                a = 'NO DATA ' + str(i)  # TODO: CHECK FOR ERROR

            insdata.answer = a
            insdata.save()

        return inspect

    def __str__(self):
        return "{}".format(self.appointment)


class InspectionData(models.Model):
    """"
    Store Inspection data value by relation to inspection and fields
    """
    question = models.ForeignKey('fields.Question')
    # answer = models.ForeignKey('fields.Answer', null=True, blank=True)
    answer = models.CharField(max_length=500)
    inspection = models.ForeignKey(Inspection)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = [('question', 'inspection')]

    def get_answer(self):
        obj = self.answer
        if self.question.field_type in [Question.TYPE_IMAGE_ADDITIONAL, Question.TYPE_IMAGE_MAIN]:
            obj = Image.objects.get(id=self.answer)
        elif self.question.field_type in [Question.TYPE_OPTIONS, Question.TYPE_OPTION, Question.TYPE_CHECK]:
            obj = Answer.objects.get(id=self.answer)
        # elif self.question.field_type in []
        return obj

    def __str__(self):
        return "{}={}".format(self.question, self.answer)


class InspectionView(models.Model):
    """
    Store User view log, log stored by Inspection for now
    """
    inspection = models.ForeignKey(Inspection)
    user = models.ForeignKey('users.User')

    created_time = models.DateTimeField(auto_now_add=True)
