import json
from random import randint
from datetime import timedelta

from rest_framework import serializers
from django.utils import timezone

from apps.fields.models import Question
from apps.fields.serializers import QuestionSerializer
from apps.images.models import Image
from apps.images.serializers import ImageDealerSerializer, RFDealerSerializer
from apps.locations.serializers import BranchMiniSerializer

from .models import Inspection, InspectionData
from apps.cars.serializers import OptionSerializer
from apps.segments.models import Segment
from apps.auctions.models import Auction
from apps.segments.serializers import SegmentListSerializer, SegmentListDealerSerializer, SegmentSummarySerializer

VIEWS_PLUS = 90
PRICE_LIST = [9000000, 9100000, 9200000, 9300000, 9600000, 9800000, 9900000]


class InspectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inspection
        exclude = ('modified_time',)

    segments = serializers.SerializerMethodField()

    def get_segments(self, obj):
        sg = Segment.objects.all()
        sg_data = SegmentListSerializer(sg, many=True, inspection=obj)
        return sg_data.data


class InspectionMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inspection
        exclude = ('modified_time', 'appointment')


class InspectionSummarySerializer(InspectionSerializer):
    def __init__(self, *args, **kwargs):
        exclude = (
            'modified_time', 'id', 'name', 'family', 'national_code', 'email', 'phone', 'appointment', 'created_time')
        super(InspectionSummarySerializer, self).__init__(*args, **kwargs)
        for e in exclude:
            self.fields.pop(e, None)

    def get_segments(self, obj):
        sg = Segment.objects.all()
        sg_data = SegmentSummarySerializer(sg, many=True, inspection=obj)
        return sg_data.data


class InspectionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inspection
        fields = ('id', 'status', 'option', 'reserve_time', 'year', 'name', 'plate', 'slug', 'spend_time')

    # option = OptionSerializer()

    option = serializers.SerializerMethodField()
    reserve_time = serializers.SerializerMethodField()
    plate = serializers.SerializerMethodField()
    year = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    spend_time = serializers.SerializerMethodField()
    slug = serializers.SerializerMethodField()

    def get_slug(self, obj):
        return obj.appointment.id

    def get_option(self, obj):
        return OptionSerializer(obj.appointment.option).data

    def get_reserve_time(self, obj):
        return obj.appointment.reserve

    def get_plate(self, obj):
        qs = Question.objects.get(field_type=Question.TYPE_PLATE)
        try:
            d = InspectionData.objects.get(question=qs, inspection=obj)
        except InspectionData.DoesNotExist:
            return None
        return d.answer

    def get_year(self, obj):
        try:
            return InspectionData.objects.get(inspection=obj, question_id=Inspection.YEAR_ID).answer
        except InspectionData.DoesNotExist:
            return obj.appointment.year

    def get_klm(self, obj):
        try:
            return InspectionData.objects.get(inspection=obj, question_id=Inspection.KLM_ID).answer
        except InspectionData.DoesNotExist:
            return obj.appointment.klm

    def get_name(self, obj):
        try:
            return InspectionData.objects.get(inspection=obj, question_id=Inspection.OWNER_NAME_ID).answer
        except InspectionData.DoesNotExist:
            return obj.appointment.name

    def get_spend_time(self, obj):
        if obj.appointment.status not in [obj.appointment.STATUS_FINISH_INSPECTOR,
                                          obj.appointment.STATUS_CONFIRM, obj.appointment.STATUS_AUCTION]:
            return 0
        from apps.appointments.models import AppointmentLog
        # find last inspection log, must handle reinspection ;)
        s = AppointmentLog.objects.filter(appointment_id=obj.appointment.id,
                                         status=obj.appointment.STATUS_CONFIRM).last()
        if not s:
            return 0
        try:
            e = AppointmentLog.objects.get(id__gt=s.id,
                                           appointment_id=obj.appointment.id,
                                           status=obj.appointment.STATUS_FINISH_INSPECTOR)
        except AppointmentLog.DoesNotExist:
            return 0
        d = (e.created_time - s.created_time).seconds
        return int(d)


class BasePostSerializer(serializers.ModelSerializer):
    class Meta:
        model = InspectionData
        exclude = ('modified_time', 'inspection')

    def __init__(self, *args, **kwargs):
        self._inspection = kwargs.pop('inspection', None)
        self._segment = kwargs.pop('segment', None)
        super(BasePostSerializer, self).__init__(self, *args, **kwargs)

    question = serializers.SerializerMethodField()
    answer = serializers.SerializerMethodField()

    def get_question(self, obj):
        return obj['qId']

    def get_answer(self, obj):
        return obj['aId']


class InspectionDealerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inspection
        exclude = ('modified_time',)

    id = serializers.SerializerMethodField()
    segments = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()
    rf = serializers.SerializerMethodField()
    positive_comments = serializers.SerializerMethodField()
    option = serializers.SerializerMethodField()
    year = serializers.SerializerMethodField()
    klm = serializers.SerializerMethodField()
    views = serializers.SerializerMethodField()
    bid = serializers.SerializerMethodField()
    branch = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    original_images = serializers.SerializerMethodField()
    document = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.slug

    def get_segments(self, obj):
        sg = Segment.objects.all()
        sg_data = SegmentListDealerSerializer(sg, many=True, inspection=obj)
        return sg_data.data

    def get_rf(self, obj):

        s_id = []
        rfs = []
        for rf in self.rf:
            try:
                s_id.index(rf['question'].segment_id)
            except ValueError:
                s_id.append(rf['question'].segment_id)
                s = Segment.objects.get(id=rf['question'].segment_id)
                rfs.append({'segment': s, 'images': []})
            rfs[s_id.index(rf['question'].segment_id)]['images'].append(rf)

        data = RFDealerSerializer(rfs, many=True).data
        return data

    def get_positive_comments(self, obj):
        qs = Question.objects.filter(question__field_type=Question.TYPE_POSITIVE).exclude(private=True)
        # inspection_data = InspectionData.objects.filter(question__in=qs, inspection=obj)
        qs = QuestionSerializer(qs, many=True, inspection=obj)
        return qs.data

    @staticmethod
    def staticmethod_images(obj):
        qs = Question.objects.filter(field_type=Question.TYPE_IMAGE_MAIN).exclude(private=True)
        inspection_data = InspectionData.objects.filter(question__in=qs, inspection=obj).order_by('question__order')
        images_id = []
        images_title = []
        cover_id = None
        for inspection in inspection_data:
            q = inspection.question
            images_id.append(int(inspection.answer))
            images_title.append(q.title)
            if not cover_id and q.id == 1550:
                cover_id = int(inspection.answer)
        main_images = Image.objects.filter(id__in=images_id)
        main_images_list = list(main_images)
        main_images_list.sort(key=lambda t: images_id.index(t.pk))
        cover = None
        images = []
        for i in range(0, len(main_images_list)):
            image = {'file': main_images_list[i].file, 'title': images_title[images_id.index(main_images_list[i].id)],
                     'id': main_images_list[i].id}
            images.append(image)
            if main_images_list[i].id == cover_id:  # Main Image Jolo
                cover = {'file': main_images_list[i].file, 'title': '', 'id': main_images_list[i].id}
        if cover is None and len(images) > 0:
            cover = {'file': images[0]['file'], 'title': '', 'id': images[0]['id']}

        main_images = ImageDealerSerializer(images, many=True).data
        cover = ImageDealerSerializer(cover).data  # TODO: get better size

        qs = Question.objects.filter(field_type=Question.TYPE_IMAGE_ADDITIONAL).exclude(private=True)
        inspection_data = InspectionData.objects.filter(question__in=qs, inspection=obj)
        images_id = []
        images_title = []

        images_question = []
        images_question_child = []

        for inspection in inspection_data:
            q = inspection.question
            images_id.append(int(inspection.answer))
            images_question_child.append(q)
            titles = [q.title, ]
            tq = q.question
            while tq.question:
                titles.append(tq.title)
                tq = tq.question
                if tq.question is None:
                    titles.insert(1, ":")
            titles.reverse()
            images_title.append(' '.join(titles))
            images_question.append(tq)
        # self.rf = images_title

        additional_images = Image.objects.filter(id__in=images_id)
        images = []
        for i in range(0, additional_images.count()):
            image = {'file': additional_images[i].file, 'title': images_title[images_id.index(additional_images[i].id)],
                     'id': additional_images[i].id,
                     'question_child': images_question_child[images_id.index(additional_images[i].id)],
                     'question': images_question[images_id.index(additional_images[i].id)]}
            images.append(image)
        additional_images = ImageDealerSerializer(images, many=True).data
        return [images, main_images, additional_images, cover]

    def get_images(self, obj):
        st = self.staticmethod_images(obj)
        self.rf = st[0]

        images = {
            'main': st[1],
            'additional': st[2],
            'cover': st[3]
        }
        return images

    def get_option(self, obj):
        return OptionSerializer(obj.appointment.option).data

    def get_year(self, obj):
        try:
            return InspectionData.objects.get(inspection=obj, question_id=Inspection.YEAR_ID).answer
        except InspectionData.DoesNotExist:
            return obj.appointment.year

    def get_klm(self, obj):
        try:
            return InspectionData.objects.get(inspection=obj, question_id=Inspection.KLM_ID).answer
        except InspectionData.DoesNotExist:
            return obj.appointment.klm

    def get_views(self, obj):
        # url = reverse('InspectionsDealer-views', kwargs={'slug': obj.pk})
        return "<esi:include src=views/ />"

    def get_bid(self, obj):
        return BidSerializer(obj).data

    def get_branch(self, obj):
        return BranchMiniSerializer(obj.appointment.branch).data

    def get_price(self, obj):
        auction_obj = Auction.objects.filter(inspection=obj).last()
        if auction_obj:
            return auction_obj.price
        else:
            return None

    def get_original_images(self, obj):
        from apps.auctions.serializers import AuctionListSerializer
        return AuctionListSerializer.get_main_images(obj)

    def get_document(self, obj):
        from apps.auctions.serializers import AuctionListSerializer
        return AuctionListSerializer.get_document(obj)


class InspectionDealerPositiveCommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inspection
        fields = ('positive_comments',)

    positive_comments = serializers.SerializerMethodField()

    def get_positive_comments(self, obj):
        qs = Question.objects.filter(question__field_type=Question.TYPE_POSITIVE).exclude(private=True)
        # inspection_data = InspectionData.objects.filter(question__in=qs, inspection=obj)
        qs = QuestionSerializer(qs, many=True, inspection=obj)
        return qs.data


class InspectionDealerImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inspection
        exclude = ('modified_time',)

    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        dealer_serializer = InspectionDealerSerializer.staticmethod_images(obj)
        images = {
            'main': dealer_serializer[1],
            'additional': dealer_serializer[2],
            'cover': dealer_serializer[3]
        }
        return images


class BidSerializer(serializers.Serializer):
    highest = serializers.SerializerMethodField()
    end = serializers.SerializerMethodField()
    step = serializers.SerializerMethodField()

    def get_highest(self, obj):
        # TODO: use real Data
        return PRICE_LIST[randint(0, len(PRICE_LIST) - 1)]

    def get_end(self, obj):
        # TODO: use real Data
        d = timezone.now()
        d = d + timedelta(minutes=randint(1, 30))
        return d.timestamp()

    def get_step(self, obj):
        # TODO: use real Data
        return round(PRICE_LIST[randint(0, len(PRICE_LIST) - 1)] / 100)


class InspectionDealerListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inspection
        fields = ('id', 'option', 'year', 'name', 'cover_image', 'bid', 'klm', 'views', 'price', 'branch')

    id = serializers.SerializerMethodField()
    option = serializers.SerializerMethodField()
    year = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    cover_image = serializers.SerializerMethodField()
    klm = serializers.SerializerMethodField()
    views = serializers.SerializerMethodField()
    branch = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    bid = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.slug

    def get_option(self, obj):
        return OptionSerializer(obj.appointment.option).data

    def get_year(self, obj):
        return obj.appointment.year

    def get_klm(self, obj):
        try:
            return InspectionData.objects.get(inspection=obj, question_id=Inspection.KLM_ID).answer
        except InspectionData.DoesNotExist:
            return obj.appointment.klm

    def get_name(self, obj):
        return obj.appointment.name

    def get_cover_image(self, obj):
        image = InspectionDealerImageSerializer(obj).data
        return image['image']['cover']

    def get_views(self, obj):
        return obj.views + VIEWS_PLUS

    def get_price(self, obj):
        # TODO: use real Data
        return PRICE_LIST[randint(0, len(PRICE_LIST) - 1)] + 200000

    def get_branch(self, obj):
        return BranchMiniSerializer(obj.appointment.branch).data

    def get_bid(self, obj):
        return BidSerializer(obj).data
