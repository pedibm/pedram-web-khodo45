# -*- coding: utf-8 -*-

from celery import task
from .models import InspectionView


@task
def submit_view(inspection, user):
    if user.is_anonymous():
        return
    user = user
    v = InspectionView()
    v.user = user
    v.inspection = inspection
    v.save()
