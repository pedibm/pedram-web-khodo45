import datetime
from django import template
from django.template.loader import get_template

from apps.fields.models import Question

register = template.Library()


@register.inclusion_tag('inspection/question_list.html', takes_context=True)
def segment_question(context):
    segment = context['segment']
    inspection = context['inspection']
    # for question in segment.question_data(inspection)['question']:
    #     print('HERE')
    return {'questions': segment.question_data(inspection)['questions']}


@register.inclusion_tag('inspection/question.html')
def question_render(question):
    if question['field_type'] in [Question.TYPE_IMAGE_MAIN, Question.TYPE_IMAGE_ADDITIONAL, Question.TYPE_POPUP]:
        return
    answer = None
    for a in question['answers']:
        if a['default']:
            answer = a
            break
    return {'question': question, 'answer': answer}
