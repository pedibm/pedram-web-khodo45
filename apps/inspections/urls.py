from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^inspection/(?P<pk>\d{0,50})/$', views.inspection, name='inspection'),
]
