# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404

from apps.segments.models import Segment
from .models import Inspection, InspectionData
from apps.fields.models import Question


def inspection(request, pk):
    print(pk)

    inspection = get_object_or_404(Inspection, pk=pk)
    inspection_data = InspectionData.objects.filter(inspection=inspection)
    main_images = inspection_data.filter(question__field_type=Question.TYPE_IMAGE_MAIN)
    additional_images = inspection_data.filter(question__field_type=Question.TYPE_IMAGE_ADDITIONAL)
    segments = Segment.objects.all()

    variables = {'inspection': inspection, 'inspection_data': inspection_data,
                 'main_images': main_images, 'additional_images': additional_images,
                 'segments': segments}
    return render(request, 'inspection/main.html', variables)
