from django.contrib import admin

from .models import City, Branch


class BranchAdmin(admin.ModelAdmin):
    list_display = ('title', 'city', 'active')
    search_fields = ('title',)
    list_filter = ('active',)


admin.site.register(City)
admin.site.register(Branch, BranchAdmin)
