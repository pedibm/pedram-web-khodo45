from rest_framework import viewsets, routers, mixins

from .models import City, Branch
from .serializers import CitySerializer, BranchSerializer


class CityViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """list of all existing citys"""
    model = City
    serializer_class = CitySerializer
    queryset = City.objects.all()


class BranchViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """list of the branchs"""
    model = Branch
    serializer_class = BranchSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned models to a given brands,
        by filtering against a `brand_id` query parameter in the URL.
        """
        queryset = Branch.objects.all()
        city_id = self.request.query_params.get('city_id', None)
        if city_id is not None:
            queryset = queryset.filter(city_id=city_id)
        return queryset


router = routers.DefaultRouter()
router.register(r'city', CityViewSet, 'City')
router.register(r'branch', BranchViewSet, 'Branch')
