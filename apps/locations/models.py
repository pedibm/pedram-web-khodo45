from django.db import models


class City(models.Model):
    """
    Store city we work in
    """
    title = models.CharField(max_length=200)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Branch(models.Model):
    """
    Store Inspection Branch Data
    """
    title = models.CharField(max_length=200)
    map = models.CharField(max_length=300, null=True, blank=True)
    map_link = models.CharField(max_length=300, null=True, blank=True)
    address = models.CharField(max_length=300, null=True, blank=True)
    phone = models.CharField(max_length=11, null=True, blank=True)
    map_x = models.CharField(max_length=11, null=True, blank=True)
    map_y = models.CharField(max_length=11, null=True, blank=True)

    city = models.ForeignKey('City')
    active = models.BooleanField(default=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-active']
