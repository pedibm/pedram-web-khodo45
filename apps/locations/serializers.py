# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import City, Branch


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        exclude = ['created_time', ]


class BranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Branch
        exclude = ['created_time', ]


class BranchMiniSerializer(serializers.ModelSerializer):
    city = CitySerializer()

    class Meta:
        model = Branch
        fields = ['title', 'city', 'address', 'map', 'phone', 'map_x', 'map_y']
