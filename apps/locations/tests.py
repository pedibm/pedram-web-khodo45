"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from .models import City, Branch


class SimpleTest(TestCase):
    def test_city(self):
        # TODO: write some real test, test serialzier and object creation
        city = City()
        self.assertIsInstance(city, City)

    def test_branch(self):
        # TODO: write some real test, test serialzier and object creation
        branch = Branch()
        self.assertIsInstance(branch, Branch)
