from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
      url(r'^$', views.index, name='index'),
      url(r'^step2/$', views.step2, name='step2'),
      url(r'^download/$', views.download, name='download'),
      url(r'^step3/$', views.step3, name='step3'),
      url(r'^sitemap.xml', views.sitemap, name='sitemap'),
      url(r'^sitemaps/cars.xml', views.sitemap_cars, name='sitemap_cars'),
      url(r'^dealer/', views.dealer),
      url(r'^document/', views.document),

  ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
