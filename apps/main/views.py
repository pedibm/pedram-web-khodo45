# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.template import RequestContext

from apps.cars.models import CarContent
from apps.testimonials.models import Testimonials
from apps.faq.models import Faq
from carpars import settings
from rest_framework.decorators import api_view, renderer_classes
from rest_framework import response, schemas
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer


def dealer(request):
    return render(request, 'dealer/main.html')


def document(request):
    return render(request, 'document/main.html')


@api_view()
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer])
def schema_view(request):
    generator = schemas.SchemaGenerator(title='Khodro45 APIs')
    return response.Response(generator.get_schema(request=request))


def index(request):
    customers = Testimonials.objects.all()
    faq = Faq.objects.all()
    splited_response = []
    for i in range(0, len(faq)):
        answer_response_obj = {
            "req": faq[i].answer,
            "res": str(faq[i].response).split("\r")
        }
        splited_response.append(answer_response_obj)
    # print splited_response

    variables = {'testimonials': customers, 'faq': splited_response}
    return render(request, 'main/index.html', variables)


def step2(request):
    variables = {}
    return render(request, 'main/step2.html', variables)

def download(request):
    variables = {}
    return render(request, 'main/download.html', variables)

def step3(request):
    variables = {}
    return render(request, 'main/step3.html', variables)


def sitemap(req):
    modified_time = CarContent.objects.values('modified_time').order_by('-modified_time')[0]
    variables = {'modified_time': modified_time['modified_time'], 'BASE_URL': settings.BASE_URL}
    return render(req, 'sitemap/index.xml', variables, content_type='application/xml')


def sitemap_cars(req):
    content = CarContent.objects.all()
    variables = {'cars': content, 'BASE_URL': settings.BASE_URL}
    return render(req, 'sitemap/cars.xml', variables, content_type='application/xml')
