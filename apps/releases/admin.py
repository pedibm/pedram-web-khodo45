from django.contrib import admin
from .models import Release


class ReleaseAdmin(admin.ModelAdmin):
    list_display = ('device', 'app', 'build_number', 'version', 'active')
    search_fields = ('build_number', 'version')
    list_filter = ('device', 'app', 'active')


admin.site.register(Release, ReleaseAdmin)
