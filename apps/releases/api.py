from rest_framework import viewsets, routers
from rest_framework.decorators import list_route
from rest_framework.response import Response

from .models import Release
from .serializers import ReleaseSerializer


class ReleaseViewSet(viewsets.GenericViewSet):
    model = ReleaseSerializer
    serializer_class = ReleaseSerializer
    queryset = Release.objects.all()

    @list_route(methods=['GET'])
    def versions(self, request):
        queryset = Release.objects.filter(active=True).distinct('app', 'device').order_by('app', 'device', '-created_time')
        data = ReleaseSerializer(queryset, many=True)
        queryset_force_update = Release.objects.filter(active=False).distinct('app', 'device').order_by('app', 'device', '-created_time')
        data_force_update = ReleaseSerializer(queryset_force_update, many=True)
        return Response({'latest': data.data, 'force_update': data_force_update.data})


router = routers.DefaultRouter()
router.register(r'releases', ReleaseViewSet, 'Releases')
