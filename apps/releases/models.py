from django.db import models
from django.utils.translation import ugettext_lazy as _


class Release(models.Model):
    """"
    This model handle all release plan, applications based on data from this model show update and force update modal
    """
    DEVICE_ANDROID = '0'
    DEVICE_IOS = '1'

    DEVICE_TYPE = (
        (DEVICE_ANDROID, _('ANDROID')),
        (DEVICE_IOS, _('IOS')),
    )
    device = models.CharField(max_length=2, choices=DEVICE_TYPE)

    APP_INSPECTION = '0'
    APP_DEALER = '1'

    APP_TYPE = (
        (APP_INSPECTION, _('INSPECTION')),
        (APP_DEALER, _('DEALER')),
    )
    app = models.CharField(max_length=1, choices=APP_TYPE)

    build_number = models.IntegerField()
    version = models.CharField(max_length=50)

    active = models.BooleanField(default=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)
