# -*- coding: utf-8 -*-
import datetime
from rest_framework import viewsets, routers, mixins
from rest_framework.decorators import list_route
from apps.appointments.models import Appointment
from django.db.models import Count
from django.db.models.functions import TruncDay
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from apps.auctions.models import Auction
from apps.cars.models import Brand
from apps.inspections.models import Inspection
from apps.reports.serializers import CheckCreatedTimeSerializer


class ReportBackOfficeViewSet(mixins.ListModelMixin,
                              viewsets.GenericViewSet):
    queryset = None

    def get_queryset(self):
        """
        Optionally restricts the returned query based on request parameter
        """
        if self.queryset:
            return self.queryset

        q = Appointment.objects.all()
        request = self.request

        created_from = request.GET.get('from', None)
        created_to = request.GET.get('to', None)
        if created_from and created_to:
            data = {'from_date': created_from, 'to_date': created_to}
            ser = CheckCreatedTimeSerializer(data=data)
            ser.is_valid(raise_exception=True)
            q = q.filter(created_time__gte=ser.validated_data['from_date'],
                         created_time__lte=ser.validated_data['to_date'])
        else:
            from_10_days = datetime.datetime.now() - datetime.timedelta(days=10)
            q = q.filter(created_time__gte=from_10_days)
        return q

    def list(self, request, *args, **kwargs):
        today = datetime.datetime.today().strftime('%Y-%m-%d')
        q = self.get_queryset()
        total_count = q.count()
        total_today = q.filter(created_time__date=today).count()

        check_rc_q = q.filter(status=Appointment.STATUS_CHECK, state__in=Appointment.REAL_CASE)
        check_real_case = check_rc_q.count()
        check_real_case_today = check_rc_q.filter(created_time__date=today).count()

        check_nrc_q = q.filter(status=Appointment.STATUS_CHECK, state__in=Appointment.NOT_REAL_CASE)
        check_not_real_case = check_nrc_q.count()
        check_not_real_case_today = check_nrc_q.filter(created_time__date=today).count()

        reserve_rc_q = q.filter(status=Appointment.STATUS_RESERVE).exclude(state__in=Appointment.NOT_REAL_CASE_RESERVE)
        reserve_real_case = reserve_rc_q.count()
        reserve_real_case_today = reserve_rc_q.filter(created_time__date=today).count()

        reserve_nrc_q = q.filter(status=Appointment.STATUS_RESERVE, state__in=Appointment.NOT_REAL_CASE_RESERVE)
        reserve_not_real_case = reserve_nrc_q.count()
        reserve_not_real_case_today = reserve_nrc_q.filter(created_time__date=today).count()

        reserve_total = q.filter(status=Appointment.STATUS_CONFIRM)
        reserve_total_count = reserve_total.count()
        reserve_total_today = reserve_total.filter(created_time__date=today).count()

        inspection_total_obj = Inspection.objects.all()
        inspection_total_count = inspection_total_obj.count()
        inspection_total_today = inspection_total_obj.filter(created_time__date=today).count()

        auction_total_obj = Auction.objects.all()
        auction_total_count = auction_total_obj.count()
        auction_total_today = auction_total_obj.filter(created_time__date=today).count()

        variables = {
            "request": {"total": total_count, "today": total_today},
            "summary": {
                "request": {'real_case': {"total": check_real_case,
                                          "today": check_real_case_today},
                            "not_real_case": {"total": check_not_real_case,
                                              "today": check_not_real_case_today}},
                "appointment": {'real_case': {"total": reserve_real_case,
                                              "today": reserve_real_case_today},
                                "not_real_case": {"total": reserve_not_real_case,
                                                  "today": reserve_not_real_case_today}}
            },
            "appointment": {"total": reserve_total_count, "today": reserve_total_today},
            "inspection": {"total": inspection_total_count, "today": inspection_total_today},
            "auction": {"total": auction_total_count, "today": auction_total_today},
        }
        return Response(variables)

    @list_route(methods=['GET'])
    def brands(self, request, *args, **kwargs):
        q_appointment = self.get_queryset()
        key = "option__model_id__brand_id"

        options_distinct = q_appointment.distinct(key).count()
        options_distinct_count = q_appointment.values(key).annotate(count=Count('id'))
        brands = []
        for b in options_distinct_count:
            brand = Brand.objects.get(id=b[key])
            brands.append({'brand': brand.title, 'count': b['count']})
        variables = {"brands_count": options_distinct, "brands_distinct_count": brands}
        return Response(variables)

    @list_route(methods=['GET'])
    def request(self, request, *args, **kwargs):
        q_appointment = self.get_queryset()

        trunc_day = q_appointment.annotate(day=TruncDay('created_time')).values('day')

        total = trunc_day.filter(status=Appointment.STATUS_CHECK).annotate(count=Count('id'))
        real_case = trunc_day.filter(status=Appointment.STATUS_CHECK, state__in=Appointment.REAL_CASE).annotate(
            count=Count('id')).order_by('day')
        not_real_case = trunc_day.filter(status=Appointment.STATUS_CHECK).exclude(
            state__in=Appointment.REAL_CASE).annotate(count=Count('id')).order_by('day')
        variables = {"total": total, "real_case": real_case, "not_real_case": not_real_case}
        return Response(variables)

    @list_route(methods=['GET'])
    def appointment(self, request, *args, **kwargs):
        q_appointment = self.get_queryset()

        trunc_day = q_appointment.annotate(day=TruncDay('created_time')).values('day')

        total = trunc_day.filter(status=Appointment.STATUS_RESERVE).annotate(count=Count('id'))
        real_case = trunc_day.filter(status=Appointment.STATUS_RESERVE).exclude(
            state__in=Appointment.NOT_REAL_CASE_RESERVE).annotate(count=Count('id')).order_by('day')
        not_real_case = trunc_day.filter(status=Appointment.STATUS_RESERVE,
                                         state__in=Appointment.NOT_REAL_CASE_RESERVE).annotate(
            count=Count('id')).order_by('day')
        variables = {"total": total, "real_case": real_case, "not_real_case": not_real_case}
        return Response(variables)


back_office_router = routers.SimpleRouter()
back_office_router.register(r'reports', ReportBackOfficeViewSet, 'reports')
