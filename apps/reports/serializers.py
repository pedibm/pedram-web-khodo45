# -*- coding: utf-8 -*-
from rest_framework import serializers
from django.conf import settings
from django.utils import timezone


class CheckCreatedTimeSerializer(serializers.Serializer):
    from_date = serializers.DateTimeField()
    to_date = serializers.DateTimeField()
