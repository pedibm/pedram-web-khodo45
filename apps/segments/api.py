from rest_framework import viewsets, routers, mixins

from .models import Segment
from .serializers import SegmentSerializer, SegmentBackOfficeSerializer, SegmentSerializerBO


class SegmentViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """exist an id for each questions list this id is 'segment id' that use in inspection , ..."""
    model = Segment
    serializer_class = SegmentSerializer
    queryset = Segment.objects.all()


class SegmentBackOfficeViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """exist an id for each questions list this id is 'segment id' that use in inspection , ..."""
    model = Segment
    serializer_class = SegmentBackOfficeSerializer
    queryset = Segment.objects.all()

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = SegmentSerializerBO
        return super(SegmentBackOfficeViewSet, self).retrieve(request, *args, **kwargs)


router = routers.DefaultRouter()
router.register(r'segments', SegmentViewSet, 'Segments')

back_office_router = routers.SimpleRouter()
back_office_router.register(r'segments', SegmentBackOfficeViewSet, 'Segments')
