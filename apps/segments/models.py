from django.db import models
from django.utils.translation import ugettext_lazy as _


class Segment(models.Model):
    """"
    Inspection Segments
    """

    title = models.CharField(
        _('title'), max_length=200,
        help_text=_('Segment Title'),
    )
    help = models.CharField(
        _('help'), max_length=50,
        help_text=_('help text'),
        null=True, blank=True
    )

    order = models.IntegerField(null=True, blank=True)
    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def question_data(self, inspection):
        from apps.segments.serializers import SegmentSerializer
        data = SegmentSerializer(self, inspection=inspection).data
        return data

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['order']
