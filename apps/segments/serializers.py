# -*- coding: utf-8 -*-

from rest_framework import serializers

from apps.fields.models import Question
from apps.inspections.models import InspectionData
from apps.appointments.models import Appointment
from .models import Segment
from jalali_date import datetime2jalali, date2jalali


class SegmentSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()

    class Meta:
        model = Segment
        exclude = ['created_time', ]

    def __init__(self, *args, **kwargs):
        self._inspection = kwargs.pop('inspection', None)
        super(SegmentSerializer, self).__init__(*args, **kwargs)

    def get_questions(self, obj):
        from apps.fields.serializers import QuestionSerializer
        qs = obj.question_set.all()
        return QuestionSerializer(qs, many=True, inspection=self._inspection).data


# TODO: Merge this class with "SegmentSerializer" class if they have same usage.
class SegmentSerializerBO(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()

    class Meta:
        model = Segment
        exclude = ['created_time', ]

    def __init__(self, *args, **kwargs):
        self._inspection = kwargs.pop('inspection', None)
        super(SegmentSerializerBO, self).__init__(*args, **kwargs)

    def get_questions(self, obj):
        from apps.fields.serializers import QuestionSerializer
        qs = obj.question_set.all().exclude(is_hidden=True)
        return QuestionSerializer(qs, many=True, inspection=self._inspection).data


class SegmentBackOfficeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Segment
        exclude = ['created_time', ]

    def __init__(self, *args, **kwargs):
        self._inspection = kwargs.pop('inspection', None)
        super(SegmentBackOfficeSerializer, self).__init__(*args, **kwargs)


class SegmentListSerializer(serializers.ModelSerializer):
    rating = serializers.SerializerMethodField()

    class Meta:
        model = Segment
        exclude = ['created_time', ]

    def __init__(self, *args, **kwargs):
        self._inspection = kwargs.pop('inspection', None)
        super(SegmentListSerializer, self).__init__(*args, **kwargs)

    def get_rating(self, obj):
        try:
            r = Question.objects.get(segment=obj, field_type=Question.TYPE_RATING)
        except Question.DoesNotExist:
            return None
        try:
            d = InspectionData.objects.get(question_id=r.id, inspection=self._inspection)
        except InspectionData.DoesNotExist:
            return None
        return d.answer


class SegmentSummarySerializer(SegmentListSerializer):
    pass


class SegmentMiniSerializer(serializers.ModelSerializer):

    class Meta:
        model = Segment
        fields = ['title', ]


class SegmentListDealerSerializer(serializers.ModelSerializer):
    rating = serializers.SerializerMethodField()

    class Meta:
        model = Segment
        exclude = ['created_time', ]

    def __init__(self, *args, **kwargs):
        self._inspection = kwargs.pop('inspection', None)
        super(SegmentListDealerSerializer, self).__init__(*args, **kwargs)

    def get_rating(self, obj):
        try:
            r = Question.objects.get(segment=obj, field_type=Question.TYPE_RATING)
        except Question.DoesNotExist:
            return None
        try:
            d = InspectionData.objects.get(question_id=r.id, inspection=self._inspection)
        except InspectionData.DoesNotExist:
            return None
        return d.answer


class SegmentDealerSerializer(SegmentSerializer):
    def get_questions(self, obj):
        from apps.fields.serializers import QuestionSerializer

        qs = obj.question_set.all().exclude(
            field_type__in=[Question.TYPE_IMAGE_ADDITIONAL, Question.TYPE_IMAGE_MAIN, Question.TYPE_POPUP,
                            Question.TYPE_RATING]).exclude(private=True)
        return QuestionSerializer(qs, many=True, inspection=self._inspection).data


class DateSerializer(serializers.Serializer):
    text = serializers.SerializerMethodField()
    value = serializers.SerializerMethodField()

    def get_text(self, obj):
        import jdatetime
        jdatetime.set_locale('fa_IR')
        jalali = datetime2jalali(obj).strftime('%A %d %B %Y')
        return jalali

    def get_value(self, obj):
        d = obj.strftime('%Y%m%d')
        return d


class HoursSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        self._branch = kwargs.pop('branch', None)
        super(HoursSerializer, self).__init__(self, *args, **kwargs)

    text = serializers.SerializerMethodField()
    value = serializers.SerializerMethodField()
    count = serializers.SerializerMethodField()
    busy = serializers.SerializerMethodField()
    full = serializers.SerializerMethodField()
    _count = None

    def get_text(self, obj):
        d = '{}:{:02d}'.format(obj.hour, obj.minute)
        return d

    def get_value(self, obj):
        d = obj.strftime('%Y-%m-%dT%H:%M')
        return d

    def get_count(self, obj):
        count = Appointment.objects.filter(reserve=obj, branch__id=self._branch).count()
        self._count = count
        return count

    def get_busy(self, obj):
        if self._count > 2:
            return True
        else:
            return False

    def get_full(self, obj):
        if self._count > 4:
            return True
        else:
            return False
