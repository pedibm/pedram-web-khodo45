from django.db import models
from django.utils.translation import ugettext_lazy as _


class SEO(models.Model):

    meta_title = models.CharField(
        _('meta title'), max_length=200,
        help_text=_('Meta tag Title'),
    )
    meta_description = models.CharField(
        _('description'), max_length=200,
        help_text=_('meta tag description'),
    )
    meta_keywords = models.CharField(
        _('keywords'), max_length=200,
        help_text=_('meta tag Keywords, seprate with `,`'),
    )

    class Meta:
        abstract = True
