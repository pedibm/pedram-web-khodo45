# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Testimonials


class TestimonialsAdmin(admin.ModelAdmin):
    list_display = ('name', 'text', 'car_identify',)
    search_fields = ('name', 'text', 'car_identify',)


admin.site.register(Testimonials, TestimonialsAdmin)
