# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class Testimonials(models.Model):
    """"
    Used in Old Site, No Need now
    """

    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to="photos", null=True, blank=True)
    text = models.TextField(max_length=500)
    car_identify = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

