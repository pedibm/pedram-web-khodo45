from rest_framework import viewsets, routers, mixins
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import filters
import django_filters.rest_framework
import string

from apps.dealers.serializers import DealerRegisterSerializer, DealerInfoSerializer
from apps.firebase.serializers import DeviceSerializer
from apps.users.tasks import send_login_token, send_not_active_message, send_not_user_message
from carpars.utilities import Redis, random_generate

from carpars.fields import persianize
from .models import User
from .serializers import FullUserSerializer, UserInfoSerializer, UserMobileSerializer, LoginSmsSerializer, \
    LoginTokenSerializer, UserCreateSerializer, UserCenterSerializer


class UserViewSet(mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    """
    User resource [Authentication needed].
    ---
    login:
        omit_serializer: true
        parameters_strategy:
            form: replace
        parameters:
         - name: username
           type: string
         - name: password
           type: string


    """

    model = User
    serializer_class = FullUserSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)

    def get_permissions(self):
        if (
                self.request.path.endswith('/login/') or
                self.request.path.endswith('/save_mobile/') or
                self.request.path.endswith('/login_sms/') or
                self.request.path.endswith('/login_token/') or
                self.request.path.endswith('/register/')
        ) and self.request.method == 'POST':
            self.permission_classes = []
        else:
            self.permission_classes = [IsAuthenticated, ]
        return super(UserViewSet, self).get_permissions()

    @list_route(methods=['POST'])
    def login(self, request):
        """
            Get User Token
            copy this to box for sample:
            {"password":"","username":""}
        """
        data = {}
        serializer = AuthTokenSerializer(data=request.data)
        if serializer.is_valid():
            token, created = Token.objects.get_or_create(user=serializer.validated_data['user'])
            data['user'] = self.serializer_class(token.user).data
            data['token'] = token.key
            return Response(data)
        return Response(serializer.errors, status=400)

    @list_route(methods=['POST'])
    def logout(self, request):
        """
            Logout User
        """
        return Response(status=200)

    @list_route(methods=['POST'])
    def login_sms(self, request):
        """
            Login User By SMS
        """
        serializer = LoginSmsSerializer(data=request.data)
        if serializer.is_valid():
            u = User.get_by_mobile(serializer.validated_data['mobile'])
            if not u:
                send_not_user_message.delay(serializer.validated_data['mobile'])
                return Response(status=200)  # FIXME: make some log or anything you know
            if not u.is_active:
                send_not_active_message.delay(serializer.validated_data['mobile'])
                return Response(status=200)
            r = Redis.get_instance().conn

            g = r.get('login_token_' + str(serializer.validated_data['mobile']))
            if g:
                token = g.decode('utf-8')
            else:
                token = random_generate(size=5, chars=string.digits)
                r.set('login_token_' + str(serializer.validated_data['mobile']), token, 3600)
            send_login_token.delay(serializer.validated_data['mobile'], token)
            return Response(status=200)

        return Response(serializer.errors, status=400)

    @list_route(methods=['POST'])
    def login_token(self, request):
        """
            Login User By SMS
        """
        serializer = LoginTokenSerializer(data=request.data)
        if serializer.is_valid():
            r = Redis.get_instance().conn
            token = r.get('login_token_' + str(serializer.validated_data['mobile']))
            if token and int(token) == serializer.validated_data['token']:
                u = User.get_by_mobile(serializer.validated_data['mobile'])
                if u:
                    data = {}
                    token, created = Token.objects.get_or_create(user=u)
                    data['user'] = self.serializer_class(token.user).data
                    data['token'] = token.key
                    r.delete('login_token_' + str(serializer.validated_data['mobile']))
                    return Response(data)
            return Response({'error': 'invalid data'}, status=400)
        return Response(serializer.errors, status=400)

    @list_route(methods=['GET'])
    def info(self, request):
        """
        Users Info
            ---
            parameters:
                - name: Authorization
                  description: Token
                  required: True
                  type: json
                  Parameter Type: json
        """
        user = UserInfoSerializer(request.user).data
        res = Response(user)
        res['Cache-Control'] = 'no-cache'
        return res

    @list_route(methods=['GET'])
    def dealer(self, request):
        """
        Users Info
            ---
            parameters:
                - name: Authorization
                  description: Token
                  required: True
                  type: json
                  Parameter Type: json
        """
        user = DealerInfoSerializer(request.user).data
        res = Response(user)
        res['Cache-Control'] = 'no-cache'
        return res

    @list_route(methods=['GET'])
    def center(self, request):
        center = UserCenterSerializer(request.user).data
        res = Response(center)
        res['Cache-Control'] = 'no-cache'
        return res

    @list_route(methods=['POST'])
    def save_mobile(self, request):
        """
            Save Mobile Number For A User
        """
        mobile_number = request.data.get('number')
        user = UserMobileSerializer(data={"username": mobile_number, "mobile": mobile_number})
        if not user.is_valid():
            return Response(user.errors)
        else:
            u = User()
            u.username = user.validated_data['mobile']
            u.set_password(user.validated_data['mobile'])
            u.mobile = user.validated_data['mobile']
            u.is_dealer = True
            u.save()
            from apps.users.tasks import send_notify_dealer
            send_notify_dealer.delay(u)
            return Response(200)

    @list_route(methods=['POST'])
    def token(self, request):
        """
        Users Token
            ---
            parameters:
                - name: token
                  description: Firebase Token
                  required: True
                  type: String
        """
        device = DeviceSerializer(data=request.data)
        if not device.is_valid():
            res = Response(device.errors, status=400)
            res['Cache-Control'] = 'no-cache'
            return res
        instance = device.save(user=self.request.user)
        res = Response(device.data)
        res['Cache-Control'] = 'no-cache'
        return res

    @list_route(methods=['POST'])
    def register(self, request):
        """
        Dealer Register
            ---
            parameters:
                - name: mobile
                  description: Dealer Mobile Number
                  required: True
                  type: Integer
                - name: city
                  description: City Id
                  required: True
                  type: Integer
                - name: address
                  description: Address
                  required: True
                  type: string
        """
        dealer = DealerRegisterSerializer(data=request.data, context={'request': request})
        if not dealer.is_valid():
            res = Response(dealer.errors, status=400)
            res['Cache-Control'] = 'no-cache'
            return res
        dealer.save()
        res = Response({"success": True})
        res['Cache-Control'] = 'no-cache'
        return res


class OrderFilter(django_filters.FilterSet):
    class Meta:
        model = User
        fields = {
            'username': ['icontains', 'exact'],
            'first_name': ['icontains', 'exact'],
            'last_name': ['icontains', 'exact'],
            'email': ['icontains', 'exact'],
            'mobile': ['icontains', 'exact'],
        }


class UserBackOfficeViewSet(mixins.ListModelMixin,
                            mixins.RetrieveModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.CreateModelMixin,
                            viewsets.GenericViewSet):
    model = User
    serializer_class = UserInfoSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       filters.OrderingFilter,
                       )
    filter_class = OrderFilter

    def get_permissions(self):
        if (
                self.request.path.endswith('/login/') or
                self.request.path.endswith('/login_sms/') or
                self.request.path.endswith('/login_token/')
        ) and self.request.method == 'POST':
            self.permission_classes = []
        else:
            self.permission_classes = [IsAuthenticated, ]
        return super(UserBackOfficeViewSet, self).get_permissions()

    def get_queryset(self):
        queryset = User.objects.all()
        if not self.request.user.is_superuser:
            queryset = queryset.filter(is_dealer=True, agent=self.request.user)
        return queryset

    def create(self, request, *args, **kwargs):
        request.data.update({"agent": request.user.id})
        request.data['username'] = persianize(request.data['username'])
        self.serializer_class = UserCreateSerializer
        return super(UserBackOfficeViewSet, self).create(request, *args, **kwargs)

    @list_route(methods=['GET'])
    def info(self, request):
        user = UserInfoSerializer(request.user).data
        res = Response(user)
        res['Cache-Control'] = 'no-cache'
        return res

    @list_route(methods=['POST'])
    def login(self, request):
        """
            Get User Token
            copy this to box for sample:
            {"password":"","username":""}
        """
        data = {}
        serializer = AuthTokenSerializer(data=request.data)
        if serializer.is_valid():
            token, created = Token.objects.get_or_create(user=serializer.validated_data['user'])
            data['user'] = self.serializer_class(token.user).data
            data['token'] = token.key
            return Response(data)
        return Response(serializer.errors, status=400)

    @list_route(methods=['POST'])
    def change_password(self, request):
        if 'password' in request.data and 'confirm_password' in request.data:
            if request.data['password'] == request.data['confirm_password']:
                request.user.set_password(request.data['password'])
            else:
                return Response(status=400)

        if 'first_name' in request.data:
            request.user.first_name = request.data['first_name']
        if 'last_name' in request.data:
            request.user.last_name = request.data['last_name']
        request.user.save()
        return Response(status=200)

    @list_route(methods=['POST'])
    def login_sms(self, request):
        """
            Login User By SMS
        """
        serializer = LoginSmsSerializer(data=request.data)
        if serializer.is_valid():
            u = User.get_by_mobile(serializer.validated_data['mobile'])
            if not u:
                send_not_user_message.delay(serializer.validated_data['mobile'])
                return Response(status=200)  # FIXME: make some log or anything you know
            if not u.is_active:
                send_not_active_message.delay(serializer.validated_data['mobile'])
                return Response(status=200)
            r = Redis.get_instance().conn

            g = r.get('login_token_' + str(serializer.validated_data['mobile']))
            if g:
                token = g.decode('utf-8')
            else:
                token = random_generate(size=5, chars=string.digits)
                r.set('login_token_' + str(serializer.validated_data['mobile']), token, 3600)
            send_login_token.delay(serializer.validated_data['mobile'], token)
            return Response(status=200)

        return Response(serializer.errors, status=400)

    @list_route(methods=['POST'])
    def login_token(self, request):
        """
            Login User By SMS
        """
        serializer = LoginTokenSerializer(data=request.data)
        if serializer.is_valid():
            r = Redis.get_instance().conn
            token = r.get('login_token_' + str(serializer.validated_data['mobile']))
            if token and int(token) == serializer.validated_data['token']:
                u = User.get_by_mobile(serializer.validated_data['mobile'])
                if u:
                    data = {}
                    token, created = Token.objects.get_or_create(user=u)
                    data['user'] = self.serializer_class(token.user).data
                    data['token'] = token.key
                    r.delete('login_token_' + str(serializer.validated_data['mobile']))
                    return Response(data)
            return Response({'error': 'invalid data'}, status=400)
        return Response(serializer.errors, status=400)

router = routers.DefaultRouter()
router.register(r'user', UserViewSet, base_name='User')

back_office_router = routers.SimpleRouter()
back_office_router.register(r'user', UserBackOfficeViewSet, 'Users')
