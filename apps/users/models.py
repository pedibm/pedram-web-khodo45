from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from carpars.fields import NormalizerCharField


class User(AbstractUser):
    verify_code = models.CharField(_('verify code'), max_length=30,
                                   null=True,
                                   blank=True)
    is_dealer = models.BooleanField(_('is dealer'),
                                    blank=True, default=False)
    mobile = models.CharField(_('Mobile Number'), max_length=11,
                              null=True,
                              blank=True)
    agent = models.ForeignKey('self', blank=True, null=True)

    ROLE_CALL_CENTER = '0'
    ROLE_QC = '1'
    ROLE_SELLS = '2'
    ROLE_SELLS_SUPPORT = '4'
    ROLE_INSPECTOR = '3'

    USER_ROLES = (
        (ROLE_CALL_CENTER, _('Call Center')),
        (ROLE_QC, _('Qc')),
        (ROLE_SELLS, _('Sells')),
        (ROLE_SELLS_SUPPORT, _('Sells Support')),
        (ROLE_INSPECTOR, _('Inspector')),
    )

    role = models.CharField(max_length=1, choices=USER_ROLES, blank=True)

    username = NormalizerCharField(_('username'),
                                   max_length=150,
                                   unique=True,
                                   help_text=_(
                                       'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
                                   )
    password = models.CharField(_('password'), max_length=128, blank=True, null=True)

    @staticmethod
    def get_by_mobile(mobile):
        try:
            u = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            try:
                u = User.objects.get(username=mobile)
            except User.DoesNotExist:
                return None
        return u

    @property
    def _is_confirmed(self):
        if self.is_staff:  # All Staff was confirmed
            return True
        from apps.dealers.models import Dealer
        try:
            d = Dealer.objects.get(id=self.id)
        except Dealer.DoesNotExist:
            # Not a DEALER or Staff what the hell!?
            return False
        return d.is_confirmed
