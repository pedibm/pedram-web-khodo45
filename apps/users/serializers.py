from rest_framework import serializers

from apps.dealers.models import Dealer
from carpars.utilities import convert_fa_numbers
from .models import User


class FullUserSerializer(serializers.ModelSerializer):
    is_agent = serializers.SerializerMethodField()
    is_confirmed = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(FullUserSerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = [u'id', 'username', 'first_name', 'role', 'is_agent', 'is_confirmed']
        read_only_fields = ['id', 'last_name', 'first_name']

    def get_is_agent(self, obj):
        if obj.is_staff and obj.role == User.ROLE_SELLS:
            return True
        return False

    def get_is_confirmed(self, obj):
        if obj.is_staff: # All Staff was confirmed
            return True
        try:
            d = Dealer.objects.get(id=obj.id)
        except Dealer.DoesNotExist:
            # Not a DEALER or Staff what the hell!?
            return False
        return d.is_confirmed


class UserMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [u'id', 'username', 'first_name', 'last_name', 'mobile']
        read_only_fields = ['id', 'first_name']


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('password',)

    agent_info = serializers.SerializerMethodField()
    is_agent = serializers.SerializerMethodField()
    center = serializers.SerializerMethodField()

    def get_agent_info(self, obj):
        if obj.agent:
            agent_data = {"username": obj.agent.username, "mobile": obj.agent.mobile}
        else:
            agent_data = {"username": None, "mobile": None}
        return agent_data

    def get_center(self, obj):
        center = {"title": u"ملاصدرا", "phone": "021-91004502", "phone_without_dash": "02191004502"}
        return center

    def get_is_agent(self, obj):
        if obj.is_staff and obj.role == User.ROLE_SELLS:
            return True
        return False


class UserCenterSerializer(UserInfoSerializer):
    class Meta:
        model = User
        fields = ('center', 'agent_info')


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'agent', 'last_name', 'is_active', 'is_dealer']

    def create(self, validated_data):
        user = super(UserCreateSerializer, self).create(validated_data)
        user.is_dealer = True
        user.mobile = validated_data['username']
        user.save()
        return user

    def validate_username(self, value):
        value = convert_fa_numbers(value)
        try:
            int(value)
        except:
            raise serializers.ValidationError("user name must be number not string")
        if value[0: 2] == "09" or value[0: 2] == "۰۹":
            pass
        else:
            raise serializers.ValidationError("user name must start with 09")
        if len(value) != 11:
            raise serializers.ValidationError("user name digits not right")
        return value


class UserMobileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'mobile', 'is_dealer']

    def validate_mobile(self, value):
        value = convert_fa_numbers(value)
        try:
            int(value)
        except:
            raise serializers.ValidationError("Mobile number must be number not string")
        if value[0: 2] != "09":
            raise serializers.ValidationError("Mobile number must start with 09")
        if len(value) != 11:
            raise serializers.ValidationError("Mobile number digits not right")
        return value


class LoginSmsSerializer(serializers.Serializer):
    mobile = serializers.CharField()

    def validate_mobile(self, value):
        value = convert_fa_numbers(value)
        try:
            int(value)
        except:
            raise serializers.ValidationError("Mobile number must be number not string")
        if value[0: 2] != "09":
            raise serializers.ValidationError("Mobile number must start with 09")
        if len(value) != 11:
            raise serializers.ValidationError("Mobile number digits not right")
        return value


class LoginTokenSerializer(LoginSmsSerializer):
    token = serializers.CharField()

    def validate_token(self, value):
        value = convert_fa_numbers(value)
        try:
            int(value)
        except:
            raise serializers.ValidationError("Token must be number not string")
        if len(value) != 5:
            raise serializers.ValidationError("Token digits not right")
        return int(value)
