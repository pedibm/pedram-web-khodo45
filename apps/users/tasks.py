# -*- coding: utf-8 -*-

from celery import task
from django.template import Context
from django.template.loader import get_template

from apps.appointments.tasks import send_sms


@task
def send_notify_dealer(user):

    variables = {'User': user}
    template = get_template('notification/dealer_sms.txt')
    context = Context(variables)
    send_sms(user, context, template, "User")


@task
def send_login_token(mobile, token):

    variables = {'token': token}
    template = get_template('user/login_token.txt')
    context = Context(variables)
    send_sms(mobile, context, template, "login_token")


@task
def send_not_active_message(mobile):

    variables = {}
    template = get_template('user/login_not_active.txt')
    context = Context(variables)
    send_sms(mobile, context, template, "login_token")


@task
def send_not_user_message(mobile):

    variables = {}
    template = get_template('user/login_not_user.txt')
    context = Context(variables)
    send_sms(mobile, context, template, "login_token")
