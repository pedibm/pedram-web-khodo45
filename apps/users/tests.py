"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APIClient

from .models import User


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        u = User()
        u.username = 'admin'
        u.set_password('admin')
        u.is_staff = True
        u.is_superuser = True
        u.save()

        u = User.objects.last()
        self.assertEqual(u.is_superuser, True)
        print('Done')


class APITest(APITestCase):
    def test_login_api(self):
        """
        Test Login with API
        :return:
        """
        user = 'admin'
        password = 'admin'
        u = User()
        u.username = user
        u.set_password(password)
        u.save()

        url = reverse('User-login')
        data = {'username': user, 'password': password}
        req = self.client.post(url, data, format='json')
        self.assertEqual(req.status_code, status.HTTP_200_OK)
        self.assertIsNot(req.data['token'], None)

    def test_get(self):
        """
        Test Login with API
        :return:
        """
        user = 'admin'
        password = 'admin'
        u = User()
        u.username = user
        u.set_password(password)
        u.save()
        token, created = Token.objects.get_or_create(user=u)

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        url = reverse('User-detail', args=(u.id,))
        req = client.get(url)
        self.assertEqual(req.status_code, status.HTTP_200_OK)
        self.assertIs(req.data['id'], u.id)
