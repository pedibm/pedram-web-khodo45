# -*- coding: utf-8 -*-
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from .models import User


def register_verify(request, verify_code):
    user = get_object_or_404(User, verify_code=verify_code)

    if user.email_verified:
        raise Http404
    else:
        user.email_verified = True
        user.verify_code = None
        user.save()

        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)

        variables = RequestContext(request, {
            'user': user,
        })
        return render_to_response(
            'registration/register_activate.html',
            variables
        )


@login_required
def login_check(request):
    variables = RequestContext(request, {
        'user': request.user,
    })
    return render_to_response(
        'registration/login_check.html',
        variables
    )
