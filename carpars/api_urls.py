from django.conf.urls import include, url
from apps.fields.api import router as question_router
from apps.segments.api import router as segments_router
from apps.cars.api import router as cars_router
from apps.locations.api import router as locations_router
from apps.appointments.api import router as appointments_router
from apps.images.api import router as images_router
from apps.inspections.api import router as inspections_router
from apps.users.api import router as users_router
from apps.auctions.api import router as auctions_router
from apps.firebase.api import router as firebase_router
from apps.dealers.api import router as dealers_router
from apps.releases.api import router as releases_router


urlpatterns = [
    url(r'', include(question_router.urls)),
    url(r'', include(segments_router.urls)),
    url(r'', include(cars_router.urls)),
    url(r'', include(locations_router.urls)),
    url(r'', include(appointments_router.urls)),
    url(r'', include(images_router.urls)),
    url(r'', include(inspections_router.urls)),
    url(r'', include(users_router.urls)),
    url(r'', include(auctions_router.urls)),
    url(r'', include(firebase_router.urls)),
    url(r'', include(dealers_router.urls)),
    url(r'', include(releases_router.urls)),
]
