from django.conf.urls import include, url
from apps.fields.api import back_office_router as question_router
from apps.segments.api import back_office_router as segments_router
from apps.cars.api import back_office_router as cars_back_office_router
# from apps.cars.api import router as cars_router
from apps.locations.api import router as locations_router
from apps.appointments.api import back_office_router as appointments_router
from apps.images.api import router as images_router
from apps.inspections.api import router as inspections_router
# from apps.users.api import router as users_router
from apps.users.api import back_office_router as user_back_office_router
from apps.auctions.api import back_office_router as auctions_router
from apps.reports.api import back_office_router as reports_router
from apps.dealers.api import back_office_router as dealers_router


urlpatterns = [
    url(r'', include(question_router.urls)),
    url(r'', include(segments_router.urls)),
    # url(r'', include(cars_router.urls)),
    url(r'', include(cars_back_office_router.urls)),
    url(r'', include(locations_router.urls)),
    url(r'', include(appointments_router.urls)),
    url(r'', include(images_router.urls)),
    url(r'', include(inspections_router.urls)),
    # url(r'', include(users_router.urls)),
    url(r'', include(auctions_router.urls)),
    url(r'', include(user_back_office_router.urls)),
    url(r'', include(reports_router.urls)),
    url(r'', include(dealers_router.urls)),
]
