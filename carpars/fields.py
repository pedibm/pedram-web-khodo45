# -*- coding: utf-8 -*-
from django.db import models


class NormalizerCharField(models.CharField):
    def normalize_value(self, value):
        return persianize(value)

    def get_db_prep_save(self, value, *args, **kwargs):
        value = self.normalize_value(value)
        return super(NormalizerCharField, self).get_db_prep_save(value, *args, **kwargs)


class NormalizerTextField(models.TextField):
    def normalize_value(self, value):
        return persianize(value)

    def get_db_prep_save(self, value, *args, **kwargs):
        value = self.normalize_value(value)
        return super(NormalizerTextField, self).get_db_prep_save(value, *args, **kwargs)


def persianize(value):
    if not value:
        return value
    per_numbers = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"]
    eng_numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    i = 0
    while i < len(per_numbers):
        value = value.replace(per_numbers[i], eng_numbers[i])
        i = i + 1
    return value
