import redis


class Redis():
    ttl = 90000000
    duplicate_count = {}
    save_count = {}
    message_count = {}

    def __init__(self):
        self.conn = redis.StrictRedis('redis')

    def check_duplicate(self, topic, id):
        if self.conn.exists(self.gen_key(topic, id)):
            self.inc_duplicate(topic)
            return True
        return False

    def save_id(self, topic, id):
        self.inc_save(topic)
        key = self.gen_key(topic, id)
        return self.conn.setex(key, self.ttl, 1)

    def flush(self):
        total = 0
        for topic in self.duplicate_count.keys():
            total = total + self.duplicate_count[topic]
            self.conn.incrby(topic + '_duplicate', self.duplicate_count[topic])
        self.conn.incrby('total_duplicate', total)

        total = 0
        for topic in self.save_count.keys():
            total = total + self.save_count[topic]
            self.conn.incrby(topic + '_save', self.save_count[topic])
        self.conn.incrby('total_save', total)

        total = 0
        for topic in self.message_count.keys():
            total = total + self.message_count[topic]
            self.conn.incrby(topic + '_message', self.message_count[topic])
        self.conn.incrby('total_message', total)

        self.duplicate_count = {}
        self.save_count = {}
        self.message_count = {}

    def inc_duplicate(self, topic):
        if not topic in self.duplicate_count:
            self.duplicate_count[topic] = 0
        self.duplicate_count[topic] = self.duplicate_count[topic] + 1

    def inc_message(self, topic):
        if not topic in self.message_count:
            self.message_count[topic] = 0
        self.message_count[topic] = self.message_count[topic] + 1

    def inc_save(self, topic):
        if not topic in self.save_count:
            self.save_count[topic] = 0
        self.save_count[topic] = self.save_count[topic] + 1

    def check_save(self, topic, id):
        if not self.check_duplicate(topic, id):
            self.save_id(topic, id)
            return True
        return False

    def gen_key(self, topic, id):
        return topic + '_' + str(id)
