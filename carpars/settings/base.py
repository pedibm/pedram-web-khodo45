# Django settings

import os.path

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../'))
PROJECT_NAME = os.path.basename(ROOT_DIR)


def ABS_PATH(*args):
    return os.path.join(ROOT_DIR, *args)


def ENV_SETTING(key, default):
    import os
    return os.environ.get(key, default)


ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Tehran'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# If you set this to False, Django will treat all time values as local to
# the specified timezone.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ABS_PATH('media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
# MEDIA_URL = 'http://carpars.com/media/'
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ABS_PATH('static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    ABS_PATH('staticfiles'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Import the secret key
try:
    from .secret import SECRET_KEY  # noqa
except ImportError:
    from django.utils.crypto import get_random_string

    SECRET_KEY = ENV_SETTING('SECRET_KEY', get_random_string(50,
                                                             'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'))

MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = PROJECT_NAME + '.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            ABS_PATH('templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'djcelery',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_swagger',
    'sorl.thumbnail',
    'ckeditor',
    'django_filters',
    'corsheaders',
    'import_export',
    'imagekit',
    # Apps
    'apps.main',
    'apps.users',
    'apps.fields',
    'apps.segments',
    'apps.faq',
    'apps.testimonials',
    'apps.cars',
    'apps.seo',
    'apps.locations',
    'apps.appointments',
    'apps.images',
    'apps.inspections',
    'apps.auctions',
    'apps.currency_price',
    'apps.firebase',
    'apps.dealers',
    'apps.releases',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'handlers': {
        'console_error_production': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'logging.StreamHandler'
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['console_error_production'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

AUTH_USER_MODEL = "users.User"

import djcelery

djcelery.setup_loader()

# Application Config

REDIS_HOST = 'redis'
REDIS_PORT = 6379

LOGIN_REDIRECT_URL = '/'

HOSTNAME = 'STAGING'
RETRY_SAVE_NEWS = 3
# APPEND_SLASH=False

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # 'rest_framework.authentication.TokenAuthentication',
        'carpars.utilities.TokenAuthenticationWithSideffects',
    ),
}
# For Django >= 1.6 test runner setting
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

BROKER_URL = 'amqp://amqp:5672/'
CELERY_RESULT_BACKEND = 'database'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
CELERY_IGNORE_RESULT = False

BASE_URL = ENV_SETTING('BASE_URL', 'https://khodro45.com')

RIZZO_TOKEN = ENV_SETTING('RIZZO_TOKEN', '')
RIZZO_BASE_URL = 'http://rizzo.site'
RIZZO_INTERVAL_24MIN = 21
RIZZO_INTERVAL_53MIN = 22
RIZZO_REDIS = "178.62.180.92"

KEY_TIMEOUT = 259200  # 3 days

SMS_NIK = {
        'src': 5000454500,
        'username': '09123903974',
        'password': 'Carpars2018!',
}

SMS_PAYAM = {
    'from': 20004545,
    'apikey': ENV_SETTING('PAYAM_TOKEN', '')
}

SMS_OPERATOR = ENV_SETTING('SMS_OPERATOR', 'payam')

CKEDITOR_UPLOAD_PATH = "ckeditor/"

SWAGGER_SETTINGS = {
    'USE_SESSION_AUTH': False,
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
    'SUPPORTED_SUBMIT_METHODS': ['get', 'post', 'put', 'delete', 'patch'],
}
CORS_ORIGIN_ALLOW_ALL = True
FCM_TOKEN = ENV_SETTING('FCM_TOKEN', '')

STAGING_NOTIF_IDENTIFIER = '-- '
ONLINE_PREFIX = 'online_{}'
ONLINE_THRESHOLD = 60 * 10
