(function($) {
    $(document).ready(function() {
        $('#CF5aa2d93b4814d_1').on('submit', function(e) {
            var dataToBeSent = {
                "branch": $("select[name='branch']").val(),
                "reserve": $("select[name='fld_7869156']").val(),
                "name": $("input[name='fld_869314']").val(),
                "email": $("input[name='fld_6279150']").val(),
            };
            $.LoadingOverlay("show");
            $.ajax({
                url: '/api/v1/appointments/' + $("input[name='appointment_id']").val() + '/',
                type: "PATCH",
                data: dataToBeSent,
                success: function(data, status) {
                    $.LoadingOverlay("hide")
                    $("#reserve_description").html("رزرو شما با موفقیت کامل شد<br />به زودی همکاران ما با شما تماس میگیرند");
                    $('#fld_9114710_1').attr('disabled', true)
                    $('.vc_custom_1528006352823').html("<span style=\"    padding: 10px;\n" +
                        "    background-color: #1edb84;\n" +
                        "    border-radius: 10px;\n" +
                        "    color: black;\n" +
                        "    font-size: 14px;\n" +
                        "    font-weight: bold;\">" +
                        "رزرو شما با موفقیت کامل شد\n" +
                        "به زودی همکاران ما با شما تماس میگیرند\n" +
                        "\n</span>")

                }
            })
            return false;

        });
    });
    $(".mk-skip-to-next").hide();
    setTimeout(function() {
        if ($("select[name='city']").length > 0) {
            // $(".vc_custom_1527924176593").hide();
            // $("#cjcrnfy9e000b3c62znai6a7d .mkhb-button-el").not("#cjcxb3b8200083c61ay4cmhj8").hide();
            var $dropdown = $("select[name='city']");
            var $dropdown2 = $("select[name='branch']");
            $dropdown2.prop("selectedIndex", 0);
            $("select[name='fld_2329261']").prop("disabled", "disabled");
            $("select[name='fld_7869156']").prop("disabled", "disabled");
            $dropdown2.prop("disabled", "disabled");
            $.LoadingOverlay("show");
            $.getJSON('/api/v1/city/', function(data) {
                $.LoadingOverlay("hide");
                $dropdown.find('option').not(':first').remove().end();
                $dropdown.prop("selectedIndex", 0);
                $.each(data, function(index, myitem) {
                    $dropdown.append($("<option />").val(myitem.id).text(myitem.title))
                })
            });
            var dataToBeSent = {
                // "MobileNumber": $("input[name='fld_4162664']").val(),
                "CarCompany": $("input[name='fld_1944725']").val(),
                "CarCategory": $("input[name='fld_7961605']").val(),
                "CarModel": $("input[name='fld_827952']").val(),
                "CarYear": $("input[name='fld_7866138']").val(),
                "CarUsage": $("input[name='fld_3438340']").val()
            };
            $.get('/api/v1/price/get/?appointment_id=' + $("input[name='appointment_id']").val(), function(data, status) {
                if (data.max > 0) {
                    $(".cars24PriceOrange p:first").text("حداقل " + data.min.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " و حداکثر " + data.max.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " تومان ")
                } else {
                    $(".cars24PriceOrange p:first").text("تعیین قیمت خودروی شما بعد از قرار کارشناسی رایگان")
                }
            }, "json");
            $("select[name='city']").change(function() {
                $("#reserve_description").html("شما در این بخش می توانید زمان کارشناسی خودروی خودتون رو مشخص کنید");
                var $dropdown = $("select[name='branch']");
                $("select[name='fld_2329261']").prop("disabled", !0);
                var dataToBeSent = {
                    "CityName": $("select[name='city']").val()
                };
                $dropdown.prop("disabled", !1);
                $.LoadingOverlay("show");
                $.getJSON('/api/v1/branch/?city_id=' + $("select[name='city']").val(), function(data, status) {
                    $.LoadingOverlay("hide");
                    $dropdown.find('option').not(':first').remove().end();
                    $dropdown.prop("selectedIndex", 0);
                    window.branches = data;
                    $.each(data, function(index, myitem) {
                        if (myitem.active) {
                            $dropdown.append($("<option />").val(myitem.id).text(myitem.title))
                        } else {
                            $dropdown.append($("<option />").val(myitem.id).text(myitem.title).attr('disabled', 'disabled'))
                        }
                    })
                })
            });
            $("select[name='branch']").change(function() {
                var $dropdown = $("select[name='fld_2329261']");
                var dataToBeSent = {
                    "branch": $("select[name='branch']").val()
                };
                var findItem = window.branches.find(x => x.id == $("select[name='branch']").val());
                if (findItem.active === true) {
                    $("#reserve_description").html((findItem.map == null ? "" : "برای مشاهده آدرس این شعبه بر روی <a href='" + findItem.map + "' target='_blank'> اینجا کلیک <a/> نمایید."));
                    $dropdown.prop("disabled", !1)
                } else {
                    $("#reserve_description").html("زمان برای این شعبه پر است");
                    $dropdown.prop("disabled", !0)
                }
                $.LoadingOverlay("show");
                $.get('/api/v1/appointments/days', dataToBeSent, function(data, status) {
                    $.LoadingOverlay("hide");
                    $dropdown.find('option').not(':first').remove().end();
                    $dropdown.prop("selectedIndex", 0);
                    $.each(data, function(index, myitem) {
                        $dropdown.append($("<option />").val(myitem.value).text(myitem.text))
                    })
                }, "json")
            });
            $("select[name='fld_2329261']").change(function() {
                var $dropdown = $("select[name='fld_7869156']");
                var dataToBeSent = {
                    "branch": $("select[name='branch']").val(),
                    "day": $("select[name='fld_2329261']").val()
                };
                $dropdown.prop("disabled", !1);
                $.LoadingOverlay("show");
                $.get('/api/v1/appointments/hours', dataToBeSent, function(data, status) {
                    $.LoadingOverlay("hide");
                    $dropdown.find('option').not(':first').remove().end();
                    $dropdown.prop("selectedIndex", 0);
                    $.each(data, function(index, myitem) {
                        $dropdown.append($("<option />").val(myitem.value).text(myitem.text))
                    })
                }, "json")
            })
        }
    }, 200);
    if ($("#fld_6086100_1").length > 0) {
        var $dropdown = $("#fld_6086100_1");
        var $dropdown2 = $("#fld_6086100_2");
        disableTillItem(0, 1);
        disableTillItem(0, 2);
        $.LoadingOverlay("show");
        $.getJSON('/api/v1/brands/',
            function(data) {
                $.LoadingOverlay("hide");
                $dropdown.find('option').not(':first').remove().end();
                $dropdown.prop("selectedIndex", 0);
                $dropdown2.find('option').not(':first').remove().end();
                $dropdown2.prop("selectedIndex", 0);
                $.each(data, function(index, myitem) {
                    $dropdown.append($("<option />").val(myitem.id).text(myitem.title));
                    $dropdown2.append($("<option />").val(myitem.id).text(myitem.title))
                })
            })
    }

    function disableTillItem(index, state) {
        if (state == 1) {
            if (index == 0) {
                $("#fld_9835059_1").prop("disabled", "disabled");
                $("#fld_279112_1").prop("disabled", "disabled");
                $("#fld_4151103_1").prop("disabled", "disabled");
                $("#fld_7465237_1").prop("disabled", "disabled");
                $("#fld_4236944_1").prop("disabled", "disabled")
            } else if (index == 1) {
                $("#fld_6086100_2").prop("selectedIndex", $("#fld_6086100_1").prop("selectedIndex")).change();
                $("#fld_9835059_1").prop("disabled", !1).prop("selectedIndex", 0);
                $("#fld_279112_1").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_4151103_1").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_7465237_1").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_4236944_1").prop("disabled", "disabled").prop("selectedIndex", 0)
            } else if (index == 2) {
                $("#fld_9835059_2").prop("selectedIndex", $("#fld_9835059_1").prop("selectedIndex")).change();
                $("#fld_279112_1").prop("disabled", !1).prop("selectedIndex", 0);
                $("#fld_4151103_1").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_7465237_1").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_4236944_1").prop("disabled", "disabled").prop("selectedIndex", 0)
            } else if (index == 3) {
                $("#fld_279112_2").prop("selectedIndex", $("#fld_279112_1").prop("selectedIndex")).change();
                $("#fld_4151103_1").prop("disabled", !1).prop("selectedIndex", 0);
                $("#fld_7465237_1").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_4236944_1").prop("disabled", "disabled").prop("selectedIndex", 0)
            } else if (index == 4) {
                $("#fld_4151103_2").prop("selectedIndex", $("#fld_4151103_1").prop("selectedIndex")).change();
                $("#fld_7465237_1").prop("disabled", !1).prop("selectedIndex", 0);
                $("#fld_4236944_1").prop("disabled", "disabled").prop("selectedIndex", 0)
            } else if (index == 5) {
                $("#fld_7465237_2").prop("selectedIndex", $("#fld_7465237_1").prop("selectedIndex")).change();
                $("#fld_4236944_1").prop("disabled", !1).prop("selectedIndex", 0)
            } else if (index == 6) {
                $("#fld_4236944_2").val($("#fld_4236944_1").val())
            }
        } else {
            if (index == 0) {
                $("#fld_9835059_2").prop("disabled", "disabled");
                $("#fld_279112_2").prop("disabled", "disabled");
                $("#fld_4151103_2").prop("disabled", "disabled");
                $("#fld_7465237_2").prop("disabled", "disabled");
                $("#fld_4236944_2").prop("disabled", "disabled")
            } else if (index == 1) {
                $("#fld_9835059_2").prop("disabled", !1).prop("selectedIndex", 0);
                $("#fld_279112_2").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_4151103_2").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_7465237_2").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_4236944_2").prop("disabled", "disabled").prop("selectedIndex", 0)
            } else if (index == 2) {
                $("#fld_279112_2").prop("disabled", !1).prop("selectedIndex", 0);
                $("#fld_4151103_2").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_7465237_2").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_4236944_2").prop("disabled", "disabled").prop("selectedIndex", 0)
            } else if (index == 3) {
                $("#fld_4151103_2").prop("disabled", !1).prop("selectedIndex", 0);
                $("#fld_7465237_2").prop("disabled", "disabled").prop("selectedIndex", 0);
                $("#fld_4236944_2").prop("disabled", "disabled").prop("selectedIndex", 0)
            } else if (index == 4) {
                $("#fld_7465237_2").prop("disabled", !1).prop("selectedIndex", 0);
                $("#fld_4236944_2").prop("disabled", "disabled").prop("selectedIndex", 0)
            } else if (index == 5) {
                $("#fld_4236944_2").prop("disabled", !1).prop("selectedIndex", 0)
            } else if (index == 6) {}
        }
    }
    $("#fld_6086100_1").change(function() {
        disableTillItem(1, 1);
        var $dropdown = $("#fld_9835059_1");
        var $dropdown2 = $("#fld_9835059_2");

        $.LoadingOverlay("show");
        $.getJSON('/api/v1/models/?brand_id=' + $("#fld_6086100_1").val(), function(data, status) {
            $.LoadingOverlay("hide");
            $dropdown.find('option').not(':first').remove().end();
            $dropdown2.find('option').not(':first').remove().end();
            $.each(data, function(index, myitem) {
                $dropdown.append($("<option />").val(myitem.id).text(myitem.title))
                $dropdown2.append($("<option />").val(myitem.id).text(myitem.title))
            })
        })
    });
    $("#fld_6086100_2").change(function() {
    disableTillItem(1, 2);
    var $dropdown = $("#fld_9835059_1");
    var $dropdown2 = $("#fld_9835059_2");

    $.LoadingOverlay("show");
    $.getJSON('/api/v1/models/?brand_id=' + $("#fld_6086100_2").val(), function(data, status) {
        $.LoadingOverlay("hide");
        $dropdown.find('option').not(':first').remove().end();
        $dropdown2.find('option').not(':first').remove().end();
        $.each(data, function(index, myitem) {
            $dropdown.append($("<option />").val(myitem.id).text(myitem.title))
            $dropdown2.append($("<option />").val(myitem.id).text(myitem.title))
            })
        })
    });


    $("#fld_279112_1").parent().parent().hide();
    $("#text-block-5").css({
        "margin-right": function() {
            if ($("#text-block-5").width() > 400) {
                return "65px"
            } else {
                return "0"
            }
        }
    });
    $("#fld_9835059_1").change(function() {
        // FIXME: New API, don't have a option
        // $dropdown.parent().parent().hide();
        $("#fld_279112_1").prop("disabled", !1);
        $("#text-block-5").css({
            "margin-right": "65px"
        });
        disableTillItem(2, 1);
        var $dropdown = $("#fld_4151103_1");

        $.LoadingOverlay("show");
        $.getJSON('/api/v1/years/?model_id=' + $("#fld_9835059_1").val(), function(data, status) {

        // $.get('', dataToBeSent, function(data, status) {
            $.LoadingOverlay("hide");
            $dropdown.find('option').not(':first').remove().end();
            var isFind = false;

            $.each(data, function (index, myitem) {
                $dropdown.append($("<option />").val(myitem.id).text(myitem.title));
            });
            $dropdown.prop("disabled", !1);
        }, "json")
    });
    $("#fld_279112_1").change(function() {
        // disableTillItem(3, 1)
        disableTillItem(4, 1)

    });
    $("#fld_4151103_1").change(function() {
        var $dropdown = $("#fld_279112_1");

        $.getJSON('/api/v1/options/?year_id=' + $("#fld_4151103_1").val() + '&model_id=' + $("#fld_9835059_1").val(), function(data, status) {

        // $.get('', dataToBeSent, function(data, status) {
            $.LoadingOverlay("hide");
            $dropdown.find('option').not(':first').remove().end();
            var isFind = false;
            if (data.length === 1) {
                myitem = data[0];
                $dropdown.append($("<option />").val(myitem.id).text(myitem.title).attr('selected', 'selected'));
            } else {
                $.each(data, function (index, myitem) {
                    $dropdown.append($("<option />").val(myitem.id).text(myitem.title));
                    isFind = true
                });
            }
            if (!isFind) {
                $dropdown.parent().parent().hide();
                $("#fld_7465237_1").prop("disabled", !1);
                $("#text-block-5").css({
                    "margin-right": "65px"
                })
            } else {
                $dropdown.parent().parent().show();
                $("#text-block-5").css({
                    "margin-right": "0"
                })
            }
        }, "json")
    });
    $("#fld_7465237_1").change(function() {
        disableTillItem(5, 1)
    });
    $("#fld_4236944_1").on("input", function() {
        disableTillItem(6, 1)
    });

    $("#fld_279112_2").parent().parent().hide();
    $("#fld_9835059_2").change(function() {
        disableTillItem(2, 2);
        // var $dropdown = $("#fld_279112_2");
        // var dataToBeSent = {
        //     "CompanyTitle": $("#fld_6086100_2").val(),
        //     "CategoryTitle": $("#fld_9835059_2").val()
        // };
        // $.LoadingOverlay("show");
        // $.post('http://carpars.ir:8080/api/bama/GetCarModelsByCategory', dataToBeSent, function(data, status) {
        //     $.LoadingOverlay("hide");
        //     $dropdown.find('option').not(':first').remove().end();
        //     var isFind = !1;
        //     $.each(data, function(index, myitem) {
        //         $dropdown.append($("<option />").val(myitem.Title).text(myitem.Title));
        //         isFind = !0
        //     });
        //     if (!isFind) {
                $("#fld_4151103_2").prop("disabled", !1);
                // $dropdown.parent().parent().hide();
                // $("#text-block-5").css({
                //     "margin-right": "65px"
                // })
            // } else {
                $dropdown.parent().parent().show();
            //     $("#text-block-5").css({
            //         "margin-right": "0"
            //     })
            // }
        // }, "json")
    });
    $("#fld_279112_2").change(function() {
        disableTillItem(3, 2)
    });
    $("#fld_4151103_2").change(function() {
        disableTillItem(4, 2)
    });
    $("#fld_7465237_2").change(function() {
        disableTillItem(5, 2)
    });
    $("#fld_4236944_2").change(function() {
        disableTillItem(6, 2)
    })
})(jQuery);
