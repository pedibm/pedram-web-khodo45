from django.conf.urls import include, url
from django.conf import settings

from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views import static

from apps.main.views import schema_view

admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^$', 'myapp.views.home', name='home'),
    # url(r'^myapp/', include('myapp.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('apps.users.urls')),
    url(r'^', include('apps.main.urls')),
    url(r'^', include('apps.cars.urls')),
    url(r'^', include('apps.inspections.urls')),
    url(r'^api/v1/', include('carpars.api_urls')),
    url(r'^api/back_office/', include('carpars.api_urls_docs')),
    url(r'^api/docs/', schema_view),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    #Log IN & OUT
    url(r'^accounts/login/$', auth_views.login),
    url(r'^login/$', auth_views.login, name="login"),
    url(r'^logout/$', auth_views.logout, name="logout"),

    # Password reset
    url(r'^accounts/password_reset/$',
        auth_views.password_reset,
        name='password_reset_request'
    ),
    url(r'^accounts/password_reset/done/$',
        auth_views.password_reset_done,
        name='password_reset_done'
    ),
    url(r'^accounts/reset/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$',
        auth_views.password_reset_confirm,
        name='password_reset_confirm'
    ),
    url(r'^accounts/reset/done/$',
        auth_views.password_reset_complete,
        name='password_reset_complete'
    ),

    #Change Password
    url(r'^accounts/change_password/$',
        auth_views.password_change,
        name='password_change'),
    url(r'^accounts/change_password_done/$',
        auth_views.password_change_done,
        name='password_change_done'),

    url(r'^static/(?P<path>.*\..*)$', static.serve, {
                'document_root': settings.STATIC_ROOT,
                }),
    url(r'^media/(?P<path>.*\..*)$', static.serve, {
                'document_root': settings.MEDIA_ROOT,
                }),
]

if settings.DEBUG:
    try:
        import debug_toolbar
        urlpatterns = [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
    except ImportError:
        pass

admin.site.site_header = "CarPars"
admin.site.site_title = "CarPars"
