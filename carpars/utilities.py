# -*- coding: utf-8 -*-
import string, random, re

import redis
from django.conf import settings
from django.utils import timezone
from rest_framework import serializers
from rest_framework.authentication import TokenAuthentication

from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from django.utils.encoding import smart_text
import math
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _


def random_generate(size=26, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


def send_email(subject, template, to, variables={}, attachment=None):
    if not type(to) == list:
        to = [to]
    template = get_template(template)
    context = Context(variables)
    htmlContent = template.render(context).encode('utf8')

    text_content = 'This is an important message.'
    msg = EmailMultiAlternatives(
        subject,
        text_content,
        "Merikh Car<%s>" % (settings.DEFAULT_FROM_EMAIL),
        to)
    msg.attach_alternative(htmlContent, "text/html")

    if attachment:
        msg.attach(
            attachment['file_name'],
            attachment['file_location'],
            attachment['file_type'])

    msg.send()


def fixUnicode(s):
    if s and (type(s) == str or type(s) == str):
        if not s == '':
            return smart_text(s)
    return s


def convert_to_number(s):
    return int.from_bytes(s.encode(), 'little')


def convert_from_number(n):
    return n.to_bytes(math.ceil(n.bit_length() / 8), 'little').decode()


def convert_fa_numbers(input_str):
    """
    This function convert Persian numbers to English numbers.

    Keyword arguments:
    input_str -- It should be string
    Returns: English numbers
    """
    mapping = {
        # Start For Persian
        '۰': '0',
        '۱': '1',
        '۲': '2',
        '۳': '3',
        '۴': '4',
        '۵': '5',
        '۶': '6',
        '۷': '7',
        '۸': '8',
        '۹': '9',
        '.': '.',
        # Start For Arabic
        '٠': '0',
        '١': '1',
        '٢': '2',
        '٣': '3',
        '٤': '4',
        '٥': '5',
        '٦': '6',
        '٧': '7',
        '٨': '8',
        '٩': '9',
    }
    pattern = "|".join(map(re.escape, mapping.keys()))
    return re.sub(pattern, lambda m: mapping[m.group()], str(input_str))


class DateTimeFieldWihTZ(serializers.DateTimeField):
    '''Class to make output of a DateTime Field timezone aware
    '''

    def to_representation(self, value):
        value = timezone.localtime(value)
        return super(DateTimeFieldWihTZ, self).to_representation(value)


class Redis:
    instance = None
    conn = None

    @classmethod
    def get_instance(cls, *args, **kwargs):
        if cls.instance:
            return cls.instance
        cls.instance = cls(*args, **kwargs)
        return cls.instance

    def __init__(self, *args, **kwargs):
        self.conn = redis.StrictRedis(settings.REDIS_HOST, settings.REDIS_PORT)


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension,)

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class TokenAuthenticationWithSideffects(TokenAuthentication):
    def authenticate(self, request):
        user_auth_tuple = super().authenticate(request)

        if user_auth_tuple is None:
            return
        (user, token) = user_auth_tuple
        # Disable it for test
        if settings.TEST:
            return user, token

        r = Redis.get_instance().conn
        key = settings.ONLINE_PREFIX.format(user.id)
        r.setex(key, settings.ONLINE_THRESHOLD, user.id)

        return user, token


# @see https://medium.com/@hakibenita/things-you-must-know-about-django-admin-as-your-app-gets-bigger-6be0b0ee9614

def admin_change_url(obj):
    app_label = obj._meta.app_label
    model_name = obj._meta.model.__name__.lower()
    return reverse('admin:{}_{}_change'.format(
        app_label, model_name
    ), args=(obj.pk,))


def admin_link(attr, short_description, empty_description="-"):
    """Decorator used for rendering a link to a related model in
    the admin detail page.
    attr (str):
        Name of the related field.
    short_description (str):
        Name if the field.
    empty_description (str):
        Value to display if the related field is None.
    The wrapped method receives the related object and should
    return the link text.
    Usage:
        @admin_link('credit_card', _('Credit Card'))
        def credit_card_link(self, credit_card):
            return credit_card.name
    """

    def wrap(func):
        def field_func(self, obj):
            related_obj = getattr(obj, attr)
            if related_obj is None:
                return empty_description
            url = admin_change_url(related_obj)
            return format_html(
                '<a href="{}">{}</a>',
                url,
                func(self, related_obj)
            )

        field_func.short_description = short_description
        field_func.allow_tags = True
        return field_func

    return wrap


def admin_changelist_url(model):
    app_label = model._meta.app_label
    model_name = model.__name__.lower()
    return reverse('admin:{}_{}_changelist'.format(
        app_label,
        model_name)
    )


def admin_changelist_link(
    attr,
    short_description,
    empty_description="-",
    query_string=None
):
    """Decorator used for rendering a link to the list display of
    a related model in the admin detail page.
    attr (str):
        Name of the related field.
    short_description (str):
        Field display name.
    empty_description (str):
        Value to display if the related field is None.
    query_string (function):
        Optional callback for adding a query string to the link.
        Receives the object and should return a query string.
    The wrapped method receives the related object and
    should return the link text.
    Usage:
        @admin_changelist_link('credit_card', _('Credit Card'))
        def credit_card_link(self, credit_card):
            return credit_card.name
    """

    def wrap(func):
        def field_func(self, obj):
            related_obj = getattr(obj, attr)
            if related_obj is None:
                return empty_description
            url = admin_changelist_url(related_obj.model)
            if query_string:
                url += '?' + query_string(obj)
            return format_html(
                '<a href="{}">{}</a>',
                url,
                func(self, related_obj)
            )

        field_func.short_description = short_description
        field_func.allow_tags = True
        return field_func

    return wrap


    #
    # def send_ban(url):
    #     """Issue a HTTP request with BAN method to Varnish :param url: a path or regexp to ban (invalidate) :return: None """
    #     sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #     try:
    #         sock.connect((settings.VARNISH_HOST, settings.VARNISH_PORT))
    #     except socket.error:
    #         logger.exception('Varnish not available')
    #     else:
    #         sock.sendall("BAN %s HTTP/1.1rnHost: %srnrn" % ( url, settings.VARNISH_HOST ))
    #         http_status, body = response.split("n", 1)
    #         _, status, text = http_status.split(" ", 2)
    #         if status == "%s" % httplib.OK:
    #             pass
    #     finally:
    #         sock.close()
