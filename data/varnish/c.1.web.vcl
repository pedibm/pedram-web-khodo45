vcl 4.0;

sub vcl_recv {

    # Ignore all cookies except for /admin
    if (!(req.url ~ "^/admin/")) {
        unset req.http.Cookie;
    }

    # # Give ourself some restart time
    # set req.backend_hint = default;
    # if (! std.healthy(req.backend)) {
    #     set req.grace = 5m;
    # } else {
    #     set req.grace = 15s;
    # }
}

sub vcl_backend_response {
    # Unset any cookies for static content and cache for 1 hour
    if (bereq.url ~ "^/(static|media)/") {
        std.log("SAJAD: media " + bereq.url);
        unset beresp.http.set-cookie;
        set beresp.ttl = 24h;
    } else if (bereq.url ~ "^/$") {
        std.log("SAJAD: main " + bereq.url);
        unset beresp.http.set-cookie;
        set beresp.ttl = 24h;
    } else if (bereq.url ~ "^/api/v1/(brands|models|options|years|branch|city)/") {
        std.log("SAJAD: brands " + bereq.url);

        unset beresp.http.set-cookie;
        set beresp.ttl = 24h;
    } else if (bereq.url ~ "^/api/v1/auctions/$") {
        std.log("SAJAD: auctions " + bereq.url);

        unset beresp.http.set-cookie;
        set beresp.ttl = 3m;
//    } else if (bereq.url ~ "^/api/v1/auctions/") {
//
//        set beresp.ttl = 3m;
    } else if (bereq.url ~ "^/api/v1/appointments/") {
        std.log("SAJAD: appointments " + bereq.url);

        if (bereq.url ~ "/days/") {
            set beresp.ttl = 20h;
        } else if (bereq.url ~ "/hours/") {
            set beresp.ttl = 1h;
        }
        set beresp.ttl = 0s;
        set beresp.uncacheable = true;

    } else if (bereq.url ~ "^/api/v1/inspection_dealer/") {
        std.log("SAJAD: inspection_dealer " + bereq.url);
        set beresp.do_esi = true;
        if (bereq.url ~ "/(image|positive_comments|segments)/") {
            set beresp.ttl = 5h;
//        } else if (bereq.url ~ "/hours/") {
//            set beresp.ttl = 1h;
        }
        set beresp.ttl = 3h;
    } else if (bereq.url ~ "^/api/v1/inspection/") {
        std.log("SAJAD: inspection " + bereq.url);

        if (bereq.url ~ "^/api/v1/inspection/$") {
            set beresp.ttl = 0s;
            set beresp.uncacheable = true;
        }
        set beresp.ttl = 0s;
        set beresp.uncacheable = true;
    } else if (bereq.url ~ "^/segments/$") {
        std.log("SAJAD: segments " + bereq.url);
        set beresp.ttl = 1d;
    } else if (bereq.url ~ "^/admin/" || bereq.url ~ "^/api/back_office/") {
        std.log("SAJAD: admin " + bereq.url);
        set beresp.ttl = 0s;
        set beresp.uncacheable = true;
    } else if (bereq.url ~ "^/api/v1/releases/versions/$") {
        unset beresp.http.set-cookie;
        set beresp.ttl = 30m;
    } else if (bereq.url ~ "/step(2|3)") {
        unset beresp.http.set-cookie;
        set bereq.url = regsub(bereq.url, "\?.*$", "");
    } else {
        std.log("SAJAD: else " + bereq.url);
        # Set Default TTL
        set beresp.ttl = 2m;
    }
    # # Add some grace for backend restarts
    # set beresp.grace = 30m;
}
